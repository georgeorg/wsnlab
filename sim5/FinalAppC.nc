#include "Final.h"



configuration FinalAppC {}
implementation {

  components MainC, FinalC as App, LedsC;

	components new DemoSensorC() as DemoSensor;
	//components new HamamatsuS1087ParC() as DemoSensor;
	
  
  components new AMSenderC(AM_RADIO_COUNT_MSG) as Sender;
  components new AMReceiverC(AM_RADIO_COUNT_MSG) as Receiver;

  components new AMSenderC(AM_RADIO_COUNT_MSG) as FloodSender;
  components new AMReceiverC(AM_RADIO_COUNT_MSG) as FloodReceiver;

  //components new SerialAMReceiverC(AM_SERIAL_MSG) as Receiver;
  //components new SerialAMSenderC(AM_SERIAL_MSG) as Sender;


  components ActiveMessageC;

  components new TimerMilliC() as InstructionTimer;
  //components new TimerMilliC() as ContextTimer;
  components new TimerMilliC() as SourceTimer;
  components new TimerMilliC() as SourceTimer2;
  components new TimerMilliC() as RebroadcastTimer;
  //components new TimerMilliC() as SensorTimer;
  components new TimerMilliC() as BufferTimer;
  //components new TimerMilliC() as SendResultTimer;
  components HilTimerMilliC as LocalTimer;
   
  App.Boot -> MainC.Boot;
  App.Receive -> Receiver;
  App.Send -> Sender;
  App.FloodReceive -> FloodReceiver;
  App.FloodSend -> FloodSender;


  App.AMControl -> ActiveMessageC;
  App.Packet -> FloodSender;
  //May need to connect this to multiple senders

  App.DemoRead -> DemoSensor;

  App.LocalTimer -> LocalTimer;
  App.InstructionTimer -> InstructionTimer;
  App.SourceTimer -> SourceTimer;
  App.SourceTimer2 -> SourceTimer2;
  //App.SensorTimer -> SensorTimer;
  App.RebroadcastTimer -> RebroadcastTimer;
  App.BufferTimer -> BufferTimer;
  //App.SendResultTimer -> SendResultTimer;
  //App.ContextTimer -> ContextTimer;
  

  App.Leds -> LedsC;	
}


