#include "Timer.h"
#include "Final.h"



module FinalC @safe() {
  uses {
   interface Leds;
   interface Boot;
   interface Receive as Receive;
   interface Receive as FloodReceive;
   interface AMSend as FloodSend;
   interface AMSend as Send;
   interface Timer<TMilli> as InstructionTimer;
   interface Timer<TMilli> as ContextTimer;
   interface LocalTime<TMilli> as LocalTimer;
   interface Timer<TMilli> as SourceTimer ;
   interface Timer<TMilli> as SourceTimer2 ;
   interface Timer<TMilli> as RebroadcastTimer ;
   //interface Timer<TMilli> as SensorTimer;
   interface Timer<TMilli> as BufferTimer;
   //interface Timer<TMilli> as SendResultTimer;
   interface SplitControl as AMControl;
   interface Packet as Packet;
   interface Read<uint16_t> as DemoRead;
  }
}
implementation{


  #define BYTES_MSG 2
  #define SIZE 10
  #define CTX_RATE 50
  #define INSTRUCTION_RATE 50

  #define BUFF_SIZE 5
  #define PGSIZE 5
  #define FAMILY_SIZE 10

  #define APP_MSG 1
  #define NONE_MSG 2
  #define NORMAL_MSG 3
  #define PIGGY_MSG 4
  #define STATS_MSG 5
  #define CANCEL_MSG 6

  #define NONE 1
  #define NORMAL 2
  #define PIGGY 3
  #define STATS 4

  //Declarations
  message_t packet;
  int8_t offset = 0;
  uint8_t msbits,lsbits;

  uint8_t i,temp_k,j,k;
  uint8_t loop_count = 0;

  bool locked = FALSE;
  bool lockedRead = FALSE;
  bool second_packet = FALSE;
  
  //uint16_t data = 0;
  uint16_t cached_data = 0;
  uint32_t cached_data_timestamp = 0;
  uint32_t t1;
  int8_t ctx_id = -1;

  application_t app[SIZE];

  ///////////////////////////////////////////
  //DECLARATIONS FROM ASSIGNMENT 3
  ///////////////////////////////////////////
    
  message_t packet;

  uint8_t ledflag;
  uint32_t rtt_time = 0;
  uint32_t backoff_time = 0;
  uint16_t base_waiting_time = 50;

  //Flags and counters
  //bool locked = FALSE;
  //bool lockedRead = FALSE;
  //bool inserted_query = FALSE;
  //bool buffer_flag = FALSE;
  uint16_t counter = 0;
  //uint8_t i, j;
  uint8_t mbuf = 0;
  uint8_t tbuf = 0;

  //Arrays and buffers
  uint16_t stored[SIZE][3];
  //stored_query_t queries[SIZE];
  //uint8_t query_buffer[SIZE];
  stored_piggy_t local_piggy[SIZE];
  //stored_stats_t local_stats[SIZE];

  stored_buffer_t temp_buffer[BUFF_SIZE];

  stored_msg_t msg_buffer[BUFF_SIZE];

  uint16_t stored_sender = 0;
  bool cancelation_packet_received = FALSE;
  uint16_t stored_app_id = 0;
  uint16_t local_packet_id = 0;
  uint16_t local_round_id = 0;
  uint32_t t1,t2,xronos;
  uint8_t stored_agg_mode = PIGGY;


  event void Boot.booted() {
    call AMControl.start();

    for(i = 0;i < SIZE;i++){
      app[i].app_id = 0;
      for(j = 0;j<10;j++){
        app[i].reg[j] = 0; 
      }
      for(j =0;j<50;j++){
        app[i].app_bytes[j] = 0;
      }
      app[i].pc = 0;
      app[i].saved_pc = 0;
      app[i].isActive = FALSE;
      app[i].pending_time = 0;
      app[i].pending_data = FALSE;
      app[i].has_msg_handler = FALSE;
      app[i].pending_msg = 0;
      app[i].agg_mode = 0;
      app[i].hop_count = 0;
      app[i].regulate_time = 1;

      //Initialize local piggy
      local_piggy[i].remaining_space = PGSIZE;
      //This denotes the NEXT packet that will be sent
      local_piggy[i].period_id = 1;
      for(j=0; j<PGSIZE; j++){
        local_piggy[i].node_id[j] = 0;
        local_piggy[i].pg_r7[j] = 0;
        local_piggy[i].pg_r8[j] = 0;
       }
    }
    //call ContextTimer.startPeriodic(CTX_RATE);
    call InstructionTimer.startPeriodic(INSTRUCTION_RATE);
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      if(TOS_NODE_ID == 1){
        call SourceTimer.startOneShot(100); 
        call SourceTimer2.startOneShot(120000);  
      }
    }
    else{
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) {
    // do nothing
  }

  task void runApp(){

    loop_count = 0;

    //Round Robin Apps

    do{
      ctx_id++;
      if(ctx_id == SIZE){ctx_id = 0;}
      loop_count = loop_count + 1;
    }while((!app[ctx_id].isActive) && (loop_count<(SIZE+1)));

    //If no active app was found, post to recheck and return
    //Not sure if good practice
    if(loop_count>=(SIZE+1)){
      dbg("VMDebugC","Didnt find active APP, returning on ctx id %d\n", ctx_id);
      return;
    }

    dbg("VMDebugC","Running app with context id:%d\n", ctx_id);

    msbits = app[ctx_id].app_bytes[app[ctx_id].pc] & 0xF0;
    msbits = msbits >> 4;
    lsbits = app[ctx_id].app_bytes[app[ctx_id].pc] & 0x0F;
    
    switch(msbits) {
          //RET Instruction
      case 0 :
        if(app[ctx_id].pc >= 4 + app[ctx_id].app_bytes[1]){
          
          //Restore pc to previous position
          app[ctx_id].pc = app[ctx_id].saved_pc;
          dbg("VMDebugC","Instruction RET from Timer. PC :%d\n",app[ctx_id].pc);
          
          if((app[ctx_id].pc >= 4 + app[ctx_id].app_bytes[1] + app[ctx_id].app_bytes[2]) && app[ctx_id].app_bytes[3]!=0){
          	dbg("VMDebugC","Instruction RET was actually from MSG Handler. PC :%d\n",app[ctx_id].pc);
          }

          //Check if we have pending msgs
          if(app[ctx_id].pending_msg != 0){
          	//Load the message and run msg handler
          	for(j=0; j<BUFF_SIZE; j++){
          		if(msg_buffer[j].app_id == app[ctx_id].app_id){

          			app[ctx_id].isActive = TRUE;
	    			app[ctx_id].reg[8] = msg_buffer[j].r7;
	    			app[ctx_id].reg[9] = msg_buffer[j].r8;
	    				
	    			app[ctx_id].saved_pc = app[ctx_id].pc;
				    app[ctx_id].pc = 4 + app[ctx_id].app_bytes[1] + app[ctx_id].app_bytes[2];

				    dbg("VMDebugC","Found pending messages, executing handler. Jumping from : %d to : %d\n",app[j].saved_pc, app[j].pc);

				    //Reduce msgs & free position in msg buffer
				    app[ctx_id].pending_msg = app[ctx_id].pending_msg - 1;
				    msg_buffer[j].app_id = 0;

          			break;
          		}
          	}
          }

          post runApp();
        }
        else{
          //If messages are pending, execute the msg handler
          if(app[ctx_id].pending_msg != 0){
          	//Load the message and run msg handler
          	for(j=0; j<BUFF_SIZE; j++){
          		if(msg_buffer[j].app_id == app[ctx_id].app_id){

          			app[ctx_id].isActive = TRUE;
	    			app[ctx_id].reg[8] = msg_buffer[j].r7;
	    			app[ctx_id].reg[9] = msg_buffer[j].r8;
	    				
	    			app[ctx_id].saved_pc = app[ctx_id].pc;
				    app[ctx_id].pc = 4 + app[ctx_id].app_bytes[1] + app[ctx_id].app_bytes[2];

				    dbg("VMDebugC","Found pending messages, executing handler. Jumping from : %d to : %d\n",app[j].saved_pc, app[j].pc);

				    //Reduce msgs & free position in msg buffer
				    app[ctx_id].pending_msg = app[ctx_id].pending_msg - 1;
				    msg_buffer[j].app_id = 0;

          			break;
          		}
          	}
          }
          //If no timer is pending, unload app
          else if( app[ctx_id].pending_time == 0){
            dbg("VMDebugC","RET from init.Deactivate App.\n");

            /*
                //Unload App
                app[ctx_id].app_id = 0;
                for(j = 0;j<10;j++){
                  app[ctx_id].reg[j] = 0; 
                }
                for(j =0;j<50;j++){
                  app[ctx_id].app_bytes[j] = 0;
                }
                app[ctx_id].pc = 0;
                app[ctx_id].saved_pc = 0;
                app[ctx_id].isActive = FALSE;
                app[ctx_id].pending_time = 0;
                app[ctx_id].pending_data = FALSE;

                app[ctx_id].has_msg_handler = FALSE;
                app[ctx_id].pending_msg = 0;
      			app[ctx_id].agg_mode = 0;
      			app[ctx_id].hop_count = 0;
      			app[ctx_id].regulate_time = 1;
      		*/
      			app[ctx_id].isActive = FALSE;
                post runApp();
          }
          //If a timer is pending, deactivate app
          //And wait for timer to fire
          else{
            dbg("VMDebugC","RET from init.Timer Pending, deactivating\n");
            app[ctx_id].isActive = FALSE;

            post runApp();
          } 
        }
        break;
          //SET Instruction
          case 1:
            app[ctx_id].pc++;
            app[ctx_id].reg[lsbits-1] = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
            dbg("VMDebugC","SET Instruction. Set reg %d to %d \n", lsbits, app[ctx_id].reg[lsbits-1]);
            //Go to next instruction
            app[ctx_id].pc++;
            dbg("VMDebugC","SET PC:%d.\n",app[ctx_id].pc);
        post runApp();
            break;
          //CPY Instruction
          case 2:
            app[ctx_id].pc++;
            app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];
            dbg("VMDebugC","CPY Instruction. Cpy reg %d to %d \n", lsbits, app[ctx_id].app_bytes[app[ctx_id].pc]);
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //ADD Instruction
          case 3:
            app[ctx_id].pc++;
            app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] + app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];
            dbg("VMDebugC","ADD Instruction. Add reg %d to %d \n", app[ctx_id].app_bytes[app[ctx_id].pc], lsbits);
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //SUB Instruction
          case 4:
            app[ctx_id].pc++;
            dbg("VMDebugC","SUB Instruction.  %d - %d.\n",
              app[ctx_id].reg[lsbits-1],app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1]);

            app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] - app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];

            dbg("VMDebugC","SUB Instruction. Sub reg %d from %d.Result:%d\n", 
              app[ctx_id].app_bytes[app[ctx_id].pc], lsbits,app[ctx_id].reg[lsbits-1]);
            
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //INC Instruction
          case 5:
            app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] + 1;
            dbg("VMDebugC","INC Instruction. Incremented reg %d \n", lsbits);
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //DEC Instruction
          case 6:
              app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] - 1;
              dbg("VMDebugC","DEC Instruction. Decremented reg %d \n", lsbits);
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //MAX Instruction
          case 7:
            app[ctx_id].pc++;
            if (app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1] > app[ctx_id].reg[lsbits-1]){
              app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];
            }
            dbg("VMDebugC","MAX Instruction \n");
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //MIN Instruction
          case 8:
            app[ctx_id].pc++;
            if (app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1] < app[ctx_id].reg[lsbits-1]){
              app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];
            }
            dbg("VMDebugC","MIN Instruction \n");
            //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            break;
          //BGZ Instruction
          case 9:
            app[ctx_id].pc++;
            if (app[ctx_id].reg[lsbits-1] > 0){
              offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
              app[ctx_id].pc = app[ctx_id].pc + offset;
            }
            else{
              //Go to next instruction
              app[ctx_id].pc++;
            }
          post runApp();
            dbg("VMDebugC","BGZ Instruction \n");
            break;
          //BEZ Instruction
          case 10:
            app[ctx_id].pc++;
            if (app[ctx_id].reg[lsbits-1] == 0){
              offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
              app[ctx_id].pc = app[ctx_id].pc + offset;
            }
            else{
              //Go to next instruction
              app[ctx_id].pc++;
            }
            post runApp();
            dbg("VMDebugC","BEZ Instruction jumping to:%d with offset:%d \n",app[ctx_id].pc,offset);
            break;
          //BRA Instruction
          case 11:
            app[ctx_id].pc++;
            offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
            app[ctx_id].pc = app[ctx_id].pc + offset;
            dbg("VMDebugC","BRA instruction jump to :%d\n",app[ctx_id].pc);
            post runApp();
            break;
          //LED Instruction
      case 12:
          dbg("VMDebugC","Instruction Led\n");
          if(lsbits == 0){
            dbg("VMDebugC","Turn led off from ctx_id:%d\n", ctx_id);
            if(app[ctx_id].app_id == 1){
              call Leds.led0Off();
            }
            else if (app[ctx_id].app_id == 2){
              call Leds.led1Off();
            }
            else{
              call Leds.led2Off();
            }
          }
          else{
            dbg("VMDebugC","Turn led on from ctx_id:%d\n", ctx_id);
            if(app[ctx_id].app_id == 1){
              call Leds.led0On();
            }
            else if (app[ctx_id].app_id == 2){
              call Leds.led1On();
            }
            else{
              call Leds.led2On();
            }
          }
          //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
          break;
          //RDB Instruction
          case 13:
            //If cached is older than 1 second, resample
            t1 = call LocalTimer.get();
            if((cached_data != 0) && (t1 - cached_data_timestamp < 1000)){
              app[ctx_id].reg[lsbits-1] = cached_data;
              dbg("VMDebugC","Data %d \n",app[ctx_id].reg[lsbits-1]);
              //Go to next instruction
            app[ctx_id].pc++;
          post runApp();
            }
            else{
              if (!lockedRead){
               dbg("VMDebugC","Calling Demoread\n");
               call DemoRead.read();
               lockedRead = TRUE;
              }
              app[ctx_id].isActive = FALSE;
              app[ctx_id].pending_data = TRUE;
              post runApp();
            }
            dbg("VMDebugC","Rdb Instruction \n");
            break;
          //TMR Instruction
      case 14:
        dbg("VMDebugC","Instruction Timer\n");
        app[ctx_id].pc++;

        //Simple Timer
        if(lsbits == 0){
        	app[ctx_id].pending_time = app[ctx_id].app_bytes[app[ctx_id].pc] * 1000;
        }
        //Aggregation Timer
        else{
        	if(!app[ctx_id].has_msg_handler){
        		app[ctx_id].agg_mode = PIGGY;	
        	}
        	else{
        		app[ctx_id].agg_mode = STATS;	
        	}
        	
        	app[ctx_id].pending_time = (app[ctx_id].app_bytes[app[ctx_id].pc] * 1000);
        	//Adjust the waiting to aggregate
        	app[ctx_id].pending_time = app[ctx_id].pending_time + (base_waiting_time /* app[ctx_id].regulate_time*/ * app[ctx_id].hop_count);
        	dbg("RadioCountToLedsC", "Setting aggregated pending_time to :%d\n", app[ctx_id].pending_time);
        }
        

        
        //Go to next command
        app[ctx_id].pc++;

        post runApp();
        break;
      //SND Instruction    
      case 15:
        dbg("VMDebugC","Instruction SND\n");

        //Send according to agg mode, after waiting for backoff

        ///////////////////
        //Send NONE MSG
        ///////////////////
        if(app[ctx_id].agg_mode == NONE){
        	//Put message in buffer
        	if(tbuf < BUFF_SIZE){

        		temp_buffer[tbuf].msg_type = NONE_MSG;
                temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
                temp_buffer[tbuf].app_id = app[ctx_id].app_id;
                temp_buffer[tbuf].period_id = app[ctx_id].period_id;
                temp_buffer[tbuf].sender = app[ctx_id].sender;
                //Check which registers to send
                if(lsbits == 0){
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = 0;
                }
                else{
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = app[ctx_id].reg[7];
                }

        		tbuf++;
        	}
        	else{
        		dbg("RadioCountToLedsC", "temp buffer FULL\n");
        	}


     		if(! call BufferTimer.isRunning()){
            	backoff_time = call LocalTimer.get();
            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

            	call BufferTimer.startOneShot(backoff_time);
          	}
        }
        ///////////////////
        //Send PIGGY MSG
        ///////////////////
        else if(app[ctx_id].agg_mode == PIGGY){

        	//Add my data to local, and add to temp buffer;
          if(tbuf < BUFF_SIZE){
	        temp_buffer[tbuf].msg_type = PIGGY_MSG;
	        temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
	        temp_buffer[tbuf].remaining_space = local_piggy[ctx_id].remaining_space - 1;
	        temp_buffer[tbuf].app_id = app[ctx_id].app_id;
	        temp_buffer[tbuf].period_id = app[ctx_id].period_id; //maybe this should be from local_piggy

	        //Lord help us with the hop count
	        temp_buffer[tbuf].hop_count = app[ctx_id].hop_count;

	        temp_buffer[tbuf].sender = app[ctx_id].sender;

	        //First add id and data to local, for allignement
	        local_piggy[ctx_id].node_id[PGSIZE - local_piggy[ctx_id].remaining_space] = TOS_NODE_ID;
	        if(lsbits == 0){
	        	local_piggy[ctx_id].pg_r7[PGSIZE - local_piggy[ctx_id].remaining_space] = app[ctx_id].reg[6];
	        	local_piggy[ctx_id].pg_r8[PGSIZE - local_piggy[ctx_id].remaining_space] = 0;
	        }
	        else{
	        	local_piggy[ctx_id].pg_r7[PGSIZE - local_piggy[ctx_id].remaining_space] = app[ctx_id].reg[6];
	        	local_piggy[ctx_id].pg_r8[PGSIZE - local_piggy[ctx_id].remaining_space] = app[ctx_id].reg[7];
	        }
	        

	        //Add to appropriate position
	        for(i=0; i<PGSIZE; i++){
	          temp_buffer[tbuf].node_id[i] = local_piggy[ctx_id].node_id[i];
	          temp_buffer[tbuf].pg_r7[i] = local_piggy[ctx_id].pg_r7[i];
	          temp_buffer[tbuf].pg_r8[i] = local_piggy[ctx_id].pg_r8[i];
	        }
	        
	        //Empty the local buffer
	        local_piggy[ctx_id].remaining_space = PGSIZE;
	        //This now denotes the NEXT period
	        local_piggy[ctx_id].period_id = local_piggy[ctx_id].period_id + 1;
	        for(i=0; i<PGSIZE; i++){
	          local_piggy[ctx_id].node_id[i] = 0;
	          local_piggy[ctx_id].pg_r7[i] = 0;
	          local_piggy[ctx_id].pg_r8[i] = 0;
	        }

	        tbuf++;

	        if(! call BufferTimer.isRunning()){
            	backoff_time = call LocalTimer.get();
            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

            	call BufferTimer.startOneShot(backoff_time);
          	}
	      }
	      else{
	      	dbg("RadioCountToLedsC", "temp buffer FULL\n");
	      }
        }
        ///////////////////
        //Send NORMAL MSG
        ///////////////////
        else if(app[ctx_id].agg_mode == NORMAL){
        	//Put message in buffer
        	if(tbuf < BUFF_SIZE){

        		temp_buffer[tbuf].msg_type = NORMAL_MSG;
                temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
                temp_buffer[tbuf].app_id = app[ctx_id].app_id;
                temp_buffer[tbuf].period_id = app[ctx_id].period_id;
                temp_buffer[tbuf].sender = app[ctx_id].sender;
                //Check which registers to send
                if(lsbits == 0){
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = 0;
                }
                else{
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = app[ctx_id].reg[7];
                }

        		tbuf++;
        	}
        	else{
        		dbg("RadioCountToLedsC", "temp buffer FULL\n");
        	}


     		if(! call BufferTimer.isRunning()){
            	backoff_time = call LocalTimer.get();
            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

            	call BufferTimer.startOneShot(backoff_time);
          	}
        }
        ///////////////////
        //Send STATS MSG
        ///////////////////
        else if(app[ctx_id].agg_mode == STATS){
        	//Put message in buffer
        	if(tbuf < BUFF_SIZE){

        		temp_buffer[tbuf].msg_type = STATS_MSG;
                temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
                temp_buffer[tbuf].app_id = app[ctx_id].app_id;
                temp_buffer[tbuf].period_id = app[ctx_id].period_id;
                //Lord help us with the hop count
	        	temp_buffer[tbuf].hop_count = app[ctx_id].hop_count;
                temp_buffer[tbuf].sender = app[ctx_id].sender;
                //Check which registers to send
                if(lsbits == 0){
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = 0;
                }
                else{
                	temp_buffer[tbuf].r7 = app[ctx_id].reg[6];
                	temp_buffer[tbuf].r8 = app[ctx_id].reg[7];
                }

        		tbuf++;
        	}
        	else{
        		dbg("RadioCountToLedsC", "temp buffer FULL\n");
        	}


     		if(! call BufferTimer.isRunning()){
            	backoff_time = call LocalTimer.get();
            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

            	call BufferTimer.startOneShot(backoff_time);
          	}

        }

        //Go to next command
        app[ctx_id].pc++;

        post runApp();
        break;
      //DEFAULT Instruction
      default :
        dbg("VMDebugC","WRONG\n");

            //Unload App
              app[ctx_id].app_id = 0;
              for(j = 0;j<10;j++){
                app[ctx_id].reg[j] = 0; 
              }
              for(j =0;j<50;j++){
                app[ctx_id].app_bytes[j] = 0;
              }
              app[ctx_id].pc = 0;
              app[ctx_id].saved_pc = 0;
              app[ctx_id].isActive = FALSE;
              app[ctx_id].pending_time = 0;
              app[ctx_id].pending_data = FALSE;

              app[ctx_id].has_msg_handler = FALSE;
              app[ctx_id].pending_msg = 0;
      		  app[ctx_id].agg_mode = 0;
      		  app[ctx_id].hop_count = 0;
      		  app[ctx_id].regulate_time = 1;

        break;
    } 
  }
  event void SourceTimer2.fired(){
  	app_bytecode_msg_t* apm = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    if (apm == NULL) {
      return;
    }

    apm->msg_type = CANCEL_MSG;
    apm->app_id = 1;
    apm->sender = TOS_NODE_ID;

    if(!locked){
    	if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
        	t1 = call LocalTimer.get();
          	dbg("RadioCountToLedsC","App id %d SendTime %d.\n",apm->app_id,t1);
          	dbg("RadioCountToLedsC", "Packet sent.\n");
          	locked = TRUE;
        }
    }
  }

  event void SourceTimer.fired(){

    app_bytecode_msg_t* apm = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    if (apm == NULL) {
      return;
    }

      apm->msg_type = APP_MSG;
      apm->app_id = 1;
      //apm->packet_id = 1;
      apm->sender = TOS_NODE_ID;
      
      /*
      //App1
      apm->codebytes[0] = 0x0C;
      apm->codebytes[1] = 0x03;
      apm->codebytes[2] = 0x05;
      apm->codebytes[3] = 0x00;
      //Timer mode and val
      apm->codebytes[4] = 0xE1;
      apm->codebytes[5] = 0x3C;
      apm->codebytes[6] = 0x00;
      apm->codebytes[7] = 0xD7;
      apm->codebytes[8] = 0xF0;
      //Timer mode and val
      apm->codebytes[9] = 0xE1;
      apm->codebytes[10] = 0x3C;
      apm->codebytes[11] = 0x00;

      for(i=12;i<28;i++){
        apm->codebytes[i] = 0;
      }
      */

      /*
      //App2
      apm->codebytes[0] = 0x10;
      apm->codebytes[1] = 0x03;
      apm->codebytes[2] = 0x05;
      apm->codebytes[3] = 0x04;
      apm->codebytes[4] = 0xE0;
      apm->codebytes[5] = 0x3C;
      apm->codebytes[6] = 0x00;
      apm->codebytes[7] = 0xD7;
      apm->codebytes[8] = 0xF0;
      apm->codebytes[9] = 0xE0;
      apm->codebytes[10] = 0x3C;
      apm->codebytes[11] = 0x00;
      apm->codebytes[12] = 0x27;
      apm->codebytes[13] = 0x09;
      apm->codebytes[14] = 0xF0;
      apm->codebytes[15] = 0x00;


      for(i=16;i<28;i++){
        apm->codebytes[i] = 0;
      }
      */

      
      //App3
      apm->codebytes[0] = 0x1C;
      apm->codebytes[1] = 0x07;
      apm->codebytes[2] = 0x0C;
      apm->codebytes[3] = 0x05;
      apm->codebytes[4] = 0x17;
      apm->codebytes[5] = 0x00;
      apm->codebytes[6] = 0x18;
      apm->codebytes[7] = 0x7F;
      apm->codebytes[8] = 0xE1;
      apm->codebytes[9] = 0x1E; //edw
      apm->codebytes[10] = 0x00;
      apm->codebytes[11] = 0xD1;
      apm->codebytes[12] = 0x57;
      apm->codebytes[13] = 0x88;
      apm->codebytes[14] = 0x01;
      apm->codebytes[15] = 0xF1;
      apm->codebytes[16] = 0x17;
      apm->codebytes[17] = 0x00;
      apm->codebytes[18] = 0x18;
      apm->codebytes[19] = 0x7F;
      apm->codebytes[20] = 0xE1;
      apm->codebytes[21] = 0x1E;//edw
      apm->codebytes[22] = 0x00;
      apm->codebytes[23] = 0x37;
      apm->codebytes[24] = 0x09;
      apm->codebytes[25] = 0x88;
      apm->codebytes[26] = 0x0A;
      apm->codebytes[27] = 0x00;
	
      

      for(j=0; j<SIZE-1; j++){
      stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
      stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
      stored[(SIZE-1)-j][2]= stored[(SIZE-1)-j-1][2];
      }

      stored[0][0] = 1; //this is packet owner
      stored[0][1] = apm->app_id;
      stored[0][2] = 1;
      //stored[0][2] = apm->round_id;

      if(!locked){
        if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
          t1 = call LocalTimer.get();
          dbg("RadioCountToLedsC","App id %d SendTime %d.\n",apm->app_id,t1);
          dbg("RadioCountToLedsC", "Packet sent.\n");
          locked = TRUE;
        }
      }

      //call SourceTimer.startOneShot(200);
      
  }

  event void RebroadcastTimer.fired(){

    if(cancelation_packet_received){
    	app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    	if (new_packet == NULL) {
      		dbg("RadioCountToLedsC", "Packet not allocated.\n");  
    	}
    
    	new_packet->msg_type = CANCEL_MSG;
    	new_packet->app_id = stored_app_id;
    	new_packet->sender = TOS_NODE_ID;

    	if(!locked){
      		if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
      			dbg("RadioCountToLedsC", "RadioCountToLedsC: Cancelation packet for app id :%d rebroadcast sent.\n", stored_app_id);
        		locked = TRUE;
      		}
    	}
	    else{
	      dbg("RadioCountToLedsC", "Locked send conflict, cancelation rebroadcast msg failed\n");
	      
	      dbg("RadioCountToLedsC", "Using temp buffer\n");
	      if(tbuf < BUFF_SIZE){
	        temp_buffer[tbuf].msg_type = new_packet->msg_type;
	        temp_buffer[tbuf].app_id = new_packet->app_id;
	        temp_buffer[tbuf].sender = new_packet->sender;
	        for(k = 0;k < 28;k++){
	          temp_buffer[tbuf].codebytes[k] = 0;
	        }

	        tbuf++;
	      }
	      else{
	        dbg("RadioCountToLedsC", "temp buffer FULL\n");
	      }
	    }

	    cancelation_packet_received = FALSE;
	}
	else{
      
	    //lock gia na mi mporei na steilei an de steilei to proigoumeno
	    app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
	    if (new_packet == NULL) {
	      dbg("RadioCountToLedsC", "Packet not allocated.\n");  
	    }
	    
	    new_packet->msg_type = APP_MSG;
	    new_packet->app_id = stored[0][1];
	    new_packet->sender = TOS_NODE_ID;
	    for(i=0; i<SIZE; i++){
	    	if(app[i].app_id == new_packet->app_id){
	    		for(k = 0;k < 28;k++){
			      new_packet->codebytes[k] = app[i].app_bytes[k];
			    }
	    		break;
	    	}
	    }
	    
	    t1 = call LocalTimer.get();
	    
	    if(!locked){
	      if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
	      dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d:%d rebroadcast sent.\n", stored[0][0],stored[0][1],stored[0][2]);
	        locked = TRUE;
	      }
	    }
	    else{
	      dbg("RadioCountToLedsC", "Locked send conflict, flooding msg failed\n");
	      
	      dbg("RadioCountToLedsC", "Using temp buffer\n");
	      if(tbuf < BUFF_SIZE){
	        temp_buffer[tbuf].msg_type = new_packet->msg_type;
	        temp_buffer[tbuf].app_id = new_packet->app_id;
	        temp_buffer[tbuf].sender = new_packet->sender;
	        for(k = 0;k < 28;k++){
	          temp_buffer[tbuf].codebytes[k] = new_packet->codebytes[k];
	        }

	        tbuf++;
	      }
	      else{
	        dbg("RadioCountToLedsC", "temp buffer FULL\n");
	      }

	    }
	}
  }

  event void BufferTimer.fired(){
    //Send with none/piggy/stats
    if(tbuf != 0){
    	////////////////////////
    	//Buffering of APP MSG
    	////////////////////////
    	if(temp_buffer[tbuf-1].msg_type == APP_MSG || temp_buffer[tbuf-1].msg_type == CANCEL_MSG ){

          app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
          if (new_packet == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }
          
          new_packet->msg_type = temp_buffer[tbuf-1].msg_type;
          new_packet->app_id = temp_buffer[tbuf-1].app_id;
          new_packet->sender = temp_buffer[tbuf-1].sender;
          for(k = 0;k < 28;k++){
              new_packet->codebytes[k] = temp_buffer[tbuf-1].codebytes[k];
          }
          
          if(!locked){
            if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d rebroadcast sent.\n",new_packet->app_id);
              locked = TRUE;
            }
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, rebroadcast failed\n");
          }

          tbuf--;
        }
        ////////////////////////
    	//Buffering of NONE MSG
    	////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NONE_MSG){

          none_msg_t* new_none = (none_msg_t*)call Packet.getPayload(&packet, sizeof(none_msg_t));
          
          if (new_none == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_none->msg_type = temp_buffer[tbuf-1].msg_type;
          new_none->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_none->app_id = temp_buffer[tbuf-1].app_id;
          new_none->period_id = temp_buffer[tbuf-1].period_id;
          new_none->r7 = temp_buffer[tbuf-1].r7;
          new_none->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(none_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NONE result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_none->result_owner_id, temp_buffer[tbuf-1].sender, new_none->period_id, new_none->app_id, new_none->r7, new_none->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NONE msg failed\n");
          }

          tbuf--;
        }
        ////////////////////////
    	//Buffering of PIGGY MSG
    	////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == PIGGY_MSG){

        	piggy_msg_t* new_piggy = (piggy_msg_t*)call Packet.getPayload(&packet, sizeof(piggy_msg_t));
	        if (new_piggy == NULL) {
	          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
	        }

	        new_piggy->msg_type = PIGGY_MSG;
	        new_piggy->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
	        new_piggy->remaining_space = temp_buffer[tbuf-1].remaining_space;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        new_piggy->period_id = temp_buffer[tbuf-1].period_id;
	        //Increase the hop count here
	        new_piggy->hop_count = temp_buffer[tbuf-1].hop_count + 1;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        for(i=0; i<PGSIZE; i++){
	        	new_piggy->node_id[i] = temp_buffer[tbuf-1].node_id[i];
	        	new_piggy->r7[i] = temp_buffer[tbuf-1].pg_r7[i];
	        	new_piggy->r8[i] = temp_buffer[tbuf-1].pg_r8[i];
	        }

	        if(!locked){
              if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
                dbg("RadioCountToLedsC", "Sent PIGGY RESULT to :%d with period id :%d, app id :%d , hop count:%d \n", 
            	temp_buffer[tbuf-1].sender, new_piggy->period_id, new_piggy->app_id, new_piggy->hop_count);

          		for(i=0; i<PGSIZE; i++){
            		dbg("RadioCountToLedsC", "It contains data from node :%d and r7 :%d, r8 :%d\n", 
            			new_piggy->node_id[i], new_piggy->r7[i], new_piggy->r8[i]);
          		}
                locked = TRUE;
              }
              else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
            }
            else{
          	  dbg("RadioCountToLedsC", "LOCKED, PIGGY msg failed\n");
            }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of NORMAL MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NORMAL_MSG){

          normal_msg_t* new_normal = (normal_msg_t*)call Packet.getPayload(&packet, sizeof(normal_msg_t));
          
          if (new_normal == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_normal->msg_type = temp_buffer[tbuf-1].msg_type;
          new_normal->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_normal->app_id = temp_buffer[tbuf-1].app_id;
          new_normal->period_id = temp_buffer[tbuf-1].period_id;
          new_normal->r7 = temp_buffer[tbuf-1].r7;
          new_normal->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(normal_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NORMAL result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_normal->result_owner_id, temp_buffer[tbuf-1].sender, new_normal->period_id, new_normal->app_id, new_normal->r7, new_normal->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NORMAL msg failed\n");
          }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of STATS MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == STATS_MSG){

          stats_msg_t* new_stats = (stats_msg_t*)call Packet.getPayload(&packet, sizeof(stats_msg_t));
          
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = temp_buffer[tbuf-1].msg_type;
          new_stats->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_stats->app_id = temp_buffer[tbuf-1].app_id;
          new_stats->period_id = temp_buffer[tbuf-1].period_id;
          //Increase the hop count here
	      new_stats->hop_count = temp_buffer[tbuf-1].hop_count + 1;
          new_stats->r7 = temp_buffer[tbuf-1].r7;
          new_stats->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a STATS result from :%d to :%d with period_id :%d, app_id :%d, hop count :%d and r7 :%d, r8 :%d\n",
              new_stats->result_owner_id, temp_buffer[tbuf-1].sender, new_stats->period_id, new_stats->app_id, new_stats->hop_count,
              new_stats->r7, new_stats->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, STATS msg failed\n");
          }

          tbuf--;
        }
    }
  }

  event void InstructionTimer.fired(){

    for (i=0; i<SIZE; i++){
      if(app[i].pending_time != 0){
        app[i].pending_time = app[i].pending_time - INSTRUCTION_RATE;
        //If the app's timer has expired
        if(app[i].pending_time == 0){
          if(!app[i].isActive){	
	        app[i].isActive = TRUE;
	        app[i].period_id = app[i].period_id + 1;

	        app[i].saved_pc = app[i].pc;
	        app[i].pc = 4 + app[i].app_bytes[1];
	        post runApp();
	          
	        dbg("VMDebugC","Inside instruction timer jumping from : %d to : %d\n",app[i].saved_pc,app[i].pc);
          }
          //The App is running another handler
          //Try again later
          else{
          	app[i].pending_time = INSTRUCTION_RATE;
          	dbg("VMDebugC","Timer bumped into Active App. Resetting Timer");
          }
        }
      }
    } 
  }


  event message_t* FloodReceive.receive(message_t* bufPtr, void* payload, uint8_t len) {
  
    if (len != sizeof(app_bytecode_msg_t)) {
      return bufPtr;
    }
    else {
      app_bytecode_msg_t* apm = (app_bytecode_msg_t*)payload;
      //dbg("RadioCountToLedsC", "Received packet id: %d:%d, from node %d.\n",apm->owner_id, apm->packet_id,apm->sender_id);
      
      if(apm->msg_type != APP_MSG && apm->msg_type != CANCEL_MSG){
        return bufPtr;
      }
      
      //Cancelation MSG handling
      if(apm->msg_type == CANCEL_MSG){
      	


      	for (i = 0; i < SIZE; i++){
      		if(apm->app_id == app[i].app_id){
      			app[i].app_id = 0;
		      	for(j = 0;j<10;j++){
		        	app[i].reg[j] = 0; 
		      	}
		      	for(j =0;j<50;j++){
		        	app[i].app_bytes[j] = 0;
		      	}
		      	app[i].pc = 0;
		      	app[i].saved_pc = 0;
		      	app[i].isActive = FALSE;
		      	app[i].pending_time = 0;
		      	app[i].pending_data = FALSE;
		      	app[i].has_msg_handler = FALSE;
		      	app[i].pending_msg = 0;
		      	app[i].agg_mode = 0;
		      	app[i].hop_count = 0;
		      	app[i].period_id = 0;
		      	app[i].sender = 0;
		      	app[i].regulate_time = 1;

		      	//Initialize local piggy
		      	local_piggy[i].remaining_space = PGSIZE;
		      //This denotes the NEXT packet that will be sent
		      	local_piggy[i].period_id = 1;
		      	for(j=0; j<PGSIZE; j++){
		        	local_piggy[i].node_id[j] = 0;
		        	local_piggy[i].pg_r7[j] = 0;
		        	local_piggy[i].pg_r8[j] = 0;
		       	}
		       	//Empty message buffer
		       	for(j=0; j<BUFF_SIZE; j++){
	    			if(msg_buffer[j].app_id == apm->app_id){
	    				msg_buffer[j].app_id = 0;
	    			}
	    		}

     			cancelation_packet_received = TRUE;
      			stored_app_id = apm->app_id;

      			dbg("RadioCountToLedsC","Received first CANCEL msg from :%d for app_id :%d. Rebroadcasting\n", apm->sender, apm->app_id);

      			backoff_time = call LocalTimer.get();
        		backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

        		call RebroadcastTimer.startOneShot(backoff_time);      			

      			break;
      		}

      	}

      	return bufPtr;
      }

      for (i = 0; i < SIZE; i++){
        //elegxos an exoume to app id sto topiko mas pinaka
          if(apm->app_id > stored[i][1]){ 

              xronos = call LocalTimer.get();
              //dbg("RadioCountToLedsC","Packet %d:%d ArrTime %d.\n",apm->owner_id,apm->packet_id,xronos);
              //Sort the routing table
              for(j=0; j<i; j++){
                stored[i-j][0]= stored[i-j-1][0];
                stored[i-j][1]= stored[i-j-1][1];
                stored[i-j][2]= stored[i-j-1][2];
              }

              //Put new element at top of routing table
              stored[0][0] = 1;
              stored[0][1] = apm->app_id;
              stored[0][2] = 1;
              //stored[0][2] = apm->round_id;
              stored_sender = apm->sender;

              //stored_agg_mode = apm->agg_mode;



              //Check for inactive queries on the table
              for(j = 0; j < SIZE; j++){
                if(app[j].app_id == 0){
                  dbg("RadioCountToLedsC","Received bytecode packet\n");
                  app[j].app_id = apm->app_id;
                  for(k = 0;k < 28;k++){
                    app[j].app_bytes[k] = apm->codebytes[k];
                  }
                  app[j].pc = 4;
                  app[j].isActive = TRUE;
                           
                  //Store and activate the query
                  app[j].app_id = apm->app_id;
                  app[j].sender = apm->sender;
                  app[j].period_id = 0;

                  //Check if msg handler exists
                  if(app[j].app_bytes[3] == 0){
                  	app[j].has_msg_handler = FALSE;
                  	app[j].agg_mode = NONE;
                  }
                  else{
                  	app[j].has_msg_handler = TRUE;
                  	app[j].agg_mode = NORMAL;		
                  }

                  post runApp(); 

                  break;
                }
                else if(j == (SIZE-1)){
                  dbg("RadioCountToLedsC","No space for query with id %d, discarded \n",apm->app_id);
                  /*ReCheck how the system reacts to a full query table*/
                  
                }
              }


              backoff_time = call LocalTimer.get();
              backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

              call RebroadcastTimer.startOneShot(backoff_time);
          
              return bufPtr;
            }
            
            else{
              //Discard the message
              //dbg("RadioCountToLedsC", "Packet id: %d:%d already received\n",apm->owner_id,apm->packet_id);     
              return bufPtr;
            } 
        }
    }
    return bufPtr;
  }

  event message_t* Receive.receive(message_t* bufPtr, void* payload, uint8_t len){

  	if(len != sizeof(none_msg_t) && len != sizeof(piggy_msg_t) && len != sizeof(normal_msg_t) && len != sizeof(stats_msg_t)){
  		return bufPtr;
  	}
  	else{

  		none_msg_t* nm = (none_msg_t*)payload;
  	

	  	if(nm->msg_type != NONE_MSG && nm->msg_type != PIGGY_MSG && nm->msg_type != NORMAL_MSG && nm->msg_type != STATS_MSG){
	        return bufPtr;
	    }

	  	///////////////////////
	  	//Handling of NONE MSG
	  	///////////////////////
	  	if(nm->msg_type == NONE_MSG){

	  		dbg("RadioCountToLedsC", "Receive RESULT r7:%d and r8:%d from node :%d, from period :%d, for app id :%d \n", 
	  			nm->r7, nm->r8, nm->result_owner_id, nm->period_id ,nm->app_id);
	  		if(TOS_NODE_ID == 1){
	  			dbg("RadioCountToLedsC","Received at source\n");
	  			return bufPtr;
	  		}

	        for(j=0; j<SIZE; j++){
	            if(app[j].app_id == nm->app_id){
	              
	              
	              none_msg_t* new_none = (none_msg_t*)call Packet.getPayload(&packet, sizeof(none_msg_t));
	              if (new_none == NULL) {
	                dbg("RadioCountToLedsC", "Packet not allocated.\n");  
	              }

	              if(!locked){

	                new_none->msg_type = NONE_MSG;
	                new_none->result_owner_id = nm->result_owner_id;
	                new_none->app_id = nm->app_id;
	                new_none->period_id = nm->period_id;
	                new_none->r7 = nm->r7;
	                new_none->r8 = nm->r8;

	              
	                if (call Send.send(app[j].sender, &packet, sizeof(none_msg_t)) == SUCCESS) {
	                  dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, app id :%d and r7 :%d, r8 :%d\n",
	                    new_none->result_owner_id, app[j].sender, new_none->period_id, new_none->app_id, new_none->r7, new_none->r8);
	                  locked = TRUE;
	                } 
	              }
	              else{
	                dbg("RadioCountToLedsC", "Using temp buffer for result from :%d to :%d with period_id :%d, app id :%d and r7 :%d, r8 :%d\n",
	                    new_none->result_owner_id, app[j].sender, new_none->period_id, new_none->app_id, new_none->r7, new_none->r8);

	                if(tbuf < BUFF_SIZE){
	                  temp_buffer[tbuf].msg_type = NONE_MSG;
	                  temp_buffer[tbuf].result_owner_id = nm->result_owner_id;
	                  temp_buffer[tbuf].app_id = nm->app_id;
	                  temp_buffer[tbuf].period_id = nm->period_id;
	                  temp_buffer[tbuf].r7 = nm->r7;
	                  temp_buffer[tbuf].r8 = nm->r8;

	                  temp_buffer[tbuf].sender = app[j].sender;

	                  tbuf++;
	                }
	                else{
	                  dbg("RadioCountToLedsC", "temp buffer FULL\n");
	                }
	              }
	            }
	            else{
	            	//We dont have the App
	            	//Cancelation Broadcast
	            	if(j == SIZE-1 && TOS_NODE_ID!=1){
	            		app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    					if (new_packet == NULL) {
				      		dbg("RadioCountToLedsC", "Packet not allocated.\n");  
				    	}
				    
				    	new_packet->msg_type = CANCEL_MSG;
				    	new_packet->app_id = nm->app_id;
				    	new_packet->sender = TOS_NODE_ID;

				    	if(!locked){
				      		if (call FloodSend.send(nm->result_owner_id, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
				      			dbg("RadioCountToLedsC", "Cancelation packet for app id :%d sent to :%d.\n", nm->app_id,nm->result_owner_id);
				        		locked = TRUE;
				      		}
				    	}
	            	}
	            }
	            return bufPtr;
	        }
	  	}
	  	////////////////////////////////////////////////////
	    //PIGGY msg handling
	    ///////////////////////////////////////////////////
	    else if(nm->msg_type == PIGGY_MSG){

	        piggy_msg_t* pgm = (piggy_msg_t*)payload;

	        dbg("RadioCountToLedsC", "Receive PIGGY RESULT with period id :%d, app id :%d , hop count:%d \n", 
            pgm->period_id, pgm->app_id, pgm->hop_count);

          	for(i=0; i<PGSIZE; i++){
            	dbg("RadioCountToLedsC", "It contains data from node :%d and r7 :%d, r8 :%d\n", pgm->node_id[i], pgm->r7[i], pgm->r8[i]);
          	}

          	//handling packet for cancelled app
          	for(i=0;i<SIZE;i++){
          		if(pgm->app_id == app[i].app_id){
          			break;
          		}
          		else{
          			if(i == SIZE-1 && TOS_NODE_ID!=1){
	            		app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    					if (new_packet == NULL) {
				      		dbg("RadioCountToLedsC", "Packet not allocated.\n");  
				    	}
				    
				    	new_packet->msg_type = CANCEL_MSG;
				    	new_packet->app_id = pgm->app_id;
				    	new_packet->sender = TOS_NODE_ID;

				    	if(!locked){
				      		if (call FloodSend.send(pgm->result_owner_id, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
				      			dbg("RadioCountToLedsC", "Cancelation packet for app id :%d sent to :%d.\n", pgm->app_id,pgm->result_owner_id);
				        		locked = TRUE;
				      		}
				    	}
	            		return bufPtr;
	            	}
          		}
          	}

	        //Full Packet
	        if(pgm->remaining_space == 0){

	          dbg("RadioCountToLedsC", "Using temp buffer for full Piggy\n");
	          if(tbuf < BUFF_SIZE){
	            temp_buffer[tbuf].msg_type = pgm->msg_type;
	            temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
	            temp_buffer[tbuf].remaining_space = pgm->remaining_space;
	            temp_buffer[tbuf].app_id = pgm->app_id;
	            temp_buffer[tbuf].period_id = pgm->period_id;
	            for(i=0; i<PGSIZE; i++){
	              temp_buffer[tbuf].node_id[i] = pgm->node_id[i];
	              temp_buffer[tbuf].pg_r7[i] = pgm->r7[i];
	              temp_buffer[tbuf].pg_r8[i] = pgm->r8[i];
	            }

	            //Will increase before sending
	            temp_buffer[tbuf].hop_count = pgm->hop_count;

	            for(j=0; j<SIZE; j++){
	            	if(app[j].app_id == pgm->app_id){
	            		temp_buffer[tbuf].sender = app[j].sender;

	            		//Adjust hop counts
	            		if(pgm->hop_count > app[j].hop_count){
	            			app[j].hop_count = pgm->hop_count;
	            		}
	            		
	            		break;
	            	}
	            }

	            tbuf++;

	            if(! call BufferTimer.isRunning()){
	            	backoff_time = call LocalTimer.get();
	            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

	            	call BufferTimer.startOneShot(backoff_time);
          	  	}
	          }
	          else{
	            dbg("RadioCountToLedsC", "temp buffer FULL\n");
	          }
 
	          return bufPtr;
	        }
	        
	        for(j=0; j<SIZE; j++){
	          if(app[j].app_id == pgm->app_id){
	            
	            //This is from an older period, just send
	            if(pgm->period_id < local_piggy[j].period_id){
	              
	              //Adjust regulate time, because we didnt wait enough
	              app[j].regulate_time++;

	              //Adjust hop_count
	              if(pgm->hop_count > app[j].hop_count){
	            	app[j].hop_count = pgm->hop_count;
	              }

	              dbg("RadioCountToLedsC", "Using temp buffer for outdated Piggy\n");
	              if(tbuf < BUFF_SIZE){
	                
	                temp_buffer[tbuf].msg_type = pgm->msg_type;
	                temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
		            temp_buffer[tbuf].remaining_space = pgm->remaining_space;
		            temp_buffer[tbuf].app_id = pgm->app_id;
		            temp_buffer[tbuf].period_id = pgm->period_id;
		            for(i=0; i<PGSIZE; i++){
		              temp_buffer[tbuf].node_id[i] = pgm->node_id[i];
		              temp_buffer[tbuf].pg_r7[i] = pgm->r7[i];
		              temp_buffer[tbuf].pg_r8[i] = pgm->r8[i];
	              	}

	            	//Will increase before sending
	           		temp_buffer[tbuf].hop_count = pgm->hop_count;

	                temp_buffer[tbuf].sender = app[j].sender;

	                if(! call BufferTimer.isRunning()){
		            	backoff_time = call LocalTimer.get();
		            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

		            	call BufferTimer.startOneShot(backoff_time);
          	  		}

	                tbuf++;
	              }
	              else{
	                dbg("RadioCountToLedsC", "temp buffer FULL\n");
	              }
	              
	              return bufPtr;
	            }
	            //This is from the correct period
	            else{

	              //Adjust hop_count
		          if(pgm->hop_count > app[j].hop_count){
		            app[j].hop_count = pgm->hop_count;
		          }


	              //Arrived packet fits in local
	              if((PGSIZE - pgm->remaining_space) <= local_piggy[j].remaining_space){

	                //Add to appropriate place
	                for(i=(PGSIZE-local_piggy[j].remaining_space); i<PGSIZE; i++){
	                  local_piggy[j].node_id[i] = pgm->node_id[i-(PGSIZE-local_piggy[j].remaining_space)];
	                  local_piggy[j].pg_r7[i] = pgm->r7[i-(PGSIZE-local_piggy[j].remaining_space)];
	                  local_piggy[j].pg_r8[i] = pgm->r8[i-(PGSIZE-local_piggy[j].remaining_space)];
	                }

	                local_piggy[j].remaining_space = local_piggy[j].remaining_space - (PGSIZE - pgm->remaining_space);

	                

	                if(local_piggy[j].remaining_space == 0){
	                  //Send if i filled a packet

	                  dbg("RadioCountToLedsC", "Using temp buffer, just filled a Piggy\n");
	                  if(tbuf < BUFF_SIZE){
	                    temp_buffer[tbuf].msg_type = PIGGY_MSG;
	                    temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
	                    temp_buffer[tbuf].remaining_space = local_piggy[j].remaining_space;
	                    temp_buffer[tbuf].app_id = app[j].app_id;
	                    temp_buffer[tbuf].period_id = local_piggy[j].period_id;
	                    for(i=0; i<PGSIZE; i++){
	                      temp_buffer[tbuf].node_id[i] = local_piggy[j].node_id[i];
	                      temp_buffer[tbuf].pg_r7[i] = local_piggy[j].pg_r7[i];
	                      temp_buffer[tbuf].pg_r8[i] = local_piggy[j].pg_r8[i];
	                    }

	                    //Will increase before sending
	           			temp_buffer[tbuf].hop_count = pgm->hop_count;

	                    temp_buffer[tbuf].sender = app[j].sender;

	                    tbuf++;

	                    if(! call BufferTimer.isRunning()){
			            	backoff_time = call LocalTimer.get();
			            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

			            	call BufferTimer.startOneShot(backoff_time);
          	  			}
	                  }
	                  else{
	                    dbg("RadioCountToLedsC", "temp buffer FULL\n");
	                  }
	                  
	                  //Empty local buffer
	                  local_piggy[j].remaining_space = PGSIZE;
	                  for(i=0; i<PGSIZE; i++){
	                    local_piggy[j].node_id[i] = 0;
	                    local_piggy[j].pg_r7[i] = 0;
	                    local_piggy[j].pg_r8[i] = 0;
	                  }

	                  return bufPtr;
	                }
	              }
	              //If arrived packet does not fit in local
	              else{

	                //Fill packet until full then send
	                
	                dbg("RadioCountToLedsC", "Using temp buffer cause Piggy packet didnt fit\n");
	                if(tbuf < BUFF_SIZE){
	                  temp_buffer[tbuf].msg_type = PIGGY_MSG;
	                  temp_buffer[tbuf].result_owner_id = TOS_NODE_ID;
	                  temp_buffer[tbuf].remaining_space = 0;
	                  temp_buffer[tbuf].app_id = app[j].app_id;
	                  temp_buffer[tbuf].period_id = local_piggy[j].period_id;

	                  for(i=0; i<PGSIZE; i++){
	                    //Fill packet from local first
	                    if(i <(PGSIZE - local_piggy[j].remaining_space)){
	                      temp_buffer[tbuf].node_id[i] = local_piggy[j].node_id[i];
	                      temp_buffer[tbuf].pg_r7[i] = local_piggy[j].pg_r7[i];
	                      temp_buffer[tbuf].pg_r8[i] = local_piggy[j].pg_r8[i];
	                    }
	                    //From received packet until full
	                    else{
	                      temp_buffer[tbuf].node_id[i] = pgm->node_id[i-(PGSIZE - local_piggy[j].remaining_space)];
	                      temp_buffer[tbuf].pg_r7[i] = pgm->r7[i-(PGSIZE - local_piggy[j].remaining_space)];
	                      temp_buffer[tbuf].pg_r8[i] = pgm->r8[i-(PGSIZE - local_piggy[j].remaining_space)];
	                    } 
	                  }

	                  //Then fill local with remainder of received
	                  for(i=0; i<PGSIZE; i++){
	                    if(i < (PGSIZE -(pgm->remaining_space + local_piggy[j].remaining_space))){
	                      local_piggy[j].node_id[i] = pgm->node_id[i + local_piggy[j].remaining_space];
	                      local_piggy[j].pg_r7[i] = pgm->r7[i + local_piggy[j].remaining_space];
	                      local_piggy[j].pg_r8[i] = pgm->r8[i + local_piggy[j].remaining_space];
	                    }
	                    else{
	                      local_piggy[j].node_id[i] = 0;
	                      local_piggy[j].pg_r7[i] = 0;
	                      local_piggy[j].pg_r8[i] = 0;
	                    }
	                  }
	                  
	                  //Will increase before sending
	           		  temp_buffer[tbuf].hop_count = pgm->hop_count;

	                  local_piggy[j].remaining_space = pgm->remaining_space + local_piggy[j].remaining_space;
	                      
	                  temp_buffer[tbuf].sender = app[j].sender;

	                  tbuf++;

	                  if(! call BufferTimer.isRunning()){
			            backoff_time = call LocalTimer.get();
			            backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

			            call BufferTimer.startOneShot(backoff_time);
          	  		  }
	                }
	                else{
	                  dbg("RadioCountToLedsC", "temp buffer FULL\n");
	                }
	                
	                return bufPtr;
	              }
	            }
	            break;
	          }
	        }  
	    }
	    //////////////////////////
	  	//Handling of NORMAL MSG
	  	//////////////////////////
	    else if(nm->msg_type == NORMAL_MSG){

	    	normal_msg_t* nrm = (normal_msg_t*)payload;

	    	dbg("RadioCountToLedsC", "Receive NORMAL RESULT r7:%d and r8:%d from node :%d, from period :%d, for app id :%d \n", 
	  			nrm->r7, nrm->r8, nrm->result_owner_id, nrm->period_id ,nrm->app_id);

	    	for(j=0; j<SIZE; j++){
	    		if(app[j].app_id == nrm->app_id){
	    			//Check if the msg handler can be executed immediately
	    			//If no handler is Active
	    			if(!app[j].isActive){

	    				app[j].isActive = TRUE;
	    				app[j].reg[8] = nrm->r7;
	    				app[j].reg[9] = nrm->r8;
	    				
	    				app[j].saved_pc = app[j].pc;
				        app[j].pc = 4 + app[j].app_bytes[1] + app[j].app_bytes[2];

				        dbg("VMDebugC","App inactive starting msg handler. Jumping from : %d to : %d\n",app[j].saved_pc, app[j].pc);

				        post runApp();
	    			}
	    			//Store in message buffer
	    			else{
	    				for(i=0; i<BUFF_SIZE; i++){
	    				  //Find empty position
	    				  if(msg_buffer[i].app_id == 0){

		    				app[j].pending_msg = app[j].pending_msg + 1;

		    				msg_buffer[i].msg_type = nrm->msg_type;
		    				msg_buffer[i].result_owner_id = nrm->result_owner_id;
		    				msg_buffer[i].app_id = nrm->app_id;
		    				msg_buffer[i].period_id = nrm->period_id;
		    				msg_buffer[i].r7 = nrm->r7;
		    				msg_buffer[i].r8 = nrm->r8;

		    				break;
		    			  }
		    			  else if(i == (BUFF_SIZE-1)){
		    			  	dbg("RadioCountToLedsC", "Message Buffer FULL, discarding msg");
		    			  }
		    			} 
	    			}

	    			return bufPtr;	
	    		}
	    		else{
	    			if(j == SIZE-1 && TOS_NODE_ID!=1){
	            		app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    					if (new_packet == NULL) {
				      		dbg("RadioCountToLedsC", "Packet not allocated.\n");  
				    	}
				    
				    	new_packet->msg_type = CANCEL_MSG;
				    	new_packet->app_id = nrm->app_id;
				    	new_packet->sender = TOS_NODE_ID;

				    	if(!locked){
				      		if (call FloodSend.send(nrm->result_owner_id, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
				      			dbg("RadioCountToLedsC", "Cancelation packet for app id :%d sent to :%d.\n", nrm->app_id,nrm->result_owner_id);
				        		locked = TRUE;
				      		}
				    	}
				    	return bufPtr;
	            	}
	    		}
	    	}
	    }
	    //////////////////////////
	  	//Handling of STATS MSG
	  	//////////////////////////
	    else if(nm->msg_type == STATS_MSG){
	    	stats_msg_t* stm = (stats_msg_t*)payload;

	    	dbg("RadioCountToLedsC", "Receive STATS RESULT r7:%d and r8:%d from node :%d, from period :%d, for app id :%d \n", 
	  			stm->r7, stm->r8, stm->result_owner_id, stm->period_id ,stm->app_id);

	    	for(j=0; j<SIZE; j++){
	    		if(app[j].app_id == stm->app_id){

	    			//Adjust hop_count
			        if(stm->hop_count > app[j].hop_count){
			          app[j].hop_count = stm->hop_count;
			        }

	    			//If this is from an older period, just send
	    			if(stm->period_id <= app[j].period_id){

	    				stats_msg_t* new_stats = (stats_msg_t*)call Packet.getPayload(&packet, sizeof(stats_msg_t));
			            if (new_stats == NULL) {
			              dbg("RadioCountToLedsC", "Packet not allocated.\n");  
			            }

			            if(!locked){

			              new_stats->msg_type = STATS_MSG;
			              new_stats->result_owner_id = stm->result_owner_id;
			              new_stats->app_id = stm->app_id;
			              new_stats->period_id = stm->period_id;
			              //Adjust hop count
			              new_stats->hop_count = stm->period_id + 1;
			              new_stats->r7 = stm->r7;
			              new_stats->r8 = stm->r8;

			              
			              if (call Send.send(app[j].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {
			                dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, app id :%d, hop_count :%d and r7 :%d, r8 :%d\n",
			                  new_stats->result_owner_id, app[j].sender, new_stats->period_id, new_stats->app_id, new_stats->hop_count, 
			                  new_stats->r7, new_stats->r8);
			                locked = TRUE;
			              } 
			            }
			            else{
			              dbg("RadioCountToLedsC", "Using temp buffer for result from :%d to :%d with period_id :%d, app id :%d, hop_count :%d and r7 :%d, r8 :%d\n",
			                  new_stats->result_owner_id, app[j].sender, new_stats->period_id, new_stats->app_id, new_stats->hop_count, 
			                  new_stats->r7, new_stats->r8);

			              if(tbuf < BUFF_SIZE){
			                temp_buffer[tbuf].msg_type = STATS_MSG;
			                temp_buffer[tbuf].result_owner_id = stm->result_owner_id;
			                temp_buffer[tbuf].app_id = stm->app_id;
			                temp_buffer[tbuf].period_id = stm->period_id;
			                //Will increase when message is sent
			                temp_buffer[tbuf].hop_count = stm->hop_count;
			                temp_buffer[tbuf].r7 = stm->r7;
			                temp_buffer[tbuf].r8 = stm->r8;

			                temp_buffer[tbuf].sender = app[j].sender;

			                tbuf++;

			                if(! call BufferTimer.isRunning()){
				            	backoff_time = call LocalTimer.get();
				            	backoff_time = ((backoff_time * TOS_NODE_ID) % 55);

				            	call BufferTimer.startOneShot(backoff_time);
	          	  		    }
			              }
			              else{
			                dbg("RadioCountToLedsC", "temp buffer FULL\n");
			              }
			            }

	    				return bufPtr;
	    			}
	    			//Check if the msg handler can be executed immediately
	    			//If no handler is Active
	    			if(!app[j].isActive){

	    				app[j].isActive = TRUE;
	    				app[j].reg[8] = stm->r7;
	    				app[j].reg[9] = stm->r8;
	    				
	    				app[j].saved_pc = app[j].pc;
				        app[j].pc = 4 + app[j].app_bytes[1] + app[j].app_bytes[2];

				        dbg("VMDebugC","App inactive starting msg handler. Jumping from : %d to : %d\n",app[j].saved_pc, app[j].pc);

				        post runApp();
	    			}
	    			//Store in message buffer
	    			else{
	    				for(i=0; i<BUFF_SIZE; i++){
	    				  //Find empty position
	    				  if(msg_buffer[i].app_id == 0){

		    				app[j].pending_msg = app[j].pending_msg + 1;

		    				msg_buffer[i].msg_type = stm->msg_type;
		    				msg_buffer[i].result_owner_id = stm->result_owner_id;
		    				msg_buffer[i].app_id = stm->app_id;
		    				msg_buffer[i].period_id = stm->period_id;
		    				msg_buffer[i].r7 = stm->r7;
		    				msg_buffer[i].r8 = stm->r8;

		    				break;
		    			  }
		    			  else if(i == (BUFF_SIZE-1)){
		    			  	dbg("RadioCountToLedsC", "Message Buffer FULL, discarding msg");
		    			  }
		    			} 
	    			}

	    			return bufPtr;	
	    		}
	    		//Dont have app
	    		//Send Cancellation
	    		else{
	    			if(j == SIZE-1 && TOS_NODE_ID!=1){
	            		app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    					if (new_packet == NULL) {
				      		dbg("RadioCountToLedsC", "Packet not allocated.\n");  
				    	}
				    
				    	new_packet->msg_type = CANCEL_MSG;
				    	new_packet->app_id = stm->app_id;
				    	new_packet->sender = TOS_NODE_ID;

				    	if(!locked){
				      		if (call FloodSend.send(stm->result_owner_id, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
				      			dbg("RadioCountToLedsC", "Cancelation packet for app id :%d sent to :%d.\n", stm->app_id,stm->result_owner_id);
				        		locked = TRUE;
				      		}
				    	}
				    	return bufPtr;
	            	}
	    		}
	    	}
	    }
	}
  	return bufPtr;
  }

  event void FloodSend.sendDone(message_t* bufPtr, error_t error) {
    if (&packet == bufPtr){
      locked = FALSE;

      if(tbuf!=0){
        //////////////////////////
        //Buffering of APP MSG
        //////////////////////////
        if(temp_buffer[tbuf-1].msg_type == APP_MSG || temp_buffer[tbuf-1].msg_type == CANCEL_MSG){

          app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
          if (new_packet == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }
          
          new_packet->msg_type = temp_buffer[tbuf-1].msg_type;
          new_packet->app_id = temp_buffer[tbuf-1].app_id;
          new_packet->sender = temp_buffer[tbuf-1].sender;
          for(k = 0;k < 28;k++){
              new_packet->codebytes[k] = temp_buffer[tbuf-1].codebytes[k];
          }
          
          if(!locked){
            if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d rebroadcast sent.\n",new_packet->app_id);
              locked = TRUE;
            }
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED rebroadcast failed\n");
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of NONE MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NONE_MSG){

          none_msg_t* new_none = (none_msg_t*)call Packet.getPayload(&packet, sizeof(none_msg_t));
          
          if (new_none == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_none->msg_type = temp_buffer[tbuf-1].msg_type;
          new_none->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_none->app_id = temp_buffer[tbuf-1].app_id;
          new_none->period_id = temp_buffer[tbuf-1].period_id;
          new_none->r7 = temp_buffer[tbuf-1].r7;
          new_none->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(none_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NONE result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_none->result_owner_id, temp_buffer[tbuf-1].sender, new_none->period_id, new_none->app_id, new_none->r7, new_none->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NONE msg failed\n");
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of PIGGY MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == PIGGY_MSG){

        	piggy_msg_t* new_piggy = (piggy_msg_t*)call Packet.getPayload(&packet, sizeof(piggy_msg_t));
	        if (new_piggy == NULL) {
	          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
	        }

	        new_piggy->msg_type = PIGGY_MSG;
	        new_piggy->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
	        new_piggy->remaining_space = temp_buffer[tbuf-1].remaining_space;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        new_piggy->period_id = temp_buffer[tbuf-1].period_id;
	        //Increase the hop count here
	        new_piggy->hop_count = temp_buffer[tbuf-1].hop_count + 1;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        for(i=0; i<PGSIZE; i++){
	        	new_piggy->node_id[i] = temp_buffer[tbuf-1].node_id[i];
	        	new_piggy->r7[i] = temp_buffer[tbuf-1].pg_r7[i];
	        	new_piggy->r8[i] = temp_buffer[tbuf-1].pg_r8[i];
	        }

	        if(!locked){
              if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
 	        	dbg("RadioCountToLedsC", "Sent PIGGY RESULT to :%d with period id :%d, app id :%d , hop count:%d \n", 
            	temp_buffer[tbuf-1], new_piggy->period_id, new_piggy->app_id, new_piggy->hop_count);

          		for(i=0; i<PGSIZE; i++){
            		dbg("RadioCountToLedsC", "It contains data from node :%d and r7 :%d, r8 :%d\n", 
            			new_piggy->node_id[i], new_piggy->r7[i], new_piggy->r8[i]);
          		}

                locked = TRUE;
              }
              else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
            }
            else{
          	  dbg("RadioCountToLedsC", "LOCKED, PIGGY msg failed\n");
            }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of NORMAL MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NORMAL_MSG){

          normal_msg_t* new_normal = (normal_msg_t*)call Packet.getPayload(&packet, sizeof(normal_msg_t));
          
          if (new_normal == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_normal->msg_type = temp_buffer[tbuf-1].msg_type;
          new_normal->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_normal->app_id = temp_buffer[tbuf-1].app_id;
          new_normal->period_id = temp_buffer[tbuf-1].period_id;
          new_normal->r7 = temp_buffer[tbuf-1].r7;
          new_normal->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(normal_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NORMAL result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_normal->result_owner_id, temp_buffer[tbuf-1].sender, new_normal->period_id, new_normal->app_id, new_normal->r7, new_normal->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NORMAL msg failed\n");
          }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of STATS MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == STATS_MSG){

          stats_msg_t* new_stats = (stats_msg_t*)call Packet.getPayload(&packet, sizeof(stats_msg_t));
          
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = temp_buffer[tbuf-1].msg_type;
          new_stats->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_stats->app_id = temp_buffer[tbuf-1].app_id;
          new_stats->period_id = temp_buffer[tbuf-1].period_id;
          //Increase the hop count here
	      new_stats->hop_count = temp_buffer[tbuf-1].hop_count + 1;
          new_stats->r7 = temp_buffer[tbuf-1].r7;
          new_stats->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a STATS result from :%d to :%d with period_id :%d, app_id :%d, hop count :%d and r7 :%d, r8 :%d\n",
              new_stats->result_owner_id, temp_buffer[tbuf-1].sender, new_stats->period_id, new_stats->app_id, new_stats->hop_count,
              new_stats->r7, new_stats->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, STATS msg failed\n");
          }

          tbuf--;
        }
      }
    }
  }

  event void Send.sendDone(message_t* bufPtr, error_t error) {
    if (&packet == bufPtr){
      locked = FALSE;

      if(tbuf!=0){
        //////////////////////////
        //Buffering of APP MSG
        //////////////////////////
        if(temp_buffer[tbuf-1].msg_type == APP_MSG || temp_buffer[tbuf-1].msg_type == CANCEL_MSG){

          app_bytecode_msg_t* new_packet = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
          if (new_packet == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }
          
          new_packet->msg_type = temp_buffer[tbuf-1].msg_type;
          new_packet->app_id = temp_buffer[tbuf-1].app_id;
          new_packet->sender = temp_buffer[tbuf-1].sender;
          for(k = 0;k < 28;k++){
              new_packet->codebytes[k] = temp_buffer[tbuf-1].codebytes[k];
          }
          
          if(!locked){
            if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d rebroadcast sent.\n",new_packet->app_id);
              locked = TRUE;
            }
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED rebroadcast failed\n");
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of NONE MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NONE_MSG){

          none_msg_t* new_none = (none_msg_t*)call Packet.getPayload(&packet, sizeof(none_msg_t));
          
          if (new_none == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_none->msg_type = temp_buffer[tbuf-1].msg_type;
          new_none->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_none->app_id = temp_buffer[tbuf-1].app_id;
          new_none->period_id = temp_buffer[tbuf-1].period_id;
          new_none->r7 = temp_buffer[tbuf-1].r7;
          new_none->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(none_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NONE result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_none->result_owner_id, temp_buffer[tbuf-1].sender, new_none->period_id, new_none->app_id, new_none->r7, new_none->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NONE msg failed\n");
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of PIGGY MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == PIGGY_MSG){

        	piggy_msg_t* new_piggy = (piggy_msg_t*)call Packet.getPayload(&packet, sizeof(piggy_msg_t));
	        if (new_piggy == NULL) {
	          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
	        }

	        new_piggy->msg_type = PIGGY_MSG;
	        new_piggy->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
	        new_piggy->remaining_space = temp_buffer[tbuf-1].remaining_space;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        new_piggy->period_id = temp_buffer[tbuf-1].period_id;
	        //Increase the hop count here
	        new_piggy->hop_count = temp_buffer[tbuf-1].hop_count + 1;
	        new_piggy->app_id = temp_buffer[tbuf-1].app_id;
	        for(i=0; i<PGSIZE; i++){
	        	new_piggy->node_id[i] = temp_buffer[tbuf-1].node_id[i];
	        	new_piggy->r7[i] = temp_buffer[tbuf-1].pg_r7[i];
	        	new_piggy->r8[i] = temp_buffer[tbuf-1].pg_r8[i];
	        }

	        if(!locked){
              if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
                dbg("RadioCountToLedsC", "Sent PIGGY RESULT to :%d with period id :%d, app id :%d , hop count:%d \n", 
            	temp_buffer[tbuf-1].sender, new_piggy->period_id, new_piggy->app_id, new_piggy->hop_count);

          		for(i=0; i<PGSIZE; i++){
            		dbg("RadioCountToLedsC", "It contains data from node :%d and r7 :%d, r8 :%d\n", new_piggy->node_id[i], new_piggy->r7[i], new_piggy->r8[i]);
          		}
                locked = TRUE;
              }
              else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
            }
            else{
          	  dbg("RadioCountToLedsC", "LOCKED, PIGGY msg failed\n");
            }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of NORMAL MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NORMAL_MSG){

          normal_msg_t* new_normal = (normal_msg_t*)call Packet.getPayload(&packet, sizeof(normal_msg_t));
          
          if (new_normal == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_normal->msg_type = temp_buffer[tbuf-1].msg_type;
          new_normal->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_normal->app_id = temp_buffer[tbuf-1].app_id;
          new_normal->period_id = temp_buffer[tbuf-1].period_id;
          new_normal->r7 = temp_buffer[tbuf-1].r7;
          new_normal->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(normal_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a NORMAL result from :%d to :%d with period_id :%d, app_id :%d and r7 :%d, r8 :%d\n",
              new_normal->result_owner_id, temp_buffer[tbuf-1].sender, new_normal->period_id, new_normal->app_id, new_normal->r7, new_normal->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, NORMAL msg failed\n");
          }

          tbuf--;
        }
        //////////////////////////
    	//Buffering of STATS MSG
    	//////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == STATS_MSG){

          stats_msg_t* new_stats = (stats_msg_t*)call Packet.getPayload(&packet, sizeof(stats_msg_t));
          
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = temp_buffer[tbuf-1].msg_type;
          new_stats->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_stats->app_id = temp_buffer[tbuf-1].app_id;
          new_stats->period_id = temp_buffer[tbuf-1].period_id;
          //Increase the hop count here
	      new_stats->hop_count = temp_buffer[tbuf-1].hop_count + 1;
          new_stats->r7 = temp_buffer[tbuf-1].r7;
          new_stats->r8 = temp_buffer[tbuf-1].r8;
              
          if(!locked){
            if (call Send.send(temp_buffer[tbuf-1].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a STATS result from :%d to :%d with period_id :%d, app_id :%d, hop count :%d and r7 :%d, r8 :%d\n",
              new_stats->result_owner_id, temp_buffer[tbuf-1].sender, new_stats->period_id, new_stats->app_id, new_stats->hop_count,
              new_stats->r7, new_stats->r8);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }
          else{
          	dbg("RadioCountToLedsC", "LOCKED, STATS msg failed\n");
          }

          tbuf--;
        }
      }
    }
  }


  event void DemoRead.readDone(error_t result, uint16_t data){

    if(result == SUCCESS){
      if (data > 127 || data < -127){
        //data = 127;
        data = TOS_NODE_ID;
      }
      cached_data = data;
      cached_data_timestamp = call LocalTimer.get();
    }
    lockedRead = FALSE;
    dbg("VMDebugC", "New data :%d.\n",cached_data);

    for(i=0; i<SIZE; i++){
      if(app[i].pending_data){
        app[i].pending_data = FALSE;
        app[i].isActive = TRUE;
        post runApp();
      }
    }
  }

  //This is not being used
  event void ContextTimer.fired(){
    loop_count = 0;

    //Round Robin Apps
    do{
      ctx_id++;
      if(ctx_id == SIZE){ctx_id = 0;}
      loop_count = loop_count + 1;
    }while((!app[ctx_id].isActive) && (loop_count<SIZE));

  }

}





