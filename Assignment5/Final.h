#ifndef Final_H
#define Final_H


#define TOSH_DATA_LENGTH 31

typedef nx_struct app_bytecode_msg {

	nx_uint8_t msg_type;
	nx_uint8_t app_id;
	nx_uint8_t sender;
	nx_uint8_t codebytes[TOSH_DATA_LENGTH-3];

} app_bytecode_msg_t;

typedef nx_struct none_msg {
	nx_uint8_t msg_type;
  	nx_uint8_t result_owner_id;
  	nx_uint8_t app_id;
  	nx_uint16_t period_id;
  	nx_int8_t r7;
  	nx_int8_t r8;
} none_msg_t;

typedef nx_struct normal_msg {
	nx_uint8_t msg_type;
  	nx_uint8_t result_owner_id;
  	nx_uint8_t app_id;
  	nx_uint16_t period_id;
  	nx_int8_t r7;
  	nx_int8_t r8;
} normal_msg_t;

typedef nx_struct piggy_msg{
	nx_uint8_t msg_type;
	nx_uint8_t result_owner_id;
	nx_uint8_t remaining_space;
	nx_uint8_t app_id;
	nx_uint16_t period_id;
	nx_uint8_t hop_count;
	nx_uint8_t node_id[5];
	nx_int8_t r7[5];
	nx_int8_t r8[5];
} piggy_msg_t;

typedef nx_struct stats_msg {
	nx_uint8_t msg_type;
  	nx_uint8_t result_owner_id;
  	nx_uint8_t app_id;
  	nx_uint16_t period_id;
  	nx_uint8_t hop_count;
  	nx_int8_t r7;
  	nx_int8_t r8;
} stats_msg_t;

typedef struct application {
	uint8_t app_id;
	int8_t reg[10];
	uint8_t app_bytes[50];
	uint8_t pc;
	uint8_t saved_pc;
	bool isActive;
	uint32_t pending_time;
	bool pending_data;

	uint8_t sender;
	uint16_t period_id;
	bool has_msg_handler;
	uint8_t pending_msg;
	uint8_t agg_mode;
	//uint16_t data;
	//uint8_t outdated_count;
	uint8_t hop_count;
	uint8_t regulate_time;

} application_t;

typedef struct stored_buffer{
	//SIZE : 30 Bytes

	//These are general purpose fields
	uint8_t msg_type;
	uint8_t app_id;
  	uint8_t sender;
  	uint8_t codebytes[24];

  	//For sending of results
  	uint8_t agg_mode;
  	int8_t r7;
  	int8_t r8;
  	uint16_t period_id;
  	uint8_t result_owner_id;

  	//For piggy
  	uint8_t remaining_space;
  	uint8_t hop_count;
	uint8_t node_id[5];
	int8_t pg_r7[5];
	int8_t pg_r8[5];

} stored_buffer_t;

typedef struct stored_msg{
	uint8_t msg_type;
  	uint8_t result_owner_id;
  	uint8_t app_id;
  	uint16_t period_id;
  	uint8_t hop_count;
  	int8_t r7;
  	int8_t r8;
} stored_msg_t;

typedef struct stored_piggy{

	uint16_t period_id;
	uint8_t remaining_space;
	uint8_t node_id[5];
	int8_t pg_r7[5];
	int8_t pg_r8[5];

} stored_piggy_t;


enum {
  AM_RADIO_COUNT_MSG = 6,
  AM_SERIAL_MSG = 5,
};

#endif
