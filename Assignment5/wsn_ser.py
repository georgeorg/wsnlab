 
#!/usr/bin/env python

import sys
import tos
import threading
import Queue
import time

#application defined messages

class AppMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                          [('msg_type', 'int', 1),('app_id', 'int', 1),('sender', 'int', 1), ('codebytes', 'list' , 28)],
                            packet)
							
class NoneMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type', 'int', 1),('result_owner_id', 'int', 1),('app_id', 'int', 1),('period_id', 'int', 2),
							('r7', 'int', 1),('r8', 'int', 1)],
                            packet)

class NormalMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type', 'int', 1),('result_owner_id', 'int', 1),('app_id', 'int', 1),('period_id', 'int', 2),
							('r7', 'int', 1),('r8', 'int', 1)],
                            packet)


class PiggyMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type', 'int', 1),('result_owner_id', 'int', 1),('remaining_space', 'int', 1),
							('app_id', 'int', 1),('period_id', 'int', 2),('hop_count', 'int', 1), ('node_id', 'blob' , 5),
							('r7', 'blob', 5),('r8', 'blob', 5)],
                            packet)
							
class StatsMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type', 'int', 1),('result_owner_id', 'int', 1),('app_id', 'int', 1),('period_id', 'int', 2),('hop_count', 'int', 1),
							('r7', 'int', 1),('r8', 'int', 1)],
                            packet)

class SerialManager(threading.Thread):

	def __init__(self,rx,tx,SerialDev,SerialBaudRate,AMChannel):
		threading.Thread.__init__(self)
		threading.Thread.deamon = False 
		self.rx = rx
		self.tx = tx
		self.spare=0;
		self.SerialDev = SerialDev
		self.SerialBaudRate = SerialBaudRate
		self.AMChannel = AMChannel
		try:
			self.serial = tos.Serial(self.SerialDev,self.SerialBaudRate,timeout=0.4)
			self.am = tos.AM(self.serial)
		except :
			print "Error : ",sys.exc_info()[1]
			sys.exit()
		
	def run(self):
		print "AMessage communication started ! "
		while True:
			while self.tx.empty() == False:
				tx_pckt = self.tx.get()
				self.am.write(tx_pckt,self.AMChannel)
				#self.rx.put(tx_pckt)
			try:
				pckt = self.am.read()
				if pckt:
					msg = AppMsg(pckt.data)
					#print pckt
					if msg.msg_type == 1:
						msg = AppMsg(pckt.data)
						print "Received App Msg"
					elif msg.msg_type == 2:
						msg = NoneMsg(pckt.data)
						print "Received None Msg"
					elif msg.msg_type == 3:
						msg = NormalMsg(pckt.data)
						print "Received Normal Msg"
					elif msg.msg_type == 4:
						msg = PiggyMsg(pckt.data)
						print "Received Piggy Msg"
					elif msg.msg_type == 5:
						msg = StatsMsg(pckt.data)
						print "Received Stats Msg"
						
					self.rx.put(msg)
			except: 				
				self.spare=0	


def receiver(rx):
	while True:
		msg = rx.get()
		if msg.msg_type == 2: #none msg
			print "Receive none result from %d for app %d with data r7:%d and r8:%d from period %d" \
			%(msg.result_owner_id,msg.app_id,msg.r7,msg.r8,msg.period_id)
			
		elif msg.msg_type == 3: #normal msg
			print "Receive normal result from %d for app %d with data r7:%d and r8:%d from period %d" \
			%(msg.result_owner_id,msg.app_id,msg.r7,msg.r8,msg.period_id)
			
		elif msg.msg_type == 4: #piggy msg
			print "Receive piggy result from %d for app %d from period %d" \
			%(msg.result_owner_id,msg.app_id,msg.period_id)
			print "It contains data:"
			#for i in msg.node_id:
			print "NodeID:",msg.node_id
			print "reg r7:",msg.r7
			print "reg r8:",msg.r8
			
		elif msg.msg_type == 5: #stats msg
			print "Receive stats result from %d for app %d with data r7:%d and r8:%d from period %d" \
			%(msg.result_owner_id,msg.app_id,msg.r7,msg.r8,msg.period_id)

def transmitter(tx):
	var = 0
	while True:
		time.sleep(1)
		msg_type = str(raw_input("App Msg (1) || Kill Msg (6)"))
		sender = 1
		app_id = str(raw_input("App id: "))
		binary = str(raw_input("Name of binary: "))	
		binary = map(ord, open(binary, "rb").read())
		for x in range (len(binary), 28):
			binary.append(0)

		msg=AppMsg((int(msg_type),int(app_id),int(sender),list(binary),[]));
		tx.put(msg)
		print binary,"sent"

def main():
	tx = Queue.Queue()
	rx = Queue.Queue()
	Manager = SerialManager(rx,tx,'/dev/ttyUSB0','115200',5)	
	Manager.deamon = False
	Manager.start()

	
	rcv_th=threading.Thread(target=receiver,args=(rx,))
	rcv_th.deamon = False;
	rcv_th.start()
	
	snd_th=threading.Thread(target=transmitter,args=(tx,))
	snd_th.deamon = False;
	snd_th.start()
	


if __name__ == "__main__":
	main()
