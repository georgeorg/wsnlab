#!/usr/bin/env python

import sys
import tos
import threading
import Queue
import time
import binascii

#application defined messages

class AppBytecodeMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type',  'int', 1),
                             ('app_id',  'int', 1),
                             ('packet_id',  'int', 1),
                             ('codebytes', 'blob', None)],
                            packet)

class SerialManager(threading.Thread):

	def __init__(self,rx,tx,SerialDev,SerialBaudRate,AMChannel):
		threading.Thread.__init__(self)
		threading.Thread.deamon = False 
		self.rx = rx
		self.tx = tx
		self.spare=0;
		self.SerialDev = SerialDev
		self.SerialBaudRate = SerialBaudRate
		self.AMChannel = AMChannel
		try:
			self.serial = tos.Serial(self.SerialDev,self.SerialBaudRate,timeout=0.4)
			self.am = tos.AM(self.serial)
		except :
			print "Error : ",sys.exc_info()[1]
			sys.exit()
		
	def run(self):
		print "AMessage communication started ! "
		while True:
			while self.tx.empty() == False:
				tx_pckt = self.tx.get()
				self.am.write(tx_pckt,self.AMChannel)
				#self.rx.put(tx_pckt)
			try:
				pckt = self.am.read()
				if pckt:
					#This should never happen
					print "Received a packet"
					
					self.rx.put(msg)
			except: 				
				self.spare=0	


def receiver(rx):
	while True:
		msg = rx.get()
		#This should never happen
		print "Received a msg"


def transmitter(tx):
	while True:
		list_of_bytes = bytearray()
		print "Please enter the app parameters:\n"
		msg_type = 2
		app_id = int(raw_input("App id:"))
		packet_id = 1

		filename = sys.argv[1]
		f = fopen(filename,"rb")
		byte = f.read(1)
		while byte != "":
			list_of_bytes.append(byte)
			byte = f.read(1)

		codebytes = list_of_bytes	


		#Here we construct the transmitted message
		msg=AppBytecodeMsg((msg_type, app_id, packet_id, codebytes, []));
        tx.put(msg)
		print "AppBytecodeMsg sent"


def main():
	tx = Queue.Queue()
	rx = Queue.Queue()
	Manager = SerialManager(rx,tx,'/dev/ttyUSB0','115200',5)	
	Manager.deamon = False
	Manager.start()

	
	rcv_th=threading.Thread(target=receiver,args=(rx,))
	rcv_th.deamon = False;
	rcv_th.start()
	
	snd_th=threading.Thread(target=transmitter,args=(tx,))
	snd_th.deamon = False;
	snd_th.start()
	


if __name__ == "__main__":
	main()
