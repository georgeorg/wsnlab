#!/usr/bin/env python

import sys
import tos
import threading
import Queue
import time

#application defined messages

class SerialMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('agg_mode',  'int', 1),
                             ('sensor_type',  'int', 2),
                             ('lifetime',  'int', 2),
                             ('period',  'int', 2),
                             ('round_id',  'int', 1)],
                            packet)

class NoneMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type',  'int', 1),
                             ('agg_mode',  'int', 1),
                             ('query_owner_id',  'int', 2),
                             ('result_owner_id',  'int', 2),
                             ('packet_id',  'int', 2),  
                             ('period',  'int', 2),
                             ('data',  'int', 2)],
                            packet)

class PiggyMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type',  'int', 1),
                             ('remaining_space',  'int', 1),
                             ('query_owner_id',  'int', 2),
                             ('query_id',  'int', 2),
                             ('period_id',  'int', 2),  
                             ('node_id',  'blob', 10),
                             ('data',  'blob', 10)],
                            packet)

class StatsMsg(tos.Packet):
    def __init__(self, packet = None):
        tos.Packet.__init__(self,
                            [('msg_type',  'int', 1),
                             ('query_owner_id',  'int', 2),
                             ('query_id',  'int', 2),
                             ('period_id',  'int', 2),
                             ('stats_owner',  'int', 2),
                             ('node_count',  'int', 2),
                             ('min',  'int', 2),
                             ('max',  'int', 2),
                             ('avg',  'int', 2)],
                            packet)

class SerialManager(threading.Thread):

	def __init__(self,rx,tx,SerialDev,SerialBaudRate,AMChannel):
		threading.Thread.__init__(self)
		threading.Thread.deamon = False 
		self.rx = rx
		self.tx = tx
		self.spare=0;
		self.SerialDev = SerialDev
		self.SerialBaudRate = SerialBaudRate
		self.AMChannel = AMChannel
		try:
			self.serial = tos.Serial(self.SerialDev,self.SerialBaudRate,timeout=0.4)
			self.am = tos.AM(self.serial)
		except :
			print "Error : ",sys.exc_info()[1]
			sys.exit()
		
	def run(self):
		print "AMessage communication started ! "
		while True:
			while self.tx.empty() == False:
				tx_pckt = self.tx.get()
				self.am.write(tx_pckt,self.AMChannel)
				#self.rx.put(tx_pckt)
			try:
				pckt = self.am.read()
				if pckt:
					#print pckt
					msg = NoneMsg(pckt.data)
					if msg.msg_type == 2: #NONE_MSG
						msg = NoneMsg(pckt.data)

					elif msg.msg_type == 4: #PIGGY_MSG
						msg = PiggyMsg(pckt.data)

					elif msg.msg_type == 5: #STATS_MSG
						msg = StatsMsg(pckt.data)
					
					self.rx.put(msg)
			except: 				
				self.spare=0	


def receiver(rx):
	while True:
		msg = rx.get()
		if msg.msg_type == 2:
			print "Received a NONE message"
			print "Result is %d from node %d for period id %d for query owner %d and id %d" \
				%(msg.data, msg.result_owner_id, msg.period_id, msg.query_owner_id, msg.packet_id)
		elif msg.msg_type == 4:
			print "Received a PIGGY message"
			print "From period id %d for query owner %d and id %d" %(msg.period_id, msg.query_owner_id, msg.query_id)
			#iterate through readings
			#this is most probably super wrong
			for i in msg.node_id:
				print "Node ID %d and Data %d" %(msg.node_id[i], msg.data[i]) #this is probably wrong
		elif msg.msg_type == 5:
			print "Received a STATS message"
			print "From period id %d for query owner %d and id %d" %(msg.period_id, msg.query_owner_id, msg.query_id)
			print "Min %d, Max %d, Average %d from %d Nodes" %(msg.min, msg.max, msg.avg, msg.node_count)


def transmitter(tx):
	while True:
		print "Please enter the query parameters"

		#This needs to be one byte, dont know how
		print "Aggregation:",
		agg_mode = raw_input(" ")
		if (agg_mode == "none"):
			agg_mode = 1
		elif (agg_mode == "piggy"):
			agg_mode = 2
		elif (agg_mode == "stats"):
			agg_mode = 3

		print "Sensor:",
		sensor_type = int(raw_input(" "))

		print "Query Lifetime:",
		lifetime = int(raw_input(" "))

		print "Query Period:",
		period = int(raw_input(" "))

		print "Round ID(Set to 1 if new query):",
		round_id = int(raw_input(" "))


		#Here we construct the transmitted message
		msg=SerialMsg((agg_mode, sensor_type, lifetime, period, round_id, []));
        tx.put(msg)
		print "sent"


def main():
	tx = Queue.Queue()
	rx = Queue.Queue()
	Manager = SerialManager(rx,tx,'/dev/ttyUSB1','115200',3)	
	Manager.deamon = False
	Manager.start()

	
	rcv_th=threading.Thread(target=receiver,args=(rx,))
	rcv_th.deamon = False;
	rcv_th.start()
	
	snd_th=threading.Thread(target=transmitter,args=(tx,))
	snd_th.deamon = False;
	snd_th.start()
	


if __name__ == "__main__":
	main()
