#ifndef VM_H
#define VM_H


typedef nx_struct app_bytecode_msg {

	nx_uint8_t msg_type;
	nx_uint8_t app_id;
	nx_uint8_t packet_id;
	nx_uint8_t bytes[25];

} app_bytecode_msg_t;

typedef struct application {
	uint8_t app_id;
	int8_t reg[6];
	uint8_t app_bytes[255];
	uint8_t pc;
	uint8_t saved_pc;
	bool isActive;
	uint32_t pending_time;
	bool pending_data;

} application_t;

enum {
  AM_RADIO_COUNT_MSG = 6,
  AM_SERIAL_MSG = 5,
};

#endif
