#include "VM.h"



configuration VMAppC {}
implementation {

  components MainC, VMC as App, LedsC;

	components new DemoSensorC() as DemoSensor;
	//components new HamamatsuS1087ParC() as Sensor;
	
  
  components new AMSenderC(AM_RADIO_COUNT_MSG) as Sender;
  components new AMReceiverC(AM_RADIO_COUNT_MSG) as Receiver;


  components ActiveMessageC;


  components new TimerMilliC() as SourceTimer;
  components new TimerMilliC() as InstructionTimer;
  components new TimerMilliC() as ContextTimer;
  components HilTimerMilliC as LocalTimer;
   
  App.Boot -> MainC.Boot;
  App.Receive -> Receiver;
  App.Send -> Sender;


  App.AMControl -> ActiveMessageC;
  App.Packet -> Sender;
  //May need to connect this to multiple senders

  App.DemoRead -> DemoSensor;

  App.LocalTimer -> LocalTimer;
  App.SourceTimer -> SourceTimer;
  App.InstructionTimer -> InstructionTimer;
  App.ContextTimer -> ContextTimer;
  

  App.Leds -> LedsC;	
}


