#include "Timer.h"
#include "VM.h"



module VMC @safe() {
  uses {
   interface Leds;
   interface Boot;
   interface Receive as Receive;
   interface AMSend as Send;
   interface Timer<TMilli> as SourceTimer;
   interface Timer<TMilli> as InstructionTimer;
   interface Timer<TMilli> as ContextTimer;
   interface LocalTime<TMilli> as LocalTimer;
   interface SplitControl as AMControl;
   interface Packet as Packet;
   interface Read<uint16_t> as DemoRead;
  }
}
implementation{

  #define APP_MSG 1
  #define BYTES_MSG 2
  #define SIZE 5
  #define CTX_RATE 50
  #define INSTRUCTION_RATE 500

  //Declarations
  message_t packet;
  int8_t offset = 0;
  uint8_t msbits,lsbits;

  uint8_t i,temp_k,j,k;
  uint8_t loop_count = 0;

  bool locked = FALSE;
  bool lockedRead = FALSE;
  bool second_packet = FALSE;
  
  //uint16_t data = 0;
  uint16_t cached_data = 0;
  uint32_t cached_data_timestamp = 0;
  uint32_t t1;
  int8_t ctx_id = -1;

  application_t app[SIZE];



  event void Boot.booted() {
    call AMControl.start();
    for(i = 0;i < SIZE;i++){
      app[i].app_id = 0;
      for(j = 0;j<6;j++){
        app[i].reg[j] = 0; 
      }
      for(j =0;j<255;j++){
        app[i].app_bytes[j] = 0;
      }
      app[i].pc = 0;
      app[i].saved_pc = 0;
      app[i].isActive = FALSE;
      app[i].pending_time = 0;
      app[i].pending_data = FALSE;
    }
    //call ContextTimer.startPeriodic(CTX_RATE);
    call InstructionTimer.startPeriodic(INSTRUCTION_RATE);
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
      if(TOS_NODE_ID == 1){
        call SourceTimer.startOneShot(100);   
      }
    }
    else{
      call AMControl.start();
    }
  }

  event void AMControl.stopDone(error_t err) {
    // do nothing
  }

  task void runApp(){

    loop_count = 0;

    //Round Robin Apps

    do{
      ctx_id++;
      if(ctx_id == SIZE){ctx_id = 0;}
      loop_count = loop_count + 1;
    }while((!app[ctx_id].isActive) && (loop_count<(SIZE+1)));

    //If no active app was found, post to recheck and return
    //Not sure if good practice
    if(loop_count>=(SIZE+1)){
      dbg("RadioCountToLedsC","Didnt find active APP, returning on ctx id %d\n", ctx_id);
      return;
    }

    dbg("RadioCountToLedsC","Running app with context id:%d\n", ctx_id);

		msbits = app[ctx_id].app_bytes[app[ctx_id].pc] & 0xF0;
		msbits = msbits >> 4;
		lsbits = app[ctx_id].app_bytes[app[ctx_id].pc] & 0x0F;
		
		switch(msbits) {
      		//RET Instruction
			case 0 :
				if(app[ctx_id].pc >= 4 + app[ctx_id].app_bytes[1]){
          		//Restore pc to previous position
				    app[ctx_id].pc = app[ctx_id].saved_pc;
					dbg("RadioCountToLedsC","Instruction RET from Timer. PC :%d\n",app[ctx_id].pc);

					post runApp();
				}
				else{
					//If no timer is pending, unload app
					if( app[ctx_id].pending_time == 0){
						dbg("RadioCountToLedsC","RET from init.Unload App.\n");
			          //Unload App
			          app[ctx_id].app_id = 0;
			          for(j = 0;j<6;j++){
			            app[ctx_id].reg[j] = 0; 
			          }
			          for(j =0;j<255;j++){
			            app[ctx_id].app_bytes[j] = 0;
			          }
			          app[ctx_id].pc = 0;
			          app[ctx_id].saved_pc = 0;
			          app[ctx_id].isActive = FALSE;
			          app[ctx_id].pending_time = 0;
			          app[ctx_id].pending_data = FALSE;

			          post runApp();
					}
					//If a timer is pending, deactivate app
					//And wait for timer to fire
					else{
					  dbg("RadioCountToLedsC","RET from init.Timer Pending, deactivating\n");
					  app[ctx_id].isActive = FALSE;

					  post runApp();
					}	
				}
				break;
      		//SET Instruction
	      	case 1:
		        app[ctx_id].pc++;
		        app[ctx_id].reg[lsbits-1] = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
		        dbg("RadioCountToLedsC","SET Instruction. Set reg %d to %d \n", lsbits, app[ctx_id].reg[lsbits-1]);
		        //Go to next instruction
		        app[ctx_id].pc++;
		        dbg("RadioCountToLedsC","SET PC:%d.\n",app[ctx_id].pc);
				post runApp();
		        break;
	      	//CPY Instruction
	      	case 2:
		        app[ctx_id].pc++;
		        app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]];
		        dbg("RadioCountToLedsC","CPY Instruction. Cpy reg %d to %d \n", lsbits, app[ctx_id].app_bytes[app[ctx_id].pc]);
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//ADD Instruction
	      	case 3:
		        app[ctx_id].pc++;
		        app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] + app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]];
		        dbg("RadioCountToLedsC","ADD Instruction. Add reg %d to %d \n", app[ctx_id].app_bytes[app[ctx_id].pc], lsbits);
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//SUB Instruction
	      	case 4:
		        app[ctx_id].pc++;
		        dbg("RadioCountToLedsC","SUB Instruction.  %d - %d.\n",
		        	app[ctx_id].reg[lsbits-1],app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1]);

		        app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] - app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]-1];

		        dbg("RadioCountToLedsC","SUB Instruction. Sub reg %d from %d.Result:%d\n", 
		        	app[ctx_id].app_bytes[app[ctx_id].pc], lsbits,app[ctx_id].reg[lsbits-1]);
		        
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//INC Instruction
	      	case 5:
		        app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] + 1;
		        dbg("RadioCountToLedsC","INC Instruction. Incremented reg %d \n", lsbits);
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//DEC Instruction
        	case 6:
	          	app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[lsbits-1] - 1;
	          	dbg("RadioCountToLedsC","DEC Instruction. Decremented reg %d \n", lsbits);
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//MAX Instruction
	      	case 7:
		        app[ctx_id].pc++;
		        if (app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]] > app[ctx_id].reg[lsbits-1]){
		          app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]];
		        }
		        dbg("RadioCountToLedsC","MAX Instruction \n");
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//MIN Instruction
	      	case 8:
		        app[ctx_id].pc++;
		        if (app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]] < app[ctx_id].reg[lsbits-1]){
		          app[ctx_id].reg[lsbits-1] = app[ctx_id].reg[app[ctx_id].app_bytes[app[ctx_id].pc]];
		        }
		        dbg("RadioCountToLedsC","MIN Instruction \n");
		        //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	        	break;
	      	//BGZ Instruction
	      	case 9:
		        app[ctx_id].pc++;
		        if (app[ctx_id].reg[lsbits-1] > 0){
		          offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
		          app[ctx_id].pc = app[ctx_id].pc + offset;
		        }
		        else{
		        	//Go to next instruction
		        	app[ctx_id].pc++;
		        }
			    post runApp();
		        dbg("RadioCountToLedsC","BGZ Instruction \n");
		        break;
	      	//BEZ Instruction
	      	case 10:
		        app[ctx_id].pc++;
		        if (app[ctx_id].reg[lsbits-1] == 0){
		          offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
		          app[ctx_id].pc = app[ctx_id].pc + offset;
		        }
		        else{
		        	//Go to next instruction
		        	app[ctx_id].pc++;
		        }
		        post runApp();
		        dbg("RadioCountToLedsC","BEZ Instruction jumping to:%d with offset:%d \n",app[ctx_id].pc,offset);
		        break;
	      	//BRA Instruction
	      	case 11:
		        app[ctx_id].pc++;
		        offset = (int8_t) app[ctx_id].app_bytes[app[ctx_id].pc];
		        app[ctx_id].pc = app[ctx_id].pc + offset;
		        dbg("RadioCountToLedsC","BRA instruction jump to :%d\n",app[ctx_id].pc);
		        post runApp();
		        break;
        	//LED Instruction
			case 12:
			  	dbg("RadioCountToLedsC","Instruction Led\n");
			  	if(lsbits == 0){
			  		dbg("RadioCountToLedsC","Turn led off from ctx_id:%d\n", ctx_id);
			  	}
			  	else{
			  		dbg("RadioCountToLedsC","Turn led on from ctx_id:%d\n", ctx_id);
			  	}
			  	//Go to next instruction
	        	app[ctx_id].pc++;
				post runApp();
			  	break;
        	//RDB Instruction
        	case 13:
	          //If cached is older than 1 second, resample
	          t1 = call LocalTimer.get();
	          if((cached_data != 0) && (t1 - cached_data_timestamp < 1000)){
	            app[ctx_id].reg[lsbits-1] = cached_data;
	            dbg("RadioCountToLedsC","Data %d \n",app[ctx_id].reg[lsbits-1]);
	            //Go to next instruction
		        app[ctx_id].pc++;
			    post runApp();
	          }
	          else{
	            if (!lockedRead){
	          	 dbg("RadioCountToLedsC","Calling Demoread\n");
	             call DemoRead.read();
	             lockedRead = TRUE;
	            }
	            app[ctx_id].isActive = FALSE;
	            app[ctx_id].pending_data = TRUE;
	            post runApp();
	          }
	          dbg("RadioCountToLedsC","Rdb Instruction \n");
	          break;
        	//TMR Instruction
			case 14:
				dbg("RadioCountToLedsC","Instruction Timer\n");
			   	app[ctx_id].pc++;
		        app[ctx_id].pending_time = app[ctx_id].app_bytes[app[ctx_id].pc] * 1000;
		        
		        //app[ctx_id].isActive = FALSE;

				//call InstructionTimer.startOneShot(app[ctx_id].app_bytes[app[ctx_id].pc]);
				
				//Go to next command
				app[ctx_id].pc++;

          		post runApp();
				break;
        	//DEFAULT Instruction
			default :
				dbg("RadioCountToLedsC","WRONG\n");

	          //Unload App
	          	app[ctx_id].app_id = 0;
	          	for(j = 0;j<6;j++){
	            	app[ctx_id].reg[j] = 0; 
	          	}
	          	for(j =0;j<255;j++){
	            	app[ctx_id].app_bytes[j] = 0;
	          	}
	          	app[ctx_id].pc = 0;
	          	app[ctx_id].saved_pc = 0;
	          	app[ctx_id].isActive = FALSE;
	          	app[ctx_id].pending_time = 0;
	          	app[ctx_id].pending_data = FALSE;

				break;
		}	
	}

  event message_t* Receive.receive(message_t* bufPtr, void* payload, uint8_t len) {

    if (len != sizeof(app_bytecode_msg_t)) {
      return bufPtr;
    }
    else {
      
      	app_bytecode_msg_t* apm = (app_bytecode_msg_t*)payload;

        for(i = 0; i < SIZE; i++){
          if(app[i].app_id == 0){
            dbg("RadioCountToLedsC","Received bytecode packet\n");
            app[i].app_id = apm->app_id;
            temp_k = ((apm->packet_id) * 25);
            j = 0;
            for(k = temp_k;k < temp_k+25;k++){
              app[i].app_bytes[k] = apm->bytes[j];
              j++;
            }
            app[i].pc = 3;
            app[i].isActive = TRUE;
            post runApp();          
            break;   
          }
        }
        return bufPtr;
      }
  }

  event void Send.sendDone(message_t* bufPtr, error_t error) {
  	if (&packet == bufPtr){
      locked = FALSE;
  	}

  }

  event void DemoRead.readDone(error_t result, uint16_t data){

    if(result == SUCCESS){
      if (data > 127 || data < -127){
      	data = 127;
      }
      cached_data = data;
      cached_data_timestamp = call LocalTimer.get();
    }
    lockedRead = FALSE;
    dbg("RadioCountToLedsC", "New data :%d.\n",cached_data);

    for(i=0; i<SIZE; i++){
      if(app[i].pending_data){
        app[i].pending_data = FALSE;
        app[i].isActive = TRUE;

        if(TOS_NODE_ID != 1){
          post runApp();
        }
      }
    }
  }


  event void ContextTimer.fired(){
    loop_count = 0;

    //Round Robin Apps
    do{
      ctx_id++;
      if(ctx_id == SIZE){ctx_id = 0;}
      loop_count = loop_count + 1;
    }while((!app[ctx_id].isActive) && (loop_count<SIZE));

  }

  event void InstructionTimer.fired(){

    for (i=0; i<SIZE; i++){
      if(app[i].pending_time != 0){
        app[i].pending_time = app[i].pending_time - INSTRUCTION_RATE;
        //If the app's timer has expired
        if(app[i].pending_time == 0){
          app[i].isActive = TRUE;

          app[i].saved_pc = app[i].pc;
          app[i].pc = 4 + app[i].app_bytes[1]-1;

          //Recheck this
          if(TOS_NODE_ID != 1){
            post runApp();
          }
          dbg("RadioCountToLedsC","Inside instruction timer jumping from : %d to : %d\n",app[i].saved_pc,app[i].pc);
        }
      }
    }

    
  }

  event void SourceTimer.fired(){

    app_bytecode_msg_t* apm2 = (app_bytecode_msg_t*)call Packet.getPayload(&packet, sizeof(app_bytecode_msg_t));
    if (apm2 == NULL) {
      return;
    }
    if(!second_packet){

      apm2->msg_type = BYTES_MSG;
      apm2->packet_id = 0;
      apm2->app_id = 1;
       
      //App3
      apm2->bytes[0] = 0x17;
      apm2->bytes[1] = 0x06;
      apm2->bytes[2] = 0x0E;
      apm2->bytes[3] = 0xC1;
      apm2->bytes[4] = 0x11;
      apm2->bytes[5] = 0x01;
      apm2->bytes[6] = 0xE0;
      apm2->bytes[7] = 0x03;
      apm2->bytes[8] = 0x00;
      apm2->bytes[9] = 0xA1;
      apm2->bytes[10] = 0x07;
      apm2->bytes[11] = 0xC0;
      apm2->bytes[12] = 0x11;
      apm2->bytes[13] = 0x00;
      apm2->bytes[14] = 0xE0;
      apm2->bytes[15] = 0x07;
      apm2->bytes[16] = 0x00;
      apm2->bytes[17] = 0xC1;
      apm2->bytes[18] = 0x11;
      apm2->bytes[19] = 0x01;
      apm2->bytes[20] = 0xE0;
      apm2->bytes[21] = 0x03;
      apm2->bytes[22] = 0x00;

      for(i=23;i<25;i++){
        apm2->bytes[i] = 0;
      }

      if(!locked){
        if (call Send.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
          dbg("RadioCountToLedsC", "Control packet sent.\n");
          locked = TRUE;
        }
      }
      call SourceTimer.startOneShot(200);
      second_packet = TRUE;
      
      }
      else{

      apm2->msg_type = BYTES_MSG;
      apm2->packet_id = 0;
      apm2->app_id = 2;
      
      //App2
      apm2->bytes[0] = 0x09;
      apm2->bytes[1] = 0x04;
      apm2->bytes[2] = 0x02;
      apm2->bytes[3] = 0xC1;
      apm2->bytes[4] = 0xE0;
      apm2->bytes[5] = 0x03;
      apm2->bytes[6] = 0x00;
      apm2->bytes[7] = 0xC0;
      apm2->bytes[8] = 0x00;
      
    /*
      //App4
      apm2->bytes[0] = 0x14;
      apm2->bytes[1] = 0x03;
      apm2->bytes[2] = 0x0E;
      apm2->bytes[3] = 0xE0;
      apm2->bytes[4] = 0x05;
      apm2->bytes[5] = 0x00;
      apm2->bytes[6] = 0xD1;
      apm2->bytes[7] = 0x12;
      apm2->bytes[8] = 0x32;
      apm2->bytes[9] = 0x42;
      apm2->bytes[10] = 0x01;
      apm2->bytes[11] = 0x92;
      apm2->bytes[12] = 0x04;
      apm2->bytes[13] = 0xC0;
      apm2->bytes[14] = 0xB0;
      apm2->bytes[15] = 0x02;
      apm2->bytes[16] = 0xC1;
      apm2->bytes[17] = 0xE0;
      apm2->bytes[18] = 0x05;
      apm2->bytes[19] = 0x00;
  	*/
      for(i=9;i<25;i++){
        apm2->bytes[i] = 0;
      }

        if(!locked){
          if (call Send.send(AM_BROADCAST_ADDR, &packet, sizeof(app_bytecode_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "Control packet sent.\n");
            locked = TRUE;
          }
        }

      }
  }

}





