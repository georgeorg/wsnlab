/**
 * This class is automatically generated by mig. DO NOT EDIT THIS FILE.
 * This class implements a Java interface to the 'RadioCountMsg'
 * message type.
 */

public class RadioCountMsg extends net.tinyos.message.Message {

    /** The default size of this message type in bytes. */
    public static final int DEFAULT_MESSAGE_SIZE = 10;

    /** The Active Message type associated with this message. */
    public static final int AM_TYPE = 6;

    /** Create a new RadioCountMsg of size 10. */
    public RadioCountMsg() {
        super(DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /** Create a new RadioCountMsg of the given data_length. */
    public RadioCountMsg(int data_length) {
        super(data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg with the given data_length
     * and base offset.
     */
    public RadioCountMsg(int data_length, int base_offset) {
        super(data_length, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg using the given byte array
     * as backing store.
     */
    public RadioCountMsg(byte[] data) {
        super(data);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg using the given byte array
     * as backing store, with the given base offset.
     */
    public RadioCountMsg(byte[] data, int base_offset) {
        super(data, base_offset);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg using the given byte array
     * as backing store, with the given base offset and data length.
     */
    public RadioCountMsg(byte[] data, int base_offset, int data_length) {
        super(data, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg embedded in the given message
     * at the given base offset.
     */
    public RadioCountMsg(net.tinyos.message.Message msg, int base_offset) {
        super(msg, base_offset, DEFAULT_MESSAGE_SIZE);
        amTypeSet(AM_TYPE);
    }

    /**
     * Create a new RadioCountMsg embedded in the given message
     * at the given base offset and length.
     */
    public RadioCountMsg(net.tinyos.message.Message msg, int base_offset, int data_length) {
        super(msg, base_offset, data_length);
        amTypeSet(AM_TYPE);
    }

    /**
    /* Return a String representation of this message. Includes the
     * message type name and the non-indexed field values.
     */
    public String toString() {
      String s = "Message <RadioCountMsg> \n";
      try {
        s += "  [sender_id=0x"+Long.toHexString(get_sender_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [prev_sender=0x"+Long.toHexString(get_prev_sender())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [owner_id=0x"+Long.toHexString(get_owner_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [packet_id=0x"+Long.toHexString(get_packet_id())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      try {
        s += "  [data=0x"+Long.toHexString(get_data())+"]\n";
      } catch (ArrayIndexOutOfBoundsException aioobe) { /* Skip field */ }
      return s;
    }

    // Message-type-specific access methods appear below.

    /////////////////////////////////////////////////////////
    // Accessor methods for field: sender_id
    //   Field type: int, unsigned
    //   Offset (bits): 0
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'sender_id' is signed (false).
     */
    public static boolean isSigned_sender_id() {
        return false;
    }

    /**
     * Return whether the field 'sender_id' is an array (false).
     */
    public static boolean isArray_sender_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'sender_id'
     */
    public static int offset_sender_id() {
        return (0 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'sender_id'
     */
    public static int offsetBits_sender_id() {
        return 0;
    }

    /**
     * Return the value (as a int) of the field 'sender_id'
     */
    public int get_sender_id() {
        return (int)getUIntBEElement(offsetBits_sender_id(), 16);
    }

    /**
     * Set the value of the field 'sender_id'
     */
    public void set_sender_id(int value) {
        setUIntBEElement(offsetBits_sender_id(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'sender_id'
     */
    public static int size_sender_id() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'sender_id'
     */
    public static int sizeBits_sender_id() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: prev_sender
    //   Field type: int, unsigned
    //   Offset (bits): 16
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'prev_sender' is signed (false).
     */
    public static boolean isSigned_prev_sender() {
        return false;
    }

    /**
     * Return whether the field 'prev_sender' is an array (false).
     */
    public static boolean isArray_prev_sender() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'prev_sender'
     */
    public static int offset_prev_sender() {
        return (16 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'prev_sender'
     */
    public static int offsetBits_prev_sender() {
        return 16;
    }

    /**
     * Return the value (as a int) of the field 'prev_sender'
     */
    public int get_prev_sender() {
        return (int)getUIntBEElement(offsetBits_prev_sender(), 16);
    }

    /**
     * Set the value of the field 'prev_sender'
     */
    public void set_prev_sender(int value) {
        setUIntBEElement(offsetBits_prev_sender(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'prev_sender'
     */
    public static int size_prev_sender() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'prev_sender'
     */
    public static int sizeBits_prev_sender() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: owner_id
    //   Field type: int, unsigned
    //   Offset (bits): 32
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'owner_id' is signed (false).
     */
    public static boolean isSigned_owner_id() {
        return false;
    }

    /**
     * Return whether the field 'owner_id' is an array (false).
     */
    public static boolean isArray_owner_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'owner_id'
     */
    public static int offset_owner_id() {
        return (32 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'owner_id'
     */
    public static int offsetBits_owner_id() {
        return 32;
    }

    /**
     * Return the value (as a int) of the field 'owner_id'
     */
    public int get_owner_id() {
        return (int)getUIntBEElement(offsetBits_owner_id(), 16);
    }

    /**
     * Set the value of the field 'owner_id'
     */
    public void set_owner_id(int value) {
        setUIntBEElement(offsetBits_owner_id(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'owner_id'
     */
    public static int size_owner_id() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'owner_id'
     */
    public static int sizeBits_owner_id() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: packet_id
    //   Field type: int, unsigned
    //   Offset (bits): 48
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'packet_id' is signed (false).
     */
    public static boolean isSigned_packet_id() {
        return false;
    }

    /**
     * Return whether the field 'packet_id' is an array (false).
     */
    public static boolean isArray_packet_id() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'packet_id'
     */
    public static int offset_packet_id() {
        return (48 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'packet_id'
     */
    public static int offsetBits_packet_id() {
        return 48;
    }

    /**
     * Return the value (as a int) of the field 'packet_id'
     */
    public int get_packet_id() {
        return (int)getUIntBEElement(offsetBits_packet_id(), 16);
    }

    /**
     * Set the value of the field 'packet_id'
     */
    public void set_packet_id(int value) {
        setUIntBEElement(offsetBits_packet_id(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'packet_id'
     */
    public static int size_packet_id() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'packet_id'
     */
    public static int sizeBits_packet_id() {
        return 16;
    }

    /////////////////////////////////////////////////////////
    // Accessor methods for field: data
    //   Field type: int, unsigned
    //   Offset (bits): 64
    //   Size (bits): 16
    /////////////////////////////////////////////////////////

    /**
     * Return whether the field 'data' is signed (false).
     */
    public static boolean isSigned_data() {
        return false;
    }

    /**
     * Return whether the field 'data' is an array (false).
     */
    public static boolean isArray_data() {
        return false;
    }

    /**
     * Return the offset (in bytes) of the field 'data'
     */
    public static int offset_data() {
        return (64 / 8);
    }

    /**
     * Return the offset (in bits) of the field 'data'
     */
    public static int offsetBits_data() {
        return 64;
    }

    /**
     * Return the value (as a int) of the field 'data'
     */
    public int get_data() {
        return (int)getUIntBEElement(offsetBits_data(), 16);
    }

    /**
     * Set the value of the field 'data'
     */
    public void set_data(int value) {
        setUIntBEElement(offsetBits_data(), 16, value);
    }

    /**
     * Return the size, in bytes, of the field 'data'
     */
    public static int size_data() {
        return (16 / 8);
    }

    /**
     * Return the size, in bits, of the field 'data'
     */
    public static int sizeBits_data() {
        return 16;
    }

}
