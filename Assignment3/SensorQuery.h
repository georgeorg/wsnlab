#ifndef SENSOR_QUERY_H
#define SENSOR_QUERY_H

typedef nx_struct serial_msg {
	nx_uint8_t agg_mode;
	nx_uint16_t sensor_type;
	nx_uint16_t period;
	nx_uint16_t lifetime;
	nx_uint8_t round_id;
} serial_msg_t;

typedef nx_struct query_msg {
	/*Curent Data Length 14 Bytes*/
	nx_uint8_t msg_type;
	nx_uint8_t agg_mode;
  	nx_uint16_t owner_id;
  	nx_uint16_t packet_id; //now used as query ID
  	nx_uint16_t sensor_type; 
  	nx_uint16_t period;
  	nx_uint16_t lifetime;
  	nx_uint16_t sender_id;
  	nx_uint8_t round_id;

} query_msg_t;

typedef nx_struct serial_msg {
  nx_uint16_t data;
} serial_msg_t;

typedef nx_struct query_result_msg {
	nx_uint8_t msg_type;
	nx_uint8_t agg_mode;
  	nx_uint16_t query_owner_id;
  	nx_uint16_t result_owner_id;
  	nx_uint16_t packet_id; //now used as query ID
  	nx_uint16_t period;
  	nx_uint16_t data;
} query_result_msg_t;


typedef nx_struct route_reply{
	nx_uint8_t msg_type;
	nx_uint16_t child_id;
	nx_uint16_t query_owner_id;
	nx_uint16_t query_id;
} route_reply_t;

typedef nx_struct piggy_msg{
	nx_uint8_t msg_type;
	nx_uint8_t remaining_space;
	nx_uint16_t query_owner_id;
	nx_uint16_t query_id;
	nx_uint16_t period_id;
	nx_uint16_t node_id[5];
	nx_uint16_t data[5];
} piggy_msg_t;

typedef nx_struct stats_msg{
	nx_uint8_t msg_type;
	nx_uint16_t query_owner_id;
	nx_uint16_t query_id;
	nx_uint16_t period_id;
	nx_uint16_t stats_owner;
	nx_uint16_t node_count;
	nx_uint16_t min;
	nx_uint16_t max;
	nx_uint16_t avg;
} stats_msg_t;

typedef struct stored_query {
	//29Bytes and 2 bools
	uint16_t owner;
	uint16_t id;
	uint16_t sender;
	uint16_t lifetime;
	uint16_t period;
	uint16_t period_id;
	uint16_t period_counter;
	bool isActive;
	uint8_t agg_mode;
	uint8_t child_count;
	uint16_t waiting_time;
	uint16_t waiting_counter;
	bool canSend;
	uint16_t data;
	uint16_t old_data;
	uint16_t min;
	uint16_t max;
	uint16_t avg;
	uint8_t outdated_count;
	uint8_t regulate_time;

} stored_query_t;

typedef struct stored_piggy{
	uint8_t msg_type;
	uint8_t remaining_space;
	uint16_t query_owner_id;
	uint16_t query_id;
	uint16_t period_id;
	uint16_t node_id[5];
	uint16_t data[5];
} stored_piggy_t;

typedef struct stored_stats{
	uint8_t msg_type;
	uint16_t query_owner_id;
	uint16_t query_id;
	uint16_t period_id;
	uint16_t stats_owner[10];
	uint16_t old_node_count[10];
	uint16_t node_count;
	uint16_t min;
	uint16_t max;
	uint16_t avg;
} stored_stats_t;

typedef struct stored_buffer{
	//Size: 49B

	//These are general purpose fields
	uint8_t msg_type;
	uint16_t query_owner_id;
	uint16_t query_id;
	uint16_t period_id; //This is used for period in query msgs
	uint16_t destination;
	//These are for query msgs
	uint8_t agg_mode;
	uint16_t sensor_type;
	uint16_t lifetime;
  	uint16_t sender_id;
  	uint16_t round_id;
  	//These are for NONE msgs
  	uint16_t none_data;
  	uint16_t result_owner_id;
  	//These are for piggy
  	uint8_t remaining_space;
  	uint16_t node_id[5];
	uint16_t data[5];
	//These are for stats
	uint16_t node_count;
	uint16_t min;
	uint16_t max;
	uint16_t avg;
} stored_buffer_t;



enum {
  AM_RADIO_COUNT_MSG = 6,
  AM_SERIAL_MSG = 5,
  AM_QUERY_MSG = 4,
};


#endif
