#include "Timer.h"
#include "SensorQuery.h"



module SensorQueryC @safe() {
  uses {
   interface Leds;
   interface Boot;
   interface Receive as FloodReceive;
   interface Receive as ResultReceive;
   interface Receive as Receive_Serial;
   interface AMSend as FloodSend;
   interface AMSend as ResultSend;
   interface AMSend as SerialSend;
   interface Timer<TMilli> as SourceTimer ;
	 interface Timer<TMilli> as RebroadcastTimer ;
	 interface Timer<TMilli> as SensorTimer;
   interface Timer<TMilli> as BufferTimer;
   interface Timer<TMilli> as SendResultTimer;
	 interface LocalTime<TMilli> as LocalTimer;
   interface SplitControl as AMControl;
   interface SplitControl as AMControl_Serial;
   interface Packet as FloodPacket;
   interface Packet as ResultPacket;
   interface Read<uint16_t> as DemoRead;
  }
}
implementation{

  #define SIZE 10
  #define BUFF_SIZE 5
  #define PGSIZE 5
  #define FAMILY_SIZE 10

  #define QUERY_MSG 1
  #define NONE_MSG 2
  #define ROUTING_MSG 3
  #define PIGGY_MSG 4
  #define STATS_MSG 5

  #define NONE 1
  #define PIGGY 2
  #define STATS 3

  ///////////////////////////////////////////
  //Approximately 1250 Bytes of storage used
  ///////////////////////////////////////////
    
  message_t packet;

  uint8_t ledflag;
  uint32_t rtt_time = 0;
  uint32_t backoff_time = 0;
  uint16_t base_waiting_time = 50;

  //Check initialization values
  uint32_t gcd_period = 0;
  uint32_t gcd_period_old = 0;

  //Same as above but for waiting time
  uint32_t gcd_waiting = 0;
  uint32_t gcd_waiting_old = 0;


  //GCD declarations
  uint32_t rem, num1, num2;

  //Flags and counters
  bool locked = FALSE;
  bool lockedRead = FALSE;
  bool inserted_query = FALSE;
  bool buffer_flag = FALSE;
  uint16_t counter = 0;
  uint8_t i, j;
  uint8_t qbuf = 0;
  uint8_t tbuf = 0;

  //Arrays and buffers
  uint16_t stored[SIZE][3];
  stored_query_t queries[SIZE];
  uint8_t query_buffer[SIZE];
  stored_piggy_t local_piggy[SIZE];
  stored_stats_t local_stats[SIZE];

  stored_buffer_t temp_buffer[BUFF_SIZE];


  uint16_t stored_sender = 0;
  uint16_t local_packet_id = 0;
  uint16_t local_round_id = 0;
  uint32_t t1,t2,xronos;
  uint16_t stored_sensor_type = 0;
  uint8_t stored_agg_mode = PIGGY;

  //TODO : Recheck size of these
  uint16_t stored_period = 0;
  uint16_t stored_lifetime = 0;




  	
  event void Boot.booted() {
    	call AMControl.start();
      call AMControl_Serial.start();

    	//Initialize routing table
    	for(i=0; i<SIZE; i++){
    		stored[i][0] = 0;
    		stored[i][1] = 0;
    		stored[i][2] = 0;
    	}

      //Initialize queries table
      for(j=0; j<SIZE; j++){
        queries[j].owner = 999;
        queries[j].id = 999;
        queries[j].sender = 0;
        queries[j].lifetime = 0;
        queries[j].period = 0;
        queries[j].period_id = 0;
        queries[j].period_counter = 0;
        queries[j].isActive = FALSE;
        queries[j].agg_mode = 0;
        queries[j].child_count = 0;
        queries[j].waiting_time = 0;
        queries[j].waiting_counter = 0;
        queries[j].data = 0;
        queries[j].old_data = 0;
        queries[j].canSend = FALSE;
        queries[j].outdated_count = 0;
        queries[j].regulate_time = 1;
      }

      //Initialize local piggy struct
      for(i=0; i<SIZE; i++){
        local_piggy[i].msg_type = PIGGY_MSG;
        local_piggy[i].remaining_space = PGSIZE;
        local_piggy[i].query_owner_id = 0;
        local_piggy[i].query_id = 0;
        //This denotes the NEXT packet that will be sent
        local_piggy[i].period_id = 1;
        for(j=0; j<PGSIZE; j++){
          local_piggy[i].node_id[j] = 0;
          local_piggy[i].data[j] = 0;
        }
      }

      //Initialize local stats struct
      for(i=0; i<SIZE; i++){
        local_stats[i].msg_type = STATS_MSG;
        local_stats[i].query_owner_id = 666;
        local_stats[i].query_id = 666;
        //This denotes the NEXT packet that will be sent
        local_stats[i].period_id = 1;
        for(j=0; j<FAMILY_SIZE; j++){
          local_stats[i].stats_owner[j] = 0;
        }
        local_stats[i].node_count = 1;
        local_stats[i].min = 0;
        local_stats[i].max = 0;
        local_stats[i].avg = 0;
      }

      //Initialize temp buffer
      for(i=0; i<SIZE; i++){
        temp_buffer[i].msg_type = 0;
        temp_buffer[i].query_owner_id = 0;
        temp_buffer[i].query_id = 0;
        temp_buffer[i].period_id = 0;
        temp_buffer[i].agg_mode = 0;
        temp_buffer[i].sensor_type = 0;
        temp_buffer[i].lifetime = 0;
        temp_buffer[i].sender_id = 0;
        temp_buffer[i].none_data = 0;
        temp_buffer[i].result_owner_id = 0;
        temp_buffer[i].remaining_space = 0;
        temp_buffer[i].round_id = 0;
        for(j=0; j<PGSIZE; j++){
          temp_buffer[i].node_id[j] = 0;
          temp_buffer[i].data[j] = 0;
        }
        temp_buffer[i]. node_count = 0;
        temp_buffer[i]. min = 0;
        temp_buffer[i]. max = 0;
        temp_buffer[i]. avg = 0;
      }
  }

  event void AMControl_Serial.startDone(error_t err) {
    if (err != SUCCESS) {
      call AMControl.start();
    } 
  }

  event void AMControl_Serial.stopDone(error_t err) {
    // do nothing
  }

  event void AMControl.startDone(error_t err) {
    if (err == SUCCESS) {
    }
   	else{
    	call AMControl.start();
   	}
  }


  event void AMControl.stopDone(error_t err) {
    // do nothing
  }

  event message_t* Receive_Serial.receive(message_t* bufPtr, void* payload, uint8_t len){
    if (len != sizeof(serial_msg_t)) {
      return bufPtr;
    }
    else{
      serial_msg_t* srl = (serial_msg_t*)payload;

      query_msg_t* qrm = (query_msg_t*)call FloodPacket.getPayload(&packet, sizeof(query_msg_t));
      if (qrm == NULL) {
        dbg("RadioCountToLedsC", "Packet not allocated.\n");
      }

      local_packet_id = local_packet_id+1;
      //local_round_id = local_round_id+1;
      

      //If round_id is 1 consider this a new query
      if(srl->round_id == 1){
        local_packet_id = local_packet_id+1;

        qrm->msg_type = QUERY_MSG;
        qrm->agg_mode = srl->agg_mode;
        qrm->packet_id = local_packet_id;
        qrm->sender_id = TOS_NODE_ID;
        qrm->owner_id = TOS_NODE_ID;
        qrm->sensor_type = srl->sensor_type;
        qrm->period = srl->period;
        qrm->lifetime = srl->lifetime;
        qrm->round_id = srl->round_id;
      }
      //Else treat as a robustness check
      else if(srl->round_id > 1){

        qrm->msg_type = QUERY_MSG;
        qrm->agg_mode = srl->agg_mode;
        qrm->packet_id = local_packet_id;
        qrm->sender_id = TOS_NODE_ID;
        qrm->owner_id = TOS_NODE_ID;
        qrm->sensor_type = srl->sensor_type;
        qrm->period = srl->period;
        qrm->lifetime = srl->lifetime;
        qrm->round_id = srl->round_id;
      }

      for(j=0; j<SIZE-1; j++){
        stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
        stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
        stored[(SIZE-1)-j][2]= stored[(SIZE-1)-j-1][2];
      }

      stored[0][0] = qrm->owner_id;
      stored[0][1] = qrm->packet_id;
      stored[0][2] = qrm->round_id;

      if(!locked){
        if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_msg_t)) == SUCCESS) {
          t1 = call LocalTimer.get();
          dbg("RadioCountToLedsC","Packet %d:%d SendTime %d.\n",qrm->owner_id,qrm->packet_id,t1);
          dbg("RadioCountToLedsC", "Packet sent.\n");
          locked = TRUE;
        }
      }

    }
  }

  event message_t* FloodReceive.receive(message_t* bufPtr, void* payload, uint8_t len) {
	
    if (len != sizeof(query_msg_t)) {
    	return bufPtr;
    }
    else {
      query_msg_t* qrm = (query_msg_t*)payload;
      //dbg("RadioCountToLedsC", "Received packet id: %d:%d, from node %d.\n",qrm->owner_id, qrm->packet_id,qrm->sender_id);
      if(qrm->msg_type != QUERY_MSG){
        return bufPtr;
      }
      
      for (i = 0; i < SIZE; i++){
        //elegxos an exoume ton owner sto topiko mas pinaka
      	if(stored[i][0] == qrm->owner_id){
      		if(qrm->packet_id > stored[i][1]){ 

	     		xronos = call LocalTimer.get();
            	//dbg("RadioCountToLedsC","Packet %d:%d ArrTime %d.\n",qrm->owner_id,qrm->packet_id,xronos);
						//Sort the routing table
	           	for(j=0; j<i; j++){
	           		stored[i-j][0]= stored[i-j-1][0];
	           		stored[i-j][1]= stored[i-j-1][1];
	           		stored[i-j][2]= stored[i-j-1][2];
	           	}

	            //Put new element at top of routing table
				stored[0][0] = qrm->owner_id;
	            stored[0][1] = qrm->packet_id;
	            stored[0][2] = qrm->round_id;
				stored_sender = qrm->sender_id;
	            stored_period = qrm->period;
	            stored_lifetime = qrm->lifetime;
	            stored_sensor_type = qrm->sensor_type;
	            stored_agg_mode = qrm->agg_mode;



	            //Check for inactive queries on the table
	            for(j=0; j<SIZE; j++){
	              if(!(queries[j].isActive)){
	                //Store and activate the query
	                queries[j].owner = qrm->owner_id;
	                queries[j].id = qrm->packet_id;
	                queries[j].sender = qrm->sender_id;
	                queries[j].lifetime = qrm->lifetime;
	                queries[j].period = qrm->period;
	                queries[j].period_id = 0;
	                queries[j].period_counter = 0;
	                queries[j].isActive = TRUE;
	                queries[j].agg_mode = qrm->agg_mode;
	                queries[j].child_count = 0;
	                queries[j].waiting_time = base_waiting_time;
	                queries[j].waiting_counter = 0;

	                //These are clearly only used for dev purposes and should be optimised

	                local_piggy[j].query_owner_id = qrm->owner_id;
	                local_piggy[j].query_id = qrm->packet_id;

	                local_stats[i].query_owner_id = qrm->owner_id;
	                local_stats[i].query_id = qrm->owner_id;

	                inserted_query = TRUE;

	                break;
	              }
	              else if(j == (SIZE-1)){
	                dbg("RadioCountToLedsC","No space for query with id %d:%d, discarded \n",qrm->owner_id,qrm->packet_id);
	                /*ReCheck how the system reacts to a full query table*/
	                inserted_query = FALSE;
	              }
	            }

	            //Compute the gcd period among the active timers
	            //Must work for a particular sensor type
	            //Only do so if the query was not discarded
	            if(inserted_query){

	              num1 = stored_period;
	              if(gcd_period == 0){
	                num2 = stored_period;
	              }
	              else{
	                num2 = gcd_period;
	              }
	              rem = num1 % num2;
	              if(rem != 0){
	                while(rem != 0){
	                  rem = num1 % num2;
	                  num1 = num2;
	                  num2 = rem;
	                }
	                gcd_period = num1;
	              }
	              else{
	                gcd_period = num2;
	              }

	              //dbg("RadioCountToLedsC", "Made it to Sensor Timer with id %d \n",qrm->packet_id);
	              if(!(call SensorTimer.isRunning())){
	                call SensorTimer.startOneShot(gcd_period);
	                gcd_period_old = gcd_period;
	                dbg("RadioCountToLedsC", "Started SensorTimer with period %d. From Receiving query with id %d:%d \n",gcd_period,qrm->owner_id,qrm->packet_id);
	              }
	            }

				backoff_time = call LocalTimer.get();
	          	backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

	          	call RebroadcastTimer.startOneShot(backoff_time);
					
	      			return bufPtr;
	      		}
	      		else if(qrm->round_id > stored[i][2]){
	      			for(j=0; j<SIZE; j++){
	              		if(queries[j].isActive && queries[j].owner == qrm->owner_id && queries[j].id == qrm->packet_id){
	              			queries[j].sender = qrm->sender_id;
	              			dbg("RadioCountToLedsC", "Round: %d.Changed sender to: %d \n",qrm->round_id,queries[j].sender);
	              			break;
	              		}
	              	}
	           		//dbg("RadioCountToLedsC","Packet %d:%d ArrTime %d.\n",qrm->owner_id,qrm->packet_id,xronos);
					//Sort the routing table
	           		for(j=0; j<i; j++){
	           			stored[i-j][0]= stored[i-j-1][0];
	           			stored[i-j][1]= stored[i-j-1][1];
	           			stored[i-j][2]= stored[i-j-1][2];
	           		}

	            	//Put new element at top of routing table
					stored[0][0] = qrm->owner_id;
		            stored[0][1] = qrm->packet_id;
		            stored[0][2] = qrm->round_id;
					stored_sender = qrm->sender_id;
		            stored_period = qrm->period;
		            stored_lifetime = qrm->lifetime;
		            stored_sensor_type = qrm->sensor_type;
		            stored_agg_mode = qrm->agg_mode;

		            backoff_time = call LocalTimer.get();
     				backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

					//REBROADCAST
      				call RebroadcastTimer.startOneShot(backoff_time);

      				return bufPtr;

	      		}
	      		else{

	      		  //Discard the message
	           	//dbg("RadioCountToLedsC", "Packet id: %d:%d already received\n",qrm->owner_id,qrm->packet_id); 		
	           	return bufPtr;
	         	}
	      	}
      }
      //Loop ended no known entry
      //Make room for the new entry in the table
      //Rebcast the message

     	xronos = call LocalTimer.get();
     	//dbg("RadioCountToLedsC","Packet %d:%d ArrTime %d.\n",qrm->owner_id,qrm->packet_id,xronos);
      	
     	for(j=0; j<SIZE-1; j++){
       	stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
       	stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
       	stored[(SIZE-1)-j][2]= stored[(SIZE-1)-j-1][2];
     	}

     	stored[0][0] = qrm->owner_id;
     	stored[0][1] = qrm->packet_id;
     	stored[0][2] = qrm->round_id;
     	stored_sender = qrm->sender_id;
      	stored_period = qrm->period;
      	stored_lifetime = qrm->lifetime;
      	stored_sensor_type = qrm->sensor_type;
      	stored_agg_mode = qrm->agg_mode;

      //Check for inactive queries on the table
      for(j=0; j<SIZE; j++){
        if(!(queries[j].isActive)){
          //Store and activate the query
          queries[j].owner = qrm->owner_id;
          queries[j].id = qrm->packet_id;
          queries[j].sender = qrm->sender_id;
          queries[j].lifetime = qrm->lifetime;
          queries[j].period = qrm->period;
          queries[j].period_id = 0;
          queries[j].period_counter = 0;
          queries[j].isActive = TRUE;
          queries[j].agg_mode = qrm->agg_mode;
          queries[j].child_count = 0;
          queries[j].waiting_time = base_waiting_time;
          queries[j].waiting_counter = 0;

          //These are clearly only used for dev purposes and should be optimised

          local_piggy[j].query_owner_id = qrm->owner_id;
          local_piggy[j].query_id = qrm->packet_id;

          local_stats[i].query_owner_id = qrm->owner_id;
          local_stats[i].query_id = qrm->owner_id;

          inserted_query = TRUE;

          break;
        }
        else if(j == (SIZE-1)){
          dbg("RadioCountToLedsC","No space for query with id %d:%d, discarded \n",qrm->owner_id,qrm->packet_id);
          /*ReCheck how the system reacts to a full query table*/
          inserted_query = FALSE;
        }
      }

      //Compute the gcd period among the active timers
      //Must work for a particular sensor type
      //Only do so if the query was not discarded
      if(inserted_query){

        num1 = stored_period;
        if(gcd_period == 0){
          num2 = stored_period;
        }
        else{
          num2 = gcd_period;
        }
        rem = num1 % num2;
        if(rem != 0){
          while(rem != 0){
            rem = num1 % num2;
            num1 = num2;
            num2 = rem;
          }
          gcd_period = num1;
        }
        else{
          gcd_period = num2;
        }

        //dbg("RadioCountToLedsC", "Made it to Sensor Timer with id %d \n",qrm->packet_id);
        if(!(call SensorTimer.isRunning())){
          call SensorTimer.startOneShot(gcd_period);
          gcd_period_old = gcd_period;
          dbg("RadioCountToLedsC", "Started SensorTimer with period %d. From Receiving query with id %d:%d \n",gcd_period,qrm->owner_id,qrm->packet_id);
        }
      }

     	backoff_time = call LocalTimer.get();
     	backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

		//REBROADCAST
      call RebroadcastTimer.startOneShot(backoff_time);

      return bufPtr;
    }
  }

  event message_t* ResultReceive.receive(message_t* bufPtr, void* payload, uint8_t len){

    if (len != sizeof(query_result_msg_t) && len != sizeof(piggy_msg_t) && len != sizeof(stats_msg_t) && len != sizeof(route_reply_t)) {
      dbg("RadioCountToLedsC", "Wrong length packet \n");
      return bufPtr;
    }
    else{
      query_result_msg_t* qrm = (query_result_msg_t*)payload;

      if(qrm->msg_type != NONE_MSG && qrm->msg_type != PIGGY_MSG && qrm->msg_type != ROUTING_MSG && qrm->msg_type != STATS_MSG){
        return bufPtr;
      }

      ////////////////////////////////////////////////////////
      //NONE msg handling
      ///////////////////////////////////////////////////////
      if(qrm->msg_type == NONE_MSG){
        if(qrm->query_owner_id == TOS_NODE_ID){
          dbg("RadioCountToLedsC", "Receive RESULT :%d from node :%d, from period :%d, for query id :%d \n", qrm->data,qrm->result_owner_id,qrm->period,qrm->packet_id);

          //////////////////////////
          //SERIAL NONE
          //////////////////////////
          query_result_msg_t* serial_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
          if (serial_result == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          serial_result->msg_type = NONE_MSG;
          serial_result->query_owner_id = qrm->query_owner_id;
          serial_result->result_owner_id = qrm->result_owner_id;
          serial_result->packet_id = qrm->packet_id;
          serial_result->period = qrm->period;
          serial_result->data = qrm->data;
              
          //May need to check for locks  
          call SerialSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_result_msg_t))


          return bufPtr;
        }
        else{
          dbg("RadioCountToLedsC", "Receive RESULT :%d from node :%d, from period :%d, for query id :%d \n", qrm->data,qrm->result_owner_id,qrm->period,qrm->packet_id);
          for(j=0; j<SIZE; j++){
            if((queries[j].owner == qrm->query_owner_id) && (queries[j].id == qrm->packet_id)){
              
              query_result_msg_t* new_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
              if (new_result == NULL) {
                dbg("RadioCountToLedsC", "Packet not allocated.\n");  
              }

              if(!locked){

                new_result->msg_type = NONE_MSG;
                new_result->query_owner_id = qrm->query_owner_id;
                new_result->result_owner_id = qrm->result_owner_id;
                new_result->packet_id = qrm->packet_id;
                new_result->period = qrm->period;
                new_result->data = qrm->data;
              
              
                if (call ResultSend.send(queries[j].sender, &packet, sizeof(query_result_msg_t)) == SUCCESS) {
                  dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
                    new_result->result_owner_id, queries[j].sender, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
                  locked = TRUE;
                }
                
              }
              else{
                dbg("RadioCountToLedsC", "Locked send conflict, NONE msg failed\n");
                dbg("RadioCountToLedsC", "from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
                    new_result->result_owner_id, queries[j].sender, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
                dbg("RadioCountToLedsC", "Using temp buffer\n");
                if(tbuf < BUFF_SIZE){
                  temp_buffer[tbuf].msg_type = NONE_MSG;
                  temp_buffer[tbuf].query_owner_id = qrm->query_owner_id;
                  temp_buffer[tbuf].result_owner_id = qrm->result_owner_id;
                  temp_buffer[tbuf].query_id = qrm->packet_id;
                  temp_buffer[tbuf].period_id = qrm->period;
                  temp_buffer[tbuf].none_data = qrm->data;

                  temp_buffer[tbuf].destination = queries[j].sender;

                  tbuf++;
                }
                else{
                  dbg("RadioCountToLedsC", "temp buffer FULL\n");
                }
              }
            }
            return bufPtr;
          }
        }
      }
      ////////////////////////////////////////////////////
      //PIGGY msg handling
      ///////////////////////////////////////////////////
      else if(qrm->msg_type == PIGGY_MSG){

        piggy_msg_t* pgm = (piggy_msg_t*)payload;

        if(pgm->query_owner_id == TOS_NODE_ID){
          //////////////////////
          //SERIAL PIGGY
          /////////////////////
          piggy_msg_t* serial_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
          if (serial_piggy == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          serial_piggy->msg_type = pgm->msg_type;
          serial_piggy->remaining_space = pgm->remaining_space;
          serial_piggy->query_owner_id = pgm->query_owner_id;
          serial_piggy->query_id = pgm->query_id;
          serial_piggy->period_id = pgm->period_id;
          for(i=0; i<PGSIZE; i++){
            serial_piggy->node_id[i] = pgm->node_id[i];
            serial_piggy->data[i] = pgm->data[i];
          }

          //May need to check for locks
          call SerialSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_result_msg_t))

          return bufPtr;
        }
        else if(pgm->query_owner_id == 0){
          dbg("RadioCountToLedsC", "Receive PIGGY RESULT with period id :%d, query owner :%d, query_id :%d \n", 
            pgm->period_id, pgm->query_owner_id, pgm->query_id);

          for(i=0; i<PGSIZE; i++){
            dbg("RadioCountToLedsC", "It contains data from node :%d and data :%d\n", pgm->node_id[i], pgm->data[i]);
          }
          return bufPtr;
        }

        if(pgm->remaining_space == 0){

          dbg("RadioCountToLedsC", "Using temp buffer\n");
          if(tbuf < BUFF_SIZE){
            temp_buffer[tbuf].msg_type = pgm->msg_type;
            temp_buffer[tbuf].remaining_space = pgm->remaining_space;
            temp_buffer[tbuf].query_owner_id = pgm->query_owner_id;
            temp_buffer[tbuf].query_id = pgm->query_id;
            temp_buffer[tbuf].period_id = pgm->period_id;
            for(i=0; i<PGSIZE; i++){
              temp_buffer[tbuf].node_id[i] = pgm->node_id[i];
              temp_buffer[tbuf].data[i] = pgm->data[i];
            }

            temp_buffer[tbuf].destination = queries[j].sender;

            tbuf++;
          }
          else{
            dbg("RadioCountToLedsC", "temp buffer FULL\n");
          }
          
          return bufPtr;
        }
        
        for(j=0; j<SIZE; j++){
          if((queries[j].owner == pgm->query_owner_id) && (queries[j].id == pgm->query_id)){
            
            //This is from an older period, just send
            if(pgm->period_id < local_piggy[j].period_id){

              
              piggy_msg_t* new_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
              if (new_piggy == NULL) {
                dbg("RadioCountToLedsC", "Packet not allocated.\n");  
              }
              

              //If a lot of outdated packets arrive, adjust waiting time
              queries[j].outdated_count++;
              if(queries[j].outdated_count == 3){
                queries[j].outdated_count = 0;
                queries[j].regulate_time++;
              }

              dbg("RadioCountToLedsC", "Using temp buffer\n");
              if(tbuf < BUFF_SIZE){
                temp_buffer[tbuf].msg_type = pgm->msg_type;
                temp_buffer[tbuf].remaining_space = pgm->remaining_space;
                temp_buffer[tbuf].query_owner_id = pgm->query_owner_id;
                temp_buffer[tbuf].query_id = pgm->query_id;
                temp_buffer[tbuf].period_id = pgm->period_id;
                for(i=0; i<PGSIZE; i++){
                  temp_buffer[tbuf].node_id[i] = pgm->node_id[i];
                  temp_buffer[tbuf].data[i] = pgm->data[i];
                }

                temp_buffer[tbuf].destination = queries[j].sender;

                tbuf++;
              }
              else{
                dbg("RadioCountToLedsC", "temp buffer FULL\n");
              }
              
              return bufPtr;
            }
            //This is from the correct period
            else{
              //Arrived packet fits in local
              if((PGSIZE - pgm->remaining_space) <= local_piggy[j].remaining_space){

                //Add to appropriate place
                for(i=(PGSIZE-local_piggy[j].remaining_space); i<PGSIZE; i++){
                  local_piggy[j].node_id[i] = pgm->node_id[i-(PGSIZE-local_piggy[j].remaining_space)];
                  local_piggy[j].data[i] = pgm->data[i-(PGSIZE-local_piggy[j].remaining_space)];
                }

                local_piggy[j].remaining_space = local_piggy[j].remaining_space - (PGSIZE - pgm->remaining_space);


                if(local_piggy[j].remaining_space == 0){
                  //Send if i filled a packet

                  dbg("RadioCountToLedsC", "Using temp buffer\n");
                  if(tbuf < BUFF_SIZE){
                    temp_buffer[tbuf].msg_type = local_piggy[j].msg_type;
                    temp_buffer[tbuf].remaining_space = local_piggy[j].remaining_space;
                    temp_buffer[tbuf].query_owner_id = local_piggy[j].query_owner_id;
                    temp_buffer[tbuf].query_id = local_piggy[j].query_id;
                    temp_buffer[tbuf].period_id = local_piggy[j].period_id;
                    for(i=0; i<PGSIZE; i++){
                      temp_buffer[tbuf].node_id[i] = local_piggy[j].node_id[i];
                      temp_buffer[tbuf].data[i] = local_piggy[j].data[i];
                    }

                    temp_buffer[tbuf].destination = queries[j].sender;

                    tbuf++;
                  }
                  else{
                    dbg("RadioCountToLedsC", "temp buffer FULL\n");
                  }
                  
                  //Empty local buffer
                  local_piggy[j].remaining_space = PGSIZE;
                  for(i=0; i<PGSIZE; i++){
                    local_piggy[j].node_id[i] = 0;
                    local_piggy[j].data[i] = 0;
                  }

                  return bufPtr;
                }
              }
              //If arrived packet does not fit in local
              else{

                //Fill packet until full then send
                
                dbg("RadioCountToLedsC", "Using temp buffer\n");
                if(tbuf < BUFF_SIZE){
                  temp_buffer[tbuf].msg_type = local_piggy[j].msg_type;
                  temp_buffer[tbuf].remaining_space = 0;
                  temp_buffer[tbuf].query_owner_id = local_piggy[j].query_owner_id;
                  temp_buffer[tbuf].query_id = local_piggy[j].query_id;
                  temp_buffer[tbuf].period_id = local_piggy[j].period_id;

                  for(i=0; i<PGSIZE; i++){
                    //Fill packet from local first
                    if(i <(PGSIZE - local_piggy[j].remaining_space)){
                      temp_buffer[tbuf].node_id[i] = local_piggy[j].node_id[i];
                      temp_buffer[tbuf].data[i] = local_piggy[j].data[i];
                    }
                    //From received packet until full
                    else{
                      temp_buffer[tbuf].node_id[i] = pgm->node_id[i-(PGSIZE - local_piggy[j].remaining_space)];
                      temp_buffer[tbuf].data[i] = pgm->data[i-(PGSIZE - local_piggy[j].remaining_space)];
                    } 
                  }

                  //Then fill local with remainder of received
                  for(i=0; i<PGSIZE; i++){
                    if(i < (PGSIZE -(pgm->remaining_space + local_piggy[j].remaining_space))){
                      local_piggy[j].node_id[i] = pgm->node_id[i + local_piggy[j].remaining_space];
                      local_piggy[j].data[i] = pgm->data[i + local_piggy[j].remaining_space];
                    }
                    else{
                      local_piggy[j].node_id[i] = 0;
                      local_piggy[j].data[i] = 0;
                    }
                  }
                      
                  local_piggy[j].remaining_space = pgm->remaining_space + local_piggy[j].remaining_space;
                      
                  temp_buffer[tbuf].destination = queries[j].sender;

                  tbuf++;
                }
                else{
                  dbg("RadioCountToLedsC", "temp buffer FULL\n");
                }
                
                return bufPtr;
              }
            }
          }
        }  
      }
      ////////////////////////////////////////////////////
      //STATS msg handling
      ///////////////////////////////////////////////////
      else if(qrm->msg_type == STATS_MSG){
        stats_msg_t* stm = (stats_msg_t*)payload;

        if(stm->query_owner_id == TOS_NODE_ID){

          /////////////////
          //SERIAL STATS
          ////////////////
          stats_msg_t* serial_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
          if (serial_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          serial_stats->msg_type = stm->msg_type;
          serial_stats->query_owner_id = stm->query_owner_id;
          serial_stats->query_id = stm->query_id;
          serial_stats->stats_owner = stm->stats_owner;
          serial_stats->period_id = stm->period_id;
          serial_stats->node_count = stm->node_count;
          serial_stats->min = stm->min;
          serial_stats->max = stm->max;
          serial_stats->avg = stm->avg;

          //May need to check for locks
          call SerialSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_result_msg_t))

          return bufPtr;
        }
        else if(stm->query_owner_id == 0){
          dbg("RadioCountToLedsC", "Receive STATS RESULT with period id :%d, query owner :%d, query_id :%d \n", 
            stm->period_id, stm->query_owner_id, stm->query_id);

          dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from %d Nodes\n", stm->min, stm->max, stm->avg, stm->node_count);

          return bufPtr;
        }

        for(j=0; j<SIZE; j++){
          if((queries[j].owner == stm->query_owner_id) && (queries[j].id == stm->query_id)){

            stats_msg_t* new_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
            if (new_stats == NULL) {
              dbg("RadioCountToLedsC", "Packet not allocated.\n");  
            }
            //This is from an older period
            if(stm->period_id < local_stats[j].period_id){

              //Check if this a known son, if not add his node count
              for(i=0; i<FAMILY_SIZE; i++){
                if(stm->stats_owner == local_stats[j].stats_owner[i]){
                  
                  if(stm->node_count > local_stats[j].old_node_count[i]){
                    //This means this son now has results for more nodes
                    //So i need to add that difference
                    local_stats[j].node_count = local_stats[j].node_count + (stm->node_count - local_stats[j].old_node_count[i]);
                    local_stats[j].old_node_count[i] = stm->node_count;
                  }

                  break;
                }
                //I found an empty position, add son
                else if(local_stats[j].stats_owner[i] == 0){
                  local_stats[j].stats_owner[i] = stm->stats_owner;
                  local_stats[j].old_node_count[i] = stm->node_count;
                  local_stats[j].node_count = local_stats[j].node_count + stm->node_count;

                  break;
                }
              }

              queries[j].outdated_count++;
              if(queries[j].outdated_count == 3){
                queries[j].outdated_count = 0;
                queries[j].regulate_time++;
              }

              new_stats->msg_type = stm->msg_type;
              new_stats->query_owner_id = stm->query_owner_id;
              new_stats->query_id = stm->query_id;
              new_stats->stats_owner = TOS_NODE_ID;
              new_stats->period_id = stm->period_id;
              new_stats->node_count = stm->node_count;
              new_stats->min = stm->min;
              new_stats->max = stm->max;
              new_stats->avg = stm->avg;

              
              if(!locked){
                if (call ResultSend.send(queries[j].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {
                  dbg("RadioCountToLedsC", "Sent an outdated stats\n");
                  dbg("RadioCountToLedsC", "With period id :%d, query owner :%d, query_id :%d \n", 
                    new_stats->period_id, new_stats->query_owner_id, new_stats->query_id);

                  dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                    new_stats->min, new_stats->max, new_stats->avg, new_stats->node_count);
                  locked = TRUE;
                } 
              }
              else{
                dbg("RadioCountToLedsC", "Found send function LOCKED, outdated STATS failed\n");
                dbg("RadioCountToLedsC", "Using temp buffer\n");
                if(tbuf < BUFF_SIZE){
                  temp_buffer[tbuf].msg_type = new_stats->msg_type;
                  temp_buffer[tbuf].query_owner_id = new_stats->query_owner_id;
                  temp_buffer[tbuf].query_id = new_stats->query_id;
                  temp_buffer[tbuf].period_id = new_stats->period_id;
                  temp_buffer[tbuf].node_count = new_stats->node_count;
                  temp_buffer[tbuf].min = new_stats->min; 
                  temp_buffer[tbuf].max = new_stats->max;
                  temp_buffer[tbuf].avg = new_stats->avg;

                  temp_buffer[tbuf].destination = queries[j].sender;

                  tbuf++;
                }
                else{
                  dbg("RadioCountToLedsC", "temp buffer FULL\n");
                }
              }
       
              return bufPtr;
            }
            //This is from the correct period
            else{
              dbg("RadioCountToLedsC", "Receive STATS RESULT with period id :%d, query owner :%d, query_id :%d \n", 
                  stm->period_id, stm->query_owner_id, stm->query_id);
              dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                    stm->min, stm->max, stm->avg, stm->node_count);

              local_stats[j].msg_type = stm->msg_type;
              local_stats[j].query_owner_id = stm->query_owner_id;
              local_stats[j].query_id = stm->query_id;
              local_stats[j].period_id = stm->period_id;

              //Check if this a known son, if not add his node count
              for(i=0; i<FAMILY_SIZE; i++){
                if(stm->stats_owner == local_stats[j].stats_owner[i]){
                  
                  if(stm->node_count > local_stats[j].old_node_count[i]){
                    //This means this son now has results for more nodes
                    //So i need to add that difference
                    local_stats[j].node_count = local_stats[j].node_count + (stm->node_count - local_stats[j].old_node_count[i]);
                    local_stats[j].old_node_count[i] = stm->node_count;
                  }

                  break;                  
                }
                //I found an empty position, add son
                else if(local_stats[j].stats_owner[i] == 0){
                  local_stats[j].stats_owner[i] = stm->stats_owner;
                  local_stats[j].old_node_count[i] = stm->node_count;
                  local_stats[j].node_count = local_stats[j].node_count + stm->node_count;

                  break;
                }
              }

              //Compute min and max
              if(local_stats[j].min == 0){
                local_stats[j].min = stm->min;
              }
              else if(stm->min < local_stats[j].min){
                local_stats[j].min = stm->min;
              }

              if(local_stats[j].max == 0){
                local_stats[j].max = stm->max;
              }
              else if (stm->max > local_stats[j].max){
                local_stats[j].max = stm->max;
              }

              //Compute avg
              if(local_stats[j].avg == 0 || local_stats[j].node_count==0){
                local_stats[j].avg = stm->avg;
              }
              else{
               local_stats[j].avg = 
                (int) ((local_stats[j].avg * (local_stats[j].node_count - stm->node_count)) + (stm->avg * stm->node_count) ) / local_stats[j].node_count; 
              }
                
              return bufPtr; 
            }
          }
        }
      }
    }
    return bufPtr;
  }
 


  event void SourceTimer.fired(){
	
  	query_msg_t* qrm = (query_msg_t*)call FloodPacket.getPayload(&packet, sizeof(query_msg_t));
    if (qrm == NULL) {
			return;
   	}

	  local_packet_id = local_packet_id+1;
	  local_round_id = local_round_id+1;
	
    qrm->msg_type = QUERY_MSG;
    qrm->agg_mode = PIGGY;
	  qrm->packet_id = 1;
	  qrm->sender_id = TOS_NODE_ID;
	  qrm->owner_id = TOS_NODE_ID;
    qrm->sensor_type = 1;
    qrm->period = 2000;
    qrm->lifetime = 40000;
    qrm->round_id = local_round_id;

		for(j=0; j<SIZE-1; j++){
    	stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
    	stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
    	stored[(SIZE-1)-j][2]= stored[(SIZE-1)-j-1][2];
    }

		stored[0][0] = qrm->owner_id;
		stored[0][1] = qrm->packet_id;
		stored[0][2] = qrm->round_id;

    if(!locked){
    	if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_msg_t)) == SUCCESS) {
				t1 = call LocalTimer.get();
				dbg("RadioCountToLedsC","Packet %d:%d SendTime %d.\n",qrm->owner_id,qrm->packet_id,t1);
				dbg("RadioCountToLedsC", "Packet sent.\n");
				locked = TRUE;
    	}
    }
	}
	

  event void RebroadcastTimer.fired(){

  	//if(!locked){
			
		//lock gia na mi mporei na steilei an de steilei to proigoumeno
  	query_msg_t* new_packet = (query_msg_t*)call FloodPacket.getPayload(&packet, sizeof(query_msg_t));
    if (new_packet == NULL) {
		  dbg("RadioCountToLedsC", "Packet not allocated.\n");	
    }
		
    new_packet->msg_type = QUERY_MSG;
    new_packet->agg_mode = stored_agg_mode; 
	  new_packet->owner_id = stored[0][0];
    new_packet->packet_id = stored[0][1];
    new_packet->round_id = stored[0][2];
	  new_packet->sender_id = TOS_NODE_ID;
    new_packet->sensor_type = stored_sensor_type;
    new_packet->period = stored_period;
    new_packet->lifetime = stored_lifetime;
		
    t1 = call LocalTimer.get();
		
    if(!locked){
		 	if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_msg_t)) == SUCCESS) {
		 	dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d:%d rebroadcast sent.\n", stored[0][0],stored[0][1],stored[0][2]);
	  	 	locked = TRUE;
      }
	  }
		else{
      dbg("RadioCountToLedsC", "Locked send conflict, flooding msg failed\n");
      
      dbg("RadioCountToLedsC", "Using temp buffer\n");
      if(tbuf < BUFF_SIZE){
        temp_buffer[tbuf].msg_type = new_packet->msg_type;
        temp_buffer[tbuf].agg_mode = new_packet->agg_mode;
        temp_buffer[tbuf].query_owner_id = new_packet->owner_id;
        temp_buffer[tbuf].query_id = new_packet->packet_id;
        temp_buffer[tbuf].sender_id = new_packet->sender_id;
        temp_buffer[tbuf].period_id = new_packet->period;
        temp_buffer[tbuf].sensor_type = new_packet->sensor_type;
        temp_buffer[tbuf].lifetime = new_packet->lifetime;
        temp_buffer[tbuf].round_id = new_packet->round_id;

        tbuf++;
      }
      else{
        dbg("RadioCountToLedsC", "temp buffer FULL\n");
      }

  	}
  }

  event void SensorTimer.fired(){

    //Check which counters to increment
    for(j=0; j<SIZE; j++){
      if(queries[j].isActive /*&& queries[j].sensor_type == this.sensor */){
        queries[j].period_counter = queries[j].period_counter + gcd_period_old;
        queries[j].lifetime = queries[j].lifetime - gcd_period_old;
        
        // Deactivate query if lifetime goes to zero
        //Also re-calculate gcd
        if(queries[j].lifetime <= 0){

          dbg("RadioCountToLedsC", "Lifetime of query id %d ended.\n",queries[j].id);
          queries[j].isActive = FALSE;

          //Calculate new gcd amongst active queries
          gcd_period = 0;
          for(i=0; i<SIZE; i++){
            if(queries[j].isActive){
              num1 = queries[i].period;
              if(gcd_period == 0){
                num2 = queries[i].period;
              }
              else{
                num2 = gcd_period;
              }
              rem = num1 % num2;

              if(rem != 0){
                while(rem != 0){
                  rem = num1 % num2;
                  num1 = num2;
                  num2 = rem;
                }
                gcd_period = num1;
              }
              else{
                gcd_period = num2;
              }
            }
          }
        }
      }
    }


    if (!lockedRead){
      call DemoRead.read();
      lockedRead = TRUE;
    }
    

    //Dont know if this works
    if(gcd_period != 0){
      call SensorTimer.startOneShot(gcd_period);
      dbg("RadioCountToLedsC", "Started SensorTimer with period %d. From inside the Timer\n",gcd_period);
    }    
    gcd_period_old = gcd_period;
  }

  event void BufferTimer.fired(){
    //Send with none/piggy/stats
    if(qbuf != 0){

      if(queries[query_buffer[qbuf-1]].agg_mode == NONE){

        query_result_msg_t* new_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
        if (new_result == NULL) {
          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
        }

        new_result->msg_type = NONE_MSG;
        new_result->query_owner_id = queries[query_buffer[qbuf-1]].owner;
        new_result->result_owner_id = TOS_NODE_ID;
        new_result->packet_id = queries[query_buffer[qbuf-1]].id;
        new_result->period = queries[query_buffer[qbuf-1]].period_id;
        new_result->data = queries[query_buffer[qbuf-1]].data;
        
        //t1 = call LocalTimer.get();
        
        if(!locked){
          if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(query_result_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
            new_result->result_owner_id, queries[query_buffer[qbuf-1]].sender, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
            locked = TRUE;
          }
          else{dbg("RadioCountToLedsC", "SEND NOT SUCCESS WEIRD BEHAVIOR\n");}
        }
        else{
          dbg("RadioCountToLedsC", "Locked send conflict, NONE result failed\n");
        }
        qbuf--; //need to check where this happens
      }
      else if(queries[query_buffer[qbuf-1]].agg_mode == PIGGY){

        //Add my data to local, and send packet;

        piggy_msg_t* new_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
        if (new_piggy == NULL) {
          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
        }

        new_piggy->msg_type = PIGGY_MSG;
        new_piggy->remaining_space = local_piggy[query_buffer[qbuf-1]].remaining_space - 1;
        new_piggy->query_owner_id = queries[query_buffer[qbuf-1]].owner;
        new_piggy->query_id = queries[query_buffer[qbuf-1]].id;
        new_piggy->period_id = queries[query_buffer[qbuf-1]].period_id;

        //First add id and data to local, for allignement
        local_piggy[query_buffer[qbuf-1]].node_id[PGSIZE - local_piggy[query_buffer[qbuf-1]].remaining_space] = TOS_NODE_ID;
        local_piggy[query_buffer[qbuf-1]].data[PGSIZE - local_piggy[query_buffer[qbuf-1]].remaining_space] = queries[query_buffer[qbuf-1]].data;

        //Add to appropriate position
        for(i=0; i<PGSIZE; i++){
          new_piggy->node_id[i] = local_piggy[query_buffer[qbuf-1]].node_id[i];
          new_piggy->data[i] = local_piggy[query_buffer[qbuf-1]].data[i];
        }

        if(!locked){
         if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "Sent a piggy at end of next period to :%d\n", queries[query_buffer[qbuf-1]].sender);

            for(i=0; i<PGSIZE; i++){
                dbg("RadioCountToLedsC", "It contains data from node :%d and data :%d\n", new_piggy->node_id[i], new_piggy->data[i]);
            }
            locked = TRUE;
          } 
        }
        else{
          dbg("RadioCountToLedsC", "Locked send conflict, PIGGY msg failed\n");
        }
        
        //Empty the local buffer

        local_piggy[query_buffer[qbuf-1]].remaining_space = PGSIZE;
        //This now denotes the NEXT period
        local_piggy[query_buffer[qbuf-1]].period_id = local_piggy[query_buffer[qbuf-1]].period_id + 1;
        for(i=0; i<PGSIZE; i++){
          local_piggy[query_buffer[qbuf-1]].node_id[i] = 0;
          local_piggy[query_buffer[qbuf-1]].data[i] = 0;
        }

        qbuf--; //need to check where this happens
      }
      else if(queries[query_buffer[qbuf-1]].agg_mode == STATS){
        //Add my data to packet and send, regardless

        stats_msg_t* new_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
        if (new_stats == NULL) {
          dbg("RadioCountToLedsC", "Packet not allocated.\n");  
        }

        new_stats->msg_type = STATS_MSG;
        new_stats->query_owner_id = queries[query_buffer[qbuf-1]].owner;
        new_stats->query_id = queries[query_buffer[qbuf-1]].id;
        new_stats->stats_owner = TOS_NODE_ID;
        new_stats->period_id = queries[query_buffer[qbuf-1]].period_id;
        new_stats->node_count = local_stats[query_buffer[qbuf-1]].node_count;

        //Compute min
        if(queries[query_buffer[qbuf-1]].data < local_stats[query_buffer[qbuf-1]].min || local_stats[query_buffer[qbuf-1]].min == 0){
          new_stats->min = queries[query_buffer[query_buffer[qbuf-1]]].data;
        }
        else{
          new_stats->min = local_stats[query_buffer[qbuf-1]].min;
        }
        //Compute max
        if(queries[query_buffer[qbuf-1]].data > local_stats[query_buffer[qbuf-1]].max){
          new_stats->max = queries[query_buffer[query_buffer[qbuf-1]]].data;
        }
        else{
          new_stats->max = local_stats[query_buffer[qbuf-1]].max;
        }
        //Compute avg
        if(local_stats[query_buffer[qbuf-1]].avg == 0){
          new_stats->avg = queries[query_buffer[qbuf-1]].data;
        }
        else{
          new_stats->avg = (int)
                      ((local_stats[query_buffer[qbuf-1]].avg * (local_stats[query_buffer[qbuf-1]].node_count - 1)) + 
                      queries[query_buffer[qbuf-1]].data)/local_stats[query_buffer[qbuf-1]].node_count;
        }

        if(!locked){
          if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {

            dbg("RadioCountToLedsC", "Sent a first in route (no childen) stats to :%d\n", queries[query_buffer[qbuf-1]].sender);
            dbg("RadioCountToLedsC", "With period id :%d, query owner :%d, query_id :%d \n", 
                new_stats->period_id, new_stats->query_owner_id, new_stats->query_id);

            dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                new_stats->min, new_stats->max, new_stats->avg, new_stats->node_count);
            locked = TRUE;
          } 
        }
        else{
          dbg("RadioCountToLedsC", "Locked send conflict, STATS msg failed\n");
        }

        //This now denotes the period id of the NEXT packet, it is one ahead
        local_stats[query_buffer[qbuf-1]].period_id = local_stats[query_buffer[qbuf-1]].period_id + 1;

        //Zero out 
        local_stats[query_buffer[qbuf-1]].min = 0;
        local_stats[query_buffer[qbuf-1]].max = 0;
        local_stats[query_buffer[qbuf-1]].avg = 0;

        qbuf--; //need to check where this happens
      }
    }
  }

  event void SendResultTimer.fired(){
    for(j=0; j<SIZE; j++){
      if(queries[j].isActive && queries[j].canSend == TRUE){
        queries[j].waiting_counter = queries[j].waiting_counter + gcd_waiting_old;

        if((queries[j].waiting_counter % queries[j].waiting_time) == 0){
          query_buffer[qbuf] = j;
          qbuf++;

          //Maybe this needs to happen inside BufferTimer
          queries[j].canSend = FALSE;

          if(! call BufferTimer.isRunning()){
            backoff_time = call LocalTimer.get();
            backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

            call BufferTimer.startOneShot(backoff_time);
          }

          queries[j].waiting_counter = 0;
        }
      }
    }

   
    if(gcd_waiting != 0){
      call SendResultTimer.startOneShot(gcd_waiting);
    }    
    gcd_waiting_old = gcd_waiting;
  }

  event void DemoRead.readDone(error_t result, uint16_t data){
    if(result == SUCCESS){
      for(j=0; j<SIZE; j++){
        if(queries[j].isActive /*&& queries[j].sensor_type == this.sensor */){
          //Check if we need to send this reading
          if((queries[j].period_counter % queries[j].period) == 0){
            
            if(queries[j].agg_mode == NONE){

              queries[j].data = data;
              query_buffer[qbuf] = j;
              //If DemoRead is called again before buffer is emptied, this may write to wrong position
              qbuf++;

              if(! call BufferTimer.isRunning()){
                backoff_time = call LocalTimer.get();
                backoff_time = ((backoff_time * TOS_NODE_ID) % 30);

                call BufferTimer.startOneShot(backoff_time);
              } 
            }
            else if(queries[j].agg_mode == PIGGY || queries[j].agg_mode == STATS){
              queries[j].data = data;
              queries[j].canSend = TRUE;

              //Safety check for waiting time
              queries[j].waiting_time = (base_waiting_time * local_stats[j].node_count) * queries[j].regulate_time;
              if(queries[j].waiting_time > queries[j].period){
                queries[j].waiting_time = queries[j].period;
              }

              //Calculate gcd of waiting times
              num1 = queries[j].waiting_time;
              if(gcd_waiting == 0){
                num2 = queries[j].waiting_time;
              }
              else{
                num2 = gcd_waiting;
              }
              rem = num1 % num2;
              if(rem != 0){
                while(rem != 0){
                  rem = num1 % num2;
                  num1 = num2;
                  num2 = rem;
                }
                gcd_waiting = num1;
              }
              else{
                gcd_waiting = num2;
              }

              if(!(call SendResultTimer.isRunning())){
                call SendResultTimer.startOneShot(gcd_waiting);
                gcd_waiting_old = gcd_waiting;
                dbg("RadioCountToLedsC", "Started SendResultTimer with gcd waiting time %d. \n",gcd_waiting);
              }
            }
            
            //Start the counter over
            queries[j].period_counter = 0;
            queries[j].period_id = queries[j].period_id + 1;
          }
        }
      }   
    }  
    lockedRead = FALSE;
  }


  event void ResultSend.sendDone(message_t* bufPtr, error_t error) {
    
    if (&packet == bufPtr){
      locked = FALSE;

      //Empty buffer
      if(qbuf != 0){
        if(queries[query_buffer[qbuf-1]].agg_mode == NONE){
          query_result_msg_t* new_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
          if (new_result == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_result->msg_type = NONE_MSG;
          new_result->query_owner_id = queries[query_buffer[qbuf-1]].owner;
          new_result->result_owner_id = TOS_NODE_ID;
          new_result->packet_id = queries[query_buffer[qbuf-1]].id;
          new_result->period = queries[query_buffer[qbuf-1]].period_id;
          new_result->data = queries[query_buffer[qbuf-1]].data;
          
          //t1 = call LocalTimer.get();
          
          if(!locked){
            if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(query_result_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
              new_result->result_owner_id, queries[query_buffer[qbuf-1]].sender, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NOT SUCCESS\n");}
          }
          else{
            dbg("RadioCountToLedsC", "Locked send conflict, NONE msg failed\n");
          }
          qbuf--;
        }
        else if(queries[query_buffer[qbuf-1]].agg_mode == PIGGY){
          //Add my data to local, and send packet;

          piggy_msg_t* new_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
          if (new_piggy == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_piggy->msg_type = PIGGY_MSG;
          new_piggy->remaining_space = local_piggy[query_buffer[qbuf-1]].remaining_space - 1;
          new_piggy->query_owner_id = queries[query_buffer[qbuf-1]].owner;
          new_piggy->query_id = queries[query_buffer[qbuf-1]].id;
          new_piggy->period_id = queries[query_buffer[qbuf-1]].period_id;
          //Add to appropriate position
          new_piggy->node_id[PGSIZE - local_piggy[query_buffer[qbuf-1]].remaining_space] = TOS_NODE_ID;
          new_piggy->data[PGSIZE - local_piggy[query_buffer[qbuf-1]].remaining_space] = queries[query_buffer[qbuf-1]].data;

          if(!locked){
           if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a piggy at end of next period to :%d\n", queries[query_buffer[qbuf-1]].sender);

              for(i=0; i<PGSIZE; i++){
                dbg("RadioCountToLedsC", "It contains data from node :%d and data :%d\n", new_piggy->node_id[i], new_piggy->data[i]);
              }
              locked = TRUE;
            } 
          }
          else{
            dbg("RadioCountToLedsC", "Locked send conflict, PIGGY msg failed\n");
          }
          
          //Empty the local buffer

          local_piggy[query_buffer[qbuf-1]].remaining_space = PGSIZE;
          //This now denotes the NEXT period
          local_piggy[query_buffer[qbuf-1]].period_id = local_piggy[query_buffer[qbuf-1]].period_id + 1;
          for(i=0; i<PGSIZE; i++){
            local_piggy[query_buffer[qbuf-1]].node_id[i] = 0;
            local_piggy[query_buffer[qbuf-1]].data[i] = 0;
          }

          qbuf--; //need to check where this happens
        }
        else if (queries[query_buffer[qbuf-1]].agg_mode == STATS){

          //Add my data to packet and send, regardless

          stats_msg_t* new_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = STATS_MSG;
          new_stats->query_owner_id = queries[query_buffer[qbuf-1]].owner;
          new_stats->query_id = queries[query_buffer[qbuf-1]].id;
          new_stats->stats_owner = TOS_NODE_ID;
          new_stats->period_id = queries[query_buffer[qbuf-1]].period_id;
          new_stats->node_count = local_stats[query_buffer[qbuf-1]].node_count;

          //Compute min
          if(queries[query_buffer[qbuf-1]].data < local_stats[query_buffer[qbuf-1]].min || local_stats[query_buffer[qbuf-1]].min == 0){
            new_stats->min = queries[query_buffer[qbuf-1]].data;
          }
          else{
            new_stats->min = local_stats[query_buffer[qbuf-1]].min;
          }
          //Compute max
          if(queries[query_buffer[qbuf-1]].data > local_stats[query_buffer[qbuf-1]].max){
            new_stats->max = queries[query_buffer[qbuf-1]].data;
          }
          else{
            new_stats->max = local_stats[query_buffer[qbuf-1]].max;
          }
          //Compute avg
          if(local_stats[query_buffer[qbuf-1]].avg == 0){
            new_stats->avg = queries[query_buffer[qbuf-1]].data;
          }
          else{
            new_stats->avg = (int)
                        ((local_stats[query_buffer[qbuf-1]].avg * (local_stats[query_buffer[qbuf-1]].node_count - 1)) + 
                        queries[query_buffer[qbuf-1]].data)/local_stats[query_buffer[qbuf-1]].node_count;
          }

          if(!locked){
            if (call ResultSend.send(queries[query_buffer[qbuf-1]].sender, &packet, sizeof(stats_msg_t)) == SUCCESS) {

              dbg("RadioCountToLedsC", "Sent a first in route (no childen) stats to :%d\n", queries[query_buffer[qbuf-1]].sender);
              dbg("RadioCountToLedsC", "With period id :%d, query owner :%d, query_id :%d \n", 
                  new_stats->period_id, new_stats->query_owner_id, new_stats->query_id);

              dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                  new_stats->min, new_stats->max, new_stats->avg, new_stats->node_count);
              locked = TRUE;
            } 
          }
          else{
            dbg("RadioCountToLedsC", "Locked send conflict, STATS msg failed\n");
          }

          //This now denotes the period id of the NEXT packet, it is one ahead
          local_stats[query_buffer[qbuf-1]].period_id = local_stats[query_buffer[qbuf-1]].period_id + 1;

          //Zero out 
          local_stats[query_buffer[qbuf-1]].min = 0;
          local_stats[query_buffer[qbuf-1]].max = 0;
          local_stats[query_buffer[qbuf-1]].avg = 0;

          qbuf--; //need to check where this happens    
        }
      }
      ///////////////////////////////////////////
      //temp_buffer handling
      //////////////////////////////////////////
      else if(tbuf!=0){
        //////////////////////////
        //Buffering of QUERY MSG
        //////////////////////////
        if(temp_buffer[tbuf-1].msg_type == QUERY_MSG){

          query_msg_t* new_packet = (query_msg_t*)call FloodPacket.getPayload(&packet, sizeof(query_msg_t));
          if (new_packet == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }
          
          new_packet->msg_type = temp_buffer[tbuf-1].msg_type;
          new_packet->agg_mode = temp_buffer[tbuf-1].agg_mode; 
          new_packet->owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_packet->packet_id = temp_buffer[tbuf-1].query_id;
          new_packet->sender_id = temp_buffer[tbuf-1].sender_id;
          new_packet->sensor_type = temp_buffer[tbuf-1].sensor_type;
          new_packet->period = temp_buffer[tbuf-1].period_id;
          new_packet->lifetime = temp_buffer[tbuf-1].lifetime;
          new_packet->round_id = temp_buffer[tbuf-1].round_id;
          
          if(!locked){
            if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d rebroadcast sent.\n", new_packet->owner_id,new_packet->packet_id);
              locked = TRUE;
            }
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of NONE MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NONE_MSG){
          query_result_msg_t* new_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
          if (new_result == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_result->msg_type = temp_buffer[tbuf-1].msg_type;
          new_result->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_result->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_result->packet_id = temp_buffer[tbuf-1].query_id;
          new_result->period = temp_buffer[tbuf-1].period_id;
          new_result->data = temp_buffer[tbuf-1].none_data;
              
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(query_result_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
              new_result->result_owner_id, temp_buffer[tbuf-1].destination, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of PIGGY MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == PIGGY_MSG){
          piggy_msg_t* new_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
          if (new_piggy == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_piggy->msg_type = temp_buffer[tbuf-1].msg_type;
          new_piggy->remaining_space = temp_buffer[tbuf-1].remaining_space;
          new_piggy->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_piggy->query_id = temp_buffer[tbuf-1].query_id;
          new_piggy->period_id = temp_buffer[tbuf-1].period_id;
          for(i=0; i<PGSIZE; i++){
            new_piggy->node_id[i] = temp_buffer[tbuf-1].node_id[i];
            new_piggy->data[i] = temp_buffer[tbuf-1].data[i];
          }
          
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a piggy from the temp buffer\n");


              for(i=0; i<PGSIZE; i++){
                dbg("RadioCountToLedsC", "It contains data from node :%d and data :%d\n", new_piggy->node_id[i], new_piggy->data[i]);
              }

              locked = TRUE;
            } 
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of STATS MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == STATS_MSG){

          stats_msg_t* new_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = temp_buffer[tbuf-1].msg_type;
          new_stats->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_stats->query_id = temp_buffer[tbuf-1].query_id;
          new_stats->stats_owner = TOS_NODE_ID;
          new_stats->period_id = temp_buffer[tbuf-1].period_id;
          new_stats->node_count = temp_buffer[tbuf-1].node_count;
          new_stats->min = temp_buffer[tbuf-1].min;
          new_stats->max = temp_buffer[tbuf-1].max;
          new_stats->avg = temp_buffer[tbuf-1].avg;

              
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(stats_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent an outdated stats\n");
              dbg("RadioCountToLedsC", "With period id :%d, query owner :%d, query_id :%d \n", 
                new_stats->period_id, new_stats->query_owner_id, new_stats->query_id);

              dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                new_stats->min, new_stats->max, new_stats->avg, new_stats->node_count);
              locked = TRUE;
            } 
          }

          tbuf--;
        }
      }
    }
  }

  event void FloodSend.sendDone(message_t* bufPtr, error_t error) {
    if (&packet == bufPtr){
      locked = FALSE;

      if(tbuf!=0){
        //////////////////////////
        //Buffering of QUERY MSG
        //////////////////////////
        if(temp_buffer[tbuf-1].msg_type == QUERY_MSG){

          query_msg_t* new_packet = (query_msg_t*)call FloodPacket.getPayload(&packet, sizeof(query_msg_t));
          if (new_packet == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }
          
          new_packet->msg_type = temp_buffer[tbuf-1].msg_type;
          new_packet->agg_mode = temp_buffer[tbuf-1].agg_mode; 
          new_packet->owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_packet->packet_id = temp_buffer[tbuf-1].query_id;
          new_packet->sender_id = temp_buffer[tbuf-1].sender_id;
          new_packet->sensor_type = temp_buffer[tbuf-1].sensor_type;
          new_packet->period = temp_buffer[tbuf-1].period_id;
          new_packet->lifetime = temp_buffer[tbuf-1].lifetime;
          new_packet->round_id = temp_buffer[tbuf-1].round_id;
          
          if(!locked){
            if (call FloodSend.send(AM_BROADCAST_ADDR, &packet, sizeof(query_msg_t)) == SUCCESS) {
            dbg("RadioCountToLedsC", "RadioCountToLedsC: packet id :%d:%d rebroadcast sent.\n", new_packet->owner_id,new_packet->packet_id);
              locked = TRUE;
            }
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of NONE MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == NONE_MSG){
          query_result_msg_t* new_result = (query_result_msg_t*)call ResultPacket.getPayload(&packet, sizeof(query_result_msg_t));
          if (new_result == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_result->msg_type = temp_buffer[tbuf-1].msg_type;
          new_result->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_result->result_owner_id = temp_buffer[tbuf-1].result_owner_id;
          new_result->packet_id = temp_buffer[tbuf-1].query_id;
          new_result->period = temp_buffer[tbuf-1].period_id;
          new_result->data = temp_buffer[tbuf-1].none_data;
              
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(query_result_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a result from :%d to :%d with period_id :%d, query owner :%d query_id :%d and data :%d\n",
              new_result->result_owner_id, temp_buffer[tbuf-1].destination, new_result->period, new_result->query_owner_id, new_result->packet_id, new_result->data);
              locked = TRUE;
            }
            else{dbg("RadioCountToLedsC", "NO SUCCES\n");}
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of PIGGY MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == PIGGY_MSG){
          piggy_msg_t* new_piggy = (piggy_msg_t*)call ResultPacket.getPayload(&packet, sizeof(piggy_msg_t));
          if (new_piggy == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_piggy->msg_type = temp_buffer[tbuf-1].msg_type;
          new_piggy->remaining_space = temp_buffer[tbuf-1].remaining_space;
          new_piggy->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_piggy->query_id = temp_buffer[tbuf-1].query_id;
          new_piggy->period_id = temp_buffer[tbuf-1].period_id;
          for(i=0; i<PGSIZE; i++){
            new_piggy->node_id[i] = temp_buffer[tbuf-1].node_id[i];
            new_piggy->data[i] = temp_buffer[tbuf-1].data[i];
          }
          
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(piggy_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent a piggy from the temp buffer\n");


              for(i=0; i<PGSIZE; i++){
                dbg("RadioCountToLedsC", "It contains data from node :%d and data :%d\n", new_piggy->node_id[i], new_piggy->data[i]);
              }

              locked = TRUE;
            } 
          }

          tbuf--;
        }
        //////////////////////////
        //Buffering of STATS MSG
        //////////////////////////
        else if(temp_buffer[tbuf-1].msg_type == STATS_MSG){

          stats_msg_t* new_stats = (stats_msg_t*)call ResultPacket.getPayload(&packet, sizeof(stats_msg_t));
          if (new_stats == NULL) {
            dbg("RadioCountToLedsC", "Packet not allocated.\n");  
          }

          new_stats->msg_type = temp_buffer[tbuf-1].msg_type;
          new_stats->query_owner_id = temp_buffer[tbuf-1].query_owner_id;
          new_stats->query_id = temp_buffer[tbuf-1].query_id;
          new_stats->stats_owner = TOS_NODE_ID;
          new_stats->period_id = temp_buffer[tbuf-1].period_id;
          new_stats->node_count = temp_buffer[tbuf-1].node_count;
          new_stats->min = temp_buffer[tbuf-1].min;
          new_stats->max = temp_buffer[tbuf-1].max;
          new_stats->avg = temp_buffer[tbuf-1].avg;

              
          if(!locked){
            if (call ResultSend.send(temp_buffer[tbuf-1].destination, &packet, sizeof(stats_msg_t)) == SUCCESS) {
              dbg("RadioCountToLedsC", "Sent an outdated stats\n");
              dbg("RadioCountToLedsC", "With period id :%d, query owner :%d, query_id :%d \n", 
                new_stats->period_id, new_stats->query_owner_id, new_stats->query_id);

              dbg("RadioCountToLedsC", "It contains min :%d max :%d avg :%d from number of Nodes :%d\n", 
                new_stats->min, new_stats->max, new_stats->avg, new_stats->node_count);
              locked = TRUE;
            } 
          }

          tbuf--;
        }
      }
    }
  }

  event void SerialSend.sendDone(message_t* bufPtr, error_t error){
    //Do nothing
    //Maybe add lock for serial
  }


}





