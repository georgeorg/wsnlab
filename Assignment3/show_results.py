#!/usr/bin/python

import sys
import os
import re
import pprint
import operator


def give_me_results(filename,nodes):


	print 
	print " 	__________         _________     .__   __          "
	print " \______   \ ____  /   _____/__ __|  |_/  |_  ______"
	print "  |       _// __ \ \_____  \|  |  \  |\   __\/  ___/"
	print "  |    |   \  ___/ /        \  |  /  |_|  |  \___ \ "
	print "  |____|_  /\___  >_______  /____/|____/__| /____  >"
	print "         \/     \/        \/                     \/ "
	print 50 * '-'
	print "\n"
	print "\n"

	size = int(nodes)+1
	total_number_of_transmissions = 0
	dokimi={}   #pinakas gia ton ypologismo twn transmissions
	coverage={}  #pinakas gia ton ypologismo tou coverage
	coverage_per_source = {}
	latencies = {}
	send_time = {}
	latency_source_4 = []
	latency_source_9 = []
	min_latency_source_4 = []
	min_latency_source_9 = []
	max_latency_source_4 = []
	max_latency_source_9 = []


	f = open(filename,'r')

	line3 = f.readlines()

	#arxikopoiisi pinakwn
	for i in range(1,size):
			dokimi[i] = 0
			coverage[i] = 0
			coverage_per_source[i] = [0]*size


	#ypologismos twn rtt kai twn synolikwn broadcast
	for line4 in line3:
		searchObj = re.search( r'DEBUG\s\((\d+)\)', line4, re.M|re.I)
		if searchObj != None :
			for part in line4.split():
				if "rebroadcast" in part:
					total_number_of_transmissions = total_number_of_transmissions+1
					dokimi[int(searchObj.group(1))]+=1
					
	total_packets = 0
	#ypologismos coverage (gia kathe pigi to synolo autwn pou piran to kathe paketo)
	for line4 in line3:
		packet_id_Obj = re.search( r'DEBUG\s\((\d+)\):\s.*Packet\s(\d+):(\d+)\sArrTime\s(\d+)', line4, re.M|re.I)
		if packet_id_Obj != None :				
			coverage_per_source[int(packet_id_Obj.group(2))][int(packet_id_Obj.group(3))]+=1
			total_packets +=1

	#ypologismos max arrival
	for line4 in line3:
		packet_id_Obj = re.search( r'DEBUG\s\((\d+)\):\s.*Packet\s(\d+:\d+)\sArrTime\s(\d+)', line4, re.M|re.I)
		if packet_id_Obj != None :
			latencies[packet_id_Obj.group(2)] = packet_id_Obj.group(3)
			
	numberofpackets4 = 0
	numberofpackets9 = 0
	#ypologismos latency and min latency
	for line4 in line3:
		packet_id_Obj = re.search( r'DEBUG\s\((\d+)\):\s.*Packet\s(\d+):(\d+)\sArrTime\s(\d+)', line4, re.M|re.I)
		if packet_id_Obj != None :
			if(int(packet_id_Obj.group(2)))	== 9:			
				latency_source_9.append({'NodeID':int(packet_id_Obj.group(1)),'PacketID':int(packet_id_Obj.group(3)),'Time':int(packet_id_Obj.group(4))})
				if(int(packet_id_Obj.group(3)) > numberofpackets9) :
					numberofpackets9 = int(packet_id_Obj.group(3))
			else:
				latency_source_4.append({'NodeID':int(packet_id_Obj.group(1)),'PacketID':int(packet_id_Obj.group(3)),'Time':int(packet_id_Obj.group(4))})
				if(int(packet_id_Obj.group(3)) > numberofpackets4) :
					numberofpackets4 = int(packet_id_Obj.group(3))
	#ypologismos send_time
	for line4 in line3:
		packet_id_Obj = re.search( r'DEBUG\s\((\d+)\):\s.*Packet\s(\d+:\d+)\sSendTime\s(\d+)', line4, re.M|re.I)
		if packet_id_Obj != None :
			send_time[packet_id_Obj.group(2)] = int(packet_id_Obj.group(3))
					


	print "The total number of transmissions is :" , total_number_of_transmissions
	print "\n"
	print "Total number of transmissions per node \n" ,dokimi
	print "\n"
	print "Total coverage per source "
	pprint.pprint(coverage_per_source)
	print "\n"
	print "Send Time \n"
	print send_time
	print "\n"
	#print "Max Arrival time  \n"
	#pprint.pprint(latency_source_4[{'Time'}])
	packet_avg = 0
	sum_avg = 0
	min_latency_4 = 0
	for i in range (1,numberofpackets4):
		for item in latency_source_4:
			if item['PacketID'] == i :
				if min_latency_4 == 0:
					min_latency_4 = 1
					min_latency_source_4.append(item['Time'])
				packet_avg = packet_avg + item['Time']- send_time['4:'+ str(item['PacketID'])]
		sum_avg = sum_avg + (packet_avg/(size-2) )
		min_latency_4 = 0
	sum_avg = sum_avg/numberofpackets4 

	print "Min latencies for source 4",min_latency_source_4
	print "Average latency of source 4",sum_avg
	packet_avg=0
	sum_avg=0
	min_latency_9 = 0
	for i in range (1,numberofpackets9):
		for item in latency_source_9:
			if item['PacketID'] == i :
				if min_latency_9 == 0:
					min_latency_9 = 1
					min_latency_source_9.append(item['Time'])
				packet_avg = packet_avg + item['Time']- send_time['9:'+ str(item['PacketID'])]
		sum_avg = sum_avg +( packet_avg/(size-2) )
		min_latency_9 = 0
	sum_avg = sum_avg/numberofpackets9

	print "Min latencies for source 9:",min_latency_source_9
	print "\n"
	print "Max latencies: ",latencies
	print "\n"
	print "Average latency of source 9:",sum_avg
	coverage_percentage = operator.div(float(total_packets),(numberofpackets4+numberofpackets9)*15)
	print "Coverage %.2f %%" %(coverage_percentage*100)
	print "\n"
	#pprint.pprint(latency_source_9)

def main():

	"""Prwto orisma to onoma tou arxeiou me tis metriseis deutero orisma posous komvous exoume
	"""
	if len(sys.argv) == 3:
		filename = sys.argv[1]
		nodes = sys.argv[2]

		if not os.path.isfile(filename):
			print filename + 'does not exist.'

	give_me_results(filename,nodes)


if __name__ == '__main__':
	main()