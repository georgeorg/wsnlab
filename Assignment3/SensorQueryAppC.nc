#include "SensorQuery.h"



configuration SensorQueryAppC {}
implementation {

  components MainC, SensorQueryC as App, LedsC;

	//components new DemoSensorC() as DemoSensor;
  components new HamamatsuS1087ParC() as DemoSensor;
	
  
  components new AMSenderC(AM_RADIO_COUNT_MSG) as FloodSender;
  components new AMReceiverC(AM_RADIO_COUNT_MSG) as FloodReceiver;

  components new AMSenderC(AM_QUERY_MSG) as ResultSender;
  components new AMReceiverC(AM_QUERY_MSG) as ResultReceiver;

  components new SerialAMReceiverC(AM_SERIAL_MSG) as SerialReceiver;
  components new SerialAMSenderC(AM_SERIAL_MSG) as SerialSender;
  components SerialActiveMessageC as SerialMessage;

  components ActiveMessageC;


  components new TimerMilliC() as SourceTimer;
  components new TimerMilliC() as RebroadcastTimer;
  components new TimerMilliC() as SensorTimer;
  components new TimerMilliC() as BufferTimer;
  components new TimerMilliC() as SendResultTimer;
  components HilTimerMilliC as LocalTimer;
   
  App.Boot -> MainC.Boot;
  App.FloodReceive -> FloodReceiver;
  App.FloodSend -> FloodSender;

  App.ResultSend -> ResultSender;
  App.ResultReceive -> ResultReceiver;

  App.Receive_Serial -> SerialReceiver;
  App.SerialSend -> SerialSender;
  App.AMControl_Serial -> SerialMessage;

  App.AMControl -> ActiveMessageC;
  App.FloodPacket -> FloodSender;
  App.ResultPacket -> ResultSender;
  //May need to connect this to multiple senders

  App.DemoRead -> DemoSensor;

  App.LocalTimer -> LocalTimer;
  App.SourceTimer -> SourceTimer;
  App.SensorTimer -> SensorTimer;
  App.RebroadcastTimer -> RebroadcastTimer;
  App.BufferTimer -> BufferTimer;
  App.SendResultTimer -> SendResultTimer;

  App.Leds -> LedsC;	
}


