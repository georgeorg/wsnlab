// $Id: RadioCountToLedsC.nc,v 1.6 2008/06/24 05:32:31 regehr Exp $

/*									tab:4
 * "Copyright (c) 2000-2005 The Regents of the University  of California.  
 * All rights reserved.
 *
 * Permission to use, copy, modify, and distribute this software and its
 * documentation for any purpose, without fee, and without written agreement is
 * hereby granted, provided that the above copyright notice, the following
 * two paragraphs and the author appear in all copies of this software.
 * 
 * IN NO EVENT SHALL THE UNIVERSITY OF CALIFORNIA BE LIABLE TO ANY PARTY FOR
 * DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES ARISING OUT
 * OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE UNIVERSITY OF
 * CALIFORNIA HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * THE UNIVERSITY OF CALIFORNIA SPECIFICALLY DISCLAIMS ANY WARRANTIES,
 * INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 * AND FITNESS FOR A PARTICULAR PURPOSE.  THE SOFTWARE PROVIDED HEREUNDER IS
 * ON AN "AS IS" BASIS, AND THE UNIVERSITY OF CALIFORNIA HAS NO OBLIGATION TO
 * PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS."
 *
 * Copyright (c) 2002-2003 Intel Corporation
 * All rights reserved.
 *
 * This file is distributed under the terms in the attached INTEL-LICENSE     
 * file. If you do not find these files, copies can be found by writing to
 * Intel Research Berkeley, 2150 Shattuck Avenue, Suite 1300, Berkeley, CA, 
 * 94704.  Attention:  Intel License Inquiry.
 */
 
#include "Timer.h"
#include "RadioCountToLeds.h"
 
/**
 * Implementation of the RadioCountToLeds application. RadioCountToLeds 
 * maintains a 4Hz counter, broadcasting its value in an AM packet 
 * every time it gets updated. A RadioCountToLeds node that hears a counter 
 * displays the bottom three bits on its LEDs. This application is a useful 
 * test to show that basic AM communication and timers work.
 *
 * @author Philip Levis
 * @date   June 6 2005
 */

module RadioCountToLedsC @safe() {
  uses {
    interface Leds;
    interface Boot;
    interface Receive;
    interface AMSend;
    //interface Timer<TMilli> as Timer0 ;
	interface Timer<TMilli> as Timer1 ;
	interface LocalTime<TMilli> as Timer3;
    interface SplitControl as AMControl;
    interface Packet;
	interface SplitControl as AMControl_Serial;
	interface Receive as Receive_Serial;

  }
}
implementation{

    #define SIZE 10
    
    message_t packet;

  	uint8_t ledflag;
  	uint32_t rtt_time = 0;
  	uint32_t waiting_time = 0;
  	bool locked;
  	bool locked_timer = FALSE;
  	uint16_t counter = 0;

  	uint8_t i, j;
  	uint16_t stored[SIZE][2];

  	uint16_t stored_sender = 0;
  	uint16_t local_packet_id = 0;
  	uint32_t t1,t2,xronos;
  	uint16_t stored_data = 0;
    //uint16_t stored_max_hops = 0;
  	
  	event void Boot.booted() {
    	call AMControl.start();
		call AMControl_Serial.start();

    	//Initialize routing table
    	for(i=0; i<SIZE; i++){
    		stored[i][0] = 0;
    		stored[i][1] = 0;
    	}
  	}


	event void AMControl_Serial.startDone(error_t err) {
		if (err != SUCCESS) {
			call AMControl.start();
		}	
	}


  	event void AMControl.startDone(error_t err) {
    	if (err == SUCCESS) {
	  		ledflag = 0;
      	/*	if(TOS_NODE_ID == 0){
				radio_count_msg_t* rcm = (radio_count_msg_t*)call Packet.getPayload(&packet, sizeof(radio_count_msg_t));
      			if (rcm == NULL) {
					return;
      			}
	    		
	    		rcm->packet_id = 1;
				rcm->sender_id = TOS_NODE_ID;
				rcm->prev_sender = TOS_NODE_ID;
				rcm->owner_id = TOS_NODE_ID;
				rcm->flood_round = 1;
				rcm->max_hops = 0;
    			
    			if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(radio_count_msg_t)) == SUCCESS) {
					t1 = call Timer3.get();
					dbg("RadioCountToLedsC", "RadioCountToLedsC: packet sent.\n");
					
					stored[0][0] = rcm->owner_id;
					stored[0][1] = rcm->packet_id;
		  			
		  			locked = TRUE;
      			}
				//call Timer0.startOneShot(1000); TIMER FOR FLOOD ROUND 2
	  		}  */
   	}	
   	else{
    	call AMControl.start();
   	}
 	}

  event void AMControl.stopDone(error_t err) {
    // do nothing
  }

    event void AMControl_Serial.stopDone(error_t err) {
    // do nothing
  }
  

  event message_t* Receive_Serial.receive(message_t* bufPtr, void* payload, uint8_t len) {

		if (len != sizeof(serial_msg_t)) {
    	return bufPtr;
    	}
    	else {
		serial_msg_t* srl = (serial_msg_t*)payload;
		//radio_count_msg_t* rcm = (radio_count_msg_t*)call Packet.getPayload(&packet, sizeof(radio_count_msg_t));
		dbg("RadioCountToLedsC", "Received packet data %d, from serial.",srl->data);




    local_packet_id = local_packet_id+1; 
	  //rcm->packet_id = local_packet_id;
		//rcm->sender_id = TOS_NODE_ID;
		//rcm->prev_sender = TOS_NODE_ID;
		//rcm->owner_id = TOS_NODE_ID;
		//rcm->data = srl->data;
		//rcm->max_hops = 0;
		

		for(j=0; j<SIZE-1; j++){
        	stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
        	stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
      	}

      	stored[0][0] = TOS_NODE_ID;
      	stored[0][1] = local_packet_id;
      	stored_sender = TOS_NODE_ID;
      	stored_data = srl->data;
	  	//stored_max_hops = rcm ->max_hops;
		
		waiting_time = call Timer3.get();
    waiting_time = (waiting_time * TOS_NODE_ID) % 10;
    call Timer1.startOneShot(waiting_time);
	return bufPtr;

	}
  }


  event message_t* Receive.receive(message_t* bufPtr, void* payload, uint8_t len) {
	
    //dbg("RadioCountToLedsC", "Received packet of length %hhu.\n", len);
    if (len != sizeof(radio_count_msg_t)) {
    	return bufPtr;
    }
    else {
      
      radio_count_msg_t* rcm = (radio_count_msg_t*)payload;
      dbg("RadioCountToLedsC", "Received packet data %d, from node %d.\n", rcm->packet_id,rcm->sender_id);
	 // if(rcm->max_hops > 100){return bufPtr;}	// elegxos gia max hops tou paketou
      for (i = 0; i < SIZE; i++){
      	
      	if(stored[i][0] == rcm->owner_id){

      		if(rcm->packet_id > stored[i][1]){
      			
      			xronos = call Timer3.get();
				    dbg("RadioCountToLedsC", "xronos pou to pire prwti fora %d.\n", xronos);
				    
				
				    //Sort therouting table
            for(j=0; j<i; j++){
              stored[i-j][0]= stored[i-j-1][0];
              stored[i-j][1]= stored[i-j-1][1];
            }

            //Put new element at top of routing table
				    stored[0][0] = rcm->owner_id;
            		stored[0][1] = rcm->packet_id;
				    stored_sender = rcm->sender_id;
				    stored_data = rcm->data;

					waiting_time = call Timer3.get();
          			waiting_time = (waiting_time * TOS_NODE_ID) % 10;

				
		  			if(rcm->packet_id % 4 == 0){
						call Leds.led0Off();
						call Leds.led1Off();
					}
					else if(rcm->packet_id % 4 == 1){
						call Leds.led0On();
						call Leds.led1Off();
					}
					else if(rcm->packet_id % 4== 2){
						call Leds.led0Off();
						call Leds.led1On();
					}
					else if(rcm->packet_id % 4 == 3){
						call Leds.led0On();
						call Leds.led1On();
					}
          			call Timer1.startOneShot(waiting_time);

      				return bufPtr;
      			}
      		else{
      			//Discard the message
            if ((rtt_time == 0) && (rcm->prev_sender == TOS_NODE_ID)){

              if(locked_timer){
                t2 = call Timer3.get();
                rtt_time = t2-t1;
                dbg("RadioCountToLedsC", "RTT:%d from node %d.\n",rtt_time,rcm->sender_id); 
                dbg("RadioCountToLedsC", "T1 %d - T2 %d.\n",t1,t2); 
              }

              if(TOS_NODE_ID == 0){
                t2=call Timer3.get();
                rtt_time = t2-t1;
                dbg("RadioCountToLedsC", "RTT:%d from node %d.\n",rtt_time,rcm->sender_id); 
                dbg("RadioCountToLedsC", "T1 %d - T2 %d.\n",t1,t2); 
              }
            }

			call Leds.led2On();//Got old message, open blue
            dbg("RadioCountToLedsC", "Packet already received\n");  
            return bufPtr;
          }
      	}
      }
      //Loop ended no known entry
      //Make room for the new entry in the table
      //Rebcast the message

      	xronos = call Timer3.get();
      	dbg("RadioCountToLedsC", "xronos pou to pire prwti fora %d.\n", xronos);
      	
	  
      	for(j=0; j<SIZE-1; j++){
        	stored[(SIZE-1)-j][0]= stored[(SIZE-1)-j-1][0]; 
        	stored[(SIZE-1)-j][1]= stored[(SIZE-1)-j-1][1];
      	}

      	stored[0][0] = rcm->owner_id;
      	stored[0][1] = rcm->packet_id;
      	stored_sender = rcm->sender_id;
      	stored_data = rcm->data;
	  	//stored_max_hops = rcm ->max_hops;

      waiting_time = call Timer3.get();
      waiting_time = (waiting_time * TOS_NODE_ID) % 10;

					if(rcm->packet_id % 4 == 0){
						call Leds.led0Off();
						call Leds.led1Off();
					}
					else if(rcm->packet_id % 4 == 1){
						call Leds.led0On();
						call Leds.led1Off();
					}
					else if(rcm->packet_id % 4 == 2){
						call Leds.led0Off();
						call Leds.led1On();
					}
					else if(rcm->packet_id % 4 == 3){
						call Leds.led0On();
						call Leds.led1On();
					}

      call Timer1.startOneShot(waiting_time);

      	return bufPtr;
    	
	}
  }
 

//FLOOD ROUND 2 TIMER AND CODE
/*
  event void Timer0.fired(){
  	
  	radio_count_msg_t* rcm = (radio_count_msg_t*)call Packet.getPayload(&packet, sizeof(radio_count_msg_t));
   	if (rcm == NULL) {
		return;
    }
	    
	rcm->packet_id = 1;
	rcm->sender_id = TOS_NODE_ID;
	rcm->prev_sender = TOS_NODE_ID;
	rcm->owner_id = TOS_NODE_ID;
	rcm->flood_round = 2;
    
    if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(radio_count_msg_t)) == SUCCESS) {
		t1 = call Timer3.get();
		dbg("RadioCountToLedsC", "RadioCountToLedsC: packet sent.\n");
		stored = rcm->packet_id;
	  	locked = TRUE;
    }
  }
*/

  event void Timer1.fired(){
  	if(!locked){
  		
  		radio_count_msg_t* new_packet = (radio_count_msg_t*)call Packet.getPayload(&packet, sizeof(radio_count_msg_t));
      if (new_packet == NULL) {
		    dbg("RadioCountToLedsC", "Packet not allocated.\n");	
      }
		
		  new_packet->owner_id = stored[0][0];
      	  new_packet->packet_id = stored[0][1];
		  new_packet->prev_sender = stored_sender;
		  new_packet->sender_id = TOS_NODE_ID;
		  new_packet->data = stored_data;
		  //new_packet->max_hops = stored_max_hops+1; 
		  	t1 = call Timer3.get();
		 	if (call AMSend.send(AM_BROADCAST_ADDR, &packet, sizeof(radio_count_msg_t)) == SUCCESS) {
			dbg("RadioCountToLedsC", "RadioCountToLedsC: packet rebroadcast sent %d , data :%d.\n", stored[0][1],stored_data);
			
			//call Leds.led0Toggle(); //Sent a packet, toggle red

	  		locked = TRUE;
			locked_timer=TRUE;
    	}
	}
	
 }

  event void AMSend.sendDone(message_t* bufPtr, error_t error) {
    
    if (&packet == bufPtr){
      locked = FALSE;
    }
	
	 if( rtt_time == 0){
		  t1 = call Timer3.get();
 	  }
  }

}





