#! /usr/bin/python
from TOSSIM import *
import random
import sys
t = Tossim([])
r = t.radio()

t.addChannel("RadioCountToLedsC", sys.stdout)


r.add(0,1,0)
r.add(1,0,0)


noise = open("./noise_small.txt", "r")

for line in noise:
	str1 = line.strip()
	if str1:
		val = int(str1)
		t.getNode(0).addNoiseTraceReading(val)
		t.getNode(1).addNoiseTraceReading(val)
		t.getNode(2).addNoiseTraceReading(val)
		t.getNode(3).addNoiseTraceReading(val)
	
print "Creating noise model for node 0"
t.getNode(0).createNoiseModel()

print "Creating noise model for node 1"
t.getNode(1).createNoiseModel()
	

t.getNode(0).bootAtTime(100);
t.getNode(1).bootAtTime(10);


for i in range(10000):
	t.runNextEvent()

#start injecting SERIAL packets periodicaly
#for i in range(0,1):
	#select pseudo-randomly a node
#	sender_node = 0;
#	print "Injecting a SERIAL trigger packet to node",sender_node;
#	msg = FloodingMsg()
#	#msg.set_group_id(5)
	#msg.set_node_id(sender_node)
	#msg.set_msg_id(0)
#	serialpkt = t.newSerialPacket();
#	serialpkt.setData(msg.data)
#	serialpkt.setType(msg.get_amType())
#	serialpkt.setDestination(sender_node)
#	serialpkt.deliverNow(sender_node)
#	for i in range(10000):
#		t.runNextEvent()
	
